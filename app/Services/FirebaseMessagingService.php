<?php

namespace App\Services;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class FirebaseMessagingService
{
    public $user_id;
    public $notification_title = 'Test notification';
    public $notification_body = 'Our notifications is working';

    public function __construct()
    {
        $this->factory = (new Factory)->withServiceAccount(base_path('firebase_key.json'));
        $this->client = $this->factory->createMessaging();
    }

    public function send_general_message() {
        $topic = 'general';
        $notification = Notification::create($this->notification_title, $this->notification_body);
        $message = CloudMessage::withTarget('topic', $topic)
            ->withNotification($notification) // optional
            // ->withData($data) // optional
        ;
        $this->client->send($message);
    }

    public function send_notification() {
        $topic = 'user-'.$this->user_id;
        $notification = Notification::create($this->notification_title, $this->notification_body);
        $message = CloudMessage::withTarget('topic', $topic)
            ->withNotification($notification) // optional
            // ->withData($data) // optional
        ;
        $this->client->send($message);
    }

}
?>
