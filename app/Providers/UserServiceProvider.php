<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //dd('jere');
        view()->composer(
            'layouts.menu',
            'App\Http\ViewComposers\UserMenuComposer'
        );

        $this->sidebar();
    }


    public function sidebar()
    {

        view()->composer(
            'front.*',
            'App\Http\ViewComposers\UserMenuComposer@categories'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
