<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public  function  Profile()
    {
        return $this->hasOne('App\Model\UserProfile');
    }
    public  function  Role()
    {
        return $this->belongsTo('App\Model\UserRole', 'role_id', 'id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public static function CreateUser($data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function isAdmin()
    {
        if ($this->role_id == "2") {
            return 1;
        }
        return 0;
    }
    public function business() {
        return $this->hasOne('App\Model\Business');
    }
    public function isBusiness()
    {
        if ($this->role_id == "3") {
            return 1;
        }
        return 0;
    }



    public function verified()
    {
        $this->verified = 1;
        $this->email_token = null;
        $this->save();
    }

    public static function GetActivityLog($user_id)
    {
        $query = @"(select id, created_at,'review' as 'activity'  from reviews where user_id = " . $user_id .
            " limit 5) union
        (select  id, created_at, 'vote' as 'activity' from votes where user_id=" . $user_id .
            " limit 5)  union
        (select id, created_at, 'comment' as 'activity'  from comments where user_id =" . $user_id .
            " limit 5) order by created_at desc";

        return DB::select($query);
    }


    public static function GetPollResponses($poll_id)
    {
        $result = User::selectRaw("users.id,users.email,poll_questions.id as qid,poll_questions.body as question,poll_question_options.body as answer")
            ->join('poll_question_responses', 'users.id', '=', 'poll_question_responses.user_id')
            ->join('poll_question_options', 'poll_question_options.id', '=', 'poll_question_responses.option_id')
            ->join('poll_questions', 'poll_questions.id', '=', 'poll_question_responses.poll_question_id')
            //->where('poll_question_responses.activated','1')
            ->where('poll_question_responses.poll_id', $poll_id)
            ->with('Profile')->get();

        return $result;
    }


    static function GetRandomPassword($length, $characters)
    {

        // $length - the length of the generated password
        // $count - number of passwords to be generated
        // $characters - types of characters to be used in the password

        // define variables used within the function
        $symbols = array();
        $passwords = array();
        $used_symbols = '';
        $pass = '';

        // an array of different character types
        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = '!?~@#-_+<>[]{}';

        $characters = explode(",", $characters); // get characters types to be used for the passsword
        foreach ($characters as $key => $value) {
            $used_symbols .= $symbols[$value]; // build a string with all characters
        }
        $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1

        $pass = '';
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
        }
        return $pass;
    }
}
