<?php

namespace App\Mail;

use App\User;
use App\UserSummary;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DailySummary extends Mailable
{
    use Queueable, SerializesModels;

    private $summaries;
    private $user;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, array $summaries)
    {
        $this->user = $user;
        $this->summaries = $summaries;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.daily-summary', [
            'user' => $this->user,
            'summaries' => $this->summaries
        ]);
    }
}
