<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BusinessAccountCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $business;
    public $pass;
    public function __construct($business,$pass)
    {
        $this->business=$business;
        $this->pass=$pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.businesses.account-created');
    }


    function render(){
        $this->build();

        if ($this->markdown) {
            return $this->buildMarkdownView()['html'];
        }

         return view($this->buildView(), $this->buildViewData());
    }
}
