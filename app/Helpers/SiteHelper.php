<?php

namespace App\Helpers;

use Image;
use App\Model\File;

class SiteHelper
{

    public static $file_path = "uploads/";
    public static $pixel_200 = "_200x200";
    public static $pixel_300 = "_300";

    public function __construct()
    {
    }

    public static function CreateThumbnail($file)
    {
        // open an image file
        $path_parts = pathinfo(self::$file_path . $file);
        $img = Image::make($file);

        // now you are able to resize the instance
        $img->resize(200, 200);

        // finally we save the image as a new file
        $img->save(self::$file_path . $path_parts['filename'] . self::$pixel_200 . '.' . $path_parts['extension']);

        $img2 = Image::make($file);
        $img2->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(self::$file_path . $path_parts['filename'] . self::$pixel_300 . '.' . $path_parts['extension']);
    }


    public static function UploadFile($file, $user_id, $custom_name = "")
    {

        $f = new File();
        $f->name = $file->getClientOriginalName();
        $f->mimetype = $file->getClientMimeType();
        $f->extension = $file->getClientOriginalExtension();
        $f->user_id = $user_id;
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        if ($custom_name != "") {
            $filename = pathinfo($custom_name, PATHINFO_FILENAME);
            $f->name = $custom_name;
        }

        $temp_name = str_replace(' ', '-', $filename) . '-' . date('YmdHis');
        $temp_name = self::Slug($temp_name);
        $f->url = $temp_name . '.' . $f->extension;



        $upload_success = $file->move(self::$file_path, $f->url);
        if ($upload_success) {
            if (strtoupper($f->extension) == 'JPG' || strtoupper($f->extension) == 'PNG') {
                self::CreateThumbnail(self::$file_path . $f->url);
            }
            $f->save();
            return $f->id;
        }

        return '0';
    }

    public static function Slug($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

    public static function getActiveTab($type)
    {
        switch ($type) {
            case 'review':
                return 1;
                break;
            case 'post':
                return 2;
                break;
            case 'question':
                return 3;
                break;

            default:
                return 1;
                break;
        }
    }

    public static function ReturnWords($string, $numwords = 20)
    {
        $string = explode(' ', $string, $numwords + 1);
        if (count($string) >= $numwords) {
            array_pop($string);
        }

        $string = implode(' ', $string);
        return $string;
    }


    public static function ReturnWordsWithFlag($string, $numwords = 20)
    {
        $required_string_array = explode(' ', $string, $numwords + 1);
        $original_string_array = explode(' ', $string);

        if (count($required_string_array) >= $numwords) {
            array_pop($required_string_array);
        }

        $final_string = implode(' ', $required_string_array);

        $was_truncated = false;
        if (count($original_string_array) > count($required_string_array)) {
            $was_truncated = true;
        }

        return array($final_string, $was_truncated);
    }

    public  static  function ReturnDayFromDateObject($time)
    {
        return date('j F, Y', strtotime($time));
    }
}
