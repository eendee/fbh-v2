<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Activity;
use App\ActivityType;

class NotifiableActivity
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $activity;
    public $activity_type_id;
    public function __construct($object,$activity_type_id,$details="")
    {
        $a= new Activity();
        $a->activity_type_id=$activity_type_id;
        $a->url=url('reviews/show/'.$object->id);////hardcodding items here, need to make it more generic as per the class name
        $a->user_id=$object->user_id;
        $a->object_id=$object->item_id;//hardcodding items here, need to make it more generic as per the class name
        $a->details=$details;
        $this->activity=$a;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
