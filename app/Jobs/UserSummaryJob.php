<?php

namespace App\Jobs;

use App\Mail\DailySummary;
use App\Model\Follow;
use App\Model\Review;
use App\User;
use App\UserSummary;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserSummaryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::all();
        $startDate = Carbon::now()->addHours(-24);
        $reviews = Review::whereDate('created_at', '>=', $startDate)->get();
        $groupedReviews = $reviews->groupBy('item_id');

        Log::info('Total of ' . count($reviews) . ' since ' . $startDate->toString());

        foreach ($users as $user) {
            $followItems = Follow::getUserItems($user->id);

            $userReviews = [];

            foreach ($followItems as $followItem) {
                $item = $followItem->item;

                if ($item == null) {
                    continue;
                }

                if (!Arr::has($groupedReviews, $item->id)) {
                    continue;
                }

                // $itemReviews = array_filter(
                //     $groupedReviews[$item->id]->all(),
                //     function ($review) use ($user) {
                //         return $review->user_id != $user->id;
                //     }
                // );

                $itemReviews = $groupedReviews[$item->id];

                if (count($itemReviews) > 0) {
                    array_push($userReviews, new UserSummary($item, $itemReviews));
                }

                Log::debug('Logging item -> ' . $item->id);
            }

            if (count($userReviews) > 0) {
                Log::info('Total of ' . count($userReviews) . ' for ' . $user->email);

                Log::info('Sending email to ' . $user->email);

                Mail::to($user)->send(new DailySummary($user, $userReviews));
            }
        }
    }
}
