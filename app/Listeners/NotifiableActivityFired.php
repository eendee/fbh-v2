<?php

namespace App\Listeners;

use App\Events\NotifiableActivity;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifiableActivityFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotifiableActivity  $event
     * @return void
     */
    public function handle(NotifiableActivity $event)
    {
        //
        //test
        //$event->review->title="test";
        $event->activity->save();
    }
}
