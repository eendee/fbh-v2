<?php

namespace App\Console\Commands;

use Cron\CronExpression;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;

class ScheduleList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List when scheduled commands are executed.';

    /**
     * @var Schedule
     */
    protected $schedule;

    /**
     * ScheduleList constructor.
     *
     * @param Schedule $schedule
     */
    public function __construct(Schedule $schedule)
    {
        parent::__construct();

        $this->schedule = $schedule;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = array_map(function ($event) {
            return [
                'cron' => $event->expression,
                'command' => $event->command,
                'next-execution' => CronExpression::factory($event->expression)->getNextRunDate()->format(DATE_W3C),
                'last-execution' => CronExpression::factory($event->expression)->getPreviousRunDate()->format(DATE_W3C)
            ];
        }, $this->schedule->events());

        $this->table(['Cron', 'Command', 'Next Execution', 'Last Execution'], $events);
    }
}
