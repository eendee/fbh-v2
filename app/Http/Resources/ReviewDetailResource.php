<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReviewDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rating' => $this->rating,
            'attribute' => $this->ReviewTemplateDetail->attribute,
            'attribute_id' => $this->review_template_detail_id,
            'description' => $this->ReviewTemplateDetail->description,
        ];
    }
}
