<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FollowedItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'item_id' => isset($this->Item) ? $this->Item->id : null,
            'updated_at' => isset($this->created_at) ? $this->created_at->diffForHumans() : null,
        ];
    }
}
