<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PollQuestionResponseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'poll_id' => $this->poll_id,
            'poll_question_id' => $this->poll_question_id,
            'option_id' => $this->option_id,
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}
