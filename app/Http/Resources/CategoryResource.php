<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image_url' => $this->GetImage(),
            'icon_url' => $this->GetIcon(),
            'items_count' => isset($this->items_count) ? $this->items_count : '',
            'parent' => isset($this->Parent) ? $this->Parent->name : null,
            'parent_id' => isset($this->Parent) ? $this->Parent->id : null
        ];
    }
}
