<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'value'=>$this->value,  
            'review_id'=>$this->review_id,  
            'user_id'=>$this->user_id,      
            'created_at'=>$this->created_at->diffForHumans(),
        ];
    }
}
