<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=>$this->description,
            'rating'=>isset($this->score)?$this->score:'',
            'image_url'=>$this->GetImage(),
            'featured'=>$this->featured,
            'rating_count'=>isset($this->rating_count)?$this->rating_count:'',
            // 'review_type_id'=>isset($this->LoadReviewTemplateDetails)&&isset($this->ReviewTemplate)?$this->GetReviewTemplateDetails():'',
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
            'category' => isset($this->Category) ? $this->Category->name : '',
            'category_image_url' => isset($this->Category) ? $this->Category->GetImage() : '',
            'category_id' => $this->category_id
        ];
    }
}
