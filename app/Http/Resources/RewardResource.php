<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RewardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'user_id' => $this->user_id,
            'type' => $this->type,
            'description' => $this->description,
            'number_of_points' => $this->number_of_points,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
