<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'title' => $this->title,
            'body'=>$this->body,
            'review_id'=>$this->review_id,
            'user_id'=>$this->user_id,
            // 'user_fullname'=>isset($this->Profile)?$this->Profile->Fullname() : '',
            'user_fullname'=>$this->returnFullName($this->User),
            // 'user_profile_picture'=>isset($this->Profile)?$this->Profile->ReturnPictureUrl() : '',
            'user_profile_picture'=>$this->returnUrl($this->User),
            'created_at'=>$this->created_at->diffForHumans(),
            // 'user'=>$this->User
        ];
    }

    public function returnUrl($user) {
        if($user->role_id == 3 && isset($user->business->image_id)) {
            return $user->business->ReturnPictureUrl();
        } elseif($user->role_id != 3) {
            return $this->Profile->ReturnPictureUrl();
        } else {
            return '';
        }
    }

    public function returnFullName($user) {
        if($user->role_id == 3 && isset($user->business->image_id)) {
            return $user->business->name;
        } elseif($user->role_id != 3) {
            return $this->Profile->Fullname();
        } else {
            return '';
        }
    }

    public function checkIsBusines($user) {


    }
}
