<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug'=>$this->slug,
            'name' => $this->name,
            'excerpt' => $this->excerpt,
            'body'=>$this->body,
            'image_url'=>isset($this->Image)?$this->GetImage():'',
            'section' => isset($this->Section)?$this->Section->name:'',
            'section_id' => isset($this->Section)?$this->Section->id:'',
            'created_at' => $this->created_at
        ];
    }
}
