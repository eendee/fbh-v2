<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=>$this->description,
            'rating'=>isset($this->score)?$this->score:'',
            'image_url'=>$this->GetImage(),
            'reviews_count'=>isset($this->ratings)?$this->ratings:'',
            'review_type_id'=>isset($this->LoadReviewTemplateDetails)&&isset($this->ReviewTemplate)?$this->GetReviewTemplateDetails():'',
            'category' => $this->Category->name,
            'category_image_url' => $this->Category->GetImage(),
            'category_id' => $this->Category->id
        ];
    }
}
