<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->user_id,
            'fullname' => $this->Fullname(),
            'sex' => $this->sex,
            'about' => $this->about,
            'age' => $this->age,
            'rank' => $this->Rank->rank,
            'picture' => $this->ReturnPictureUrl(),
            'points' => $this->calculatePoints()
        ];
    }
    protected function calculatePoints() {
        $sum = 0;
        $rewards = $this->Rewards;
        foreach ($rewards as $key => $reward) {
            $sum += (int)$reward->number_of_points;
        }
        return $sum;
    }
}
