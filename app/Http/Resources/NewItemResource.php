<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=>$this->description,
            'image_url'=>$this->GetImage(),
            'featured'=>$this->featured,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
            'category' => $this->Category->name,
            'category_image_url' => $this->Category->GetImage(),
            'category_id' => $this->Category->id
        ];
    }
}
