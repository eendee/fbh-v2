<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserPublicProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fullname' => $this->Fullname(),
            'sex' => $this->sex,
            'about' => $this->about,
            'picture' => $this->ReturnPictureUrl(),
        ];
    }
}
