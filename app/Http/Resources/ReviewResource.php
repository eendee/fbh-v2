<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VoteResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'review_details' => $this->ReviewDetails,
            'review_image_url' => isset($this->File) ? $this->GetImage() : '',
            'votes' => isset($this->Votes) ? VoteResource::collection($this->Votes) : '',
            'overall_score' => $this->overall_score,
            'average_score' => $this->score,
            'user_id' => $this->user_id,
            'user_fullname' => isset($this->Profile) ? $this->Profile->Fullname() : '',
            'points' => $this->calculatePoints(),
            'review_type_id' => $this->review_type_id,
            'user_profile_picture' => isset($this->Profile) ? $this->Profile->ReturnPictureUrl() : '',
            'comments_count' => isset($this->comments_count) ? $this->comments_count : '',
            'comments' => isset($this->comments) ? count($this->comments) : '',
            'item' => isset($this->Profile) ? $this->Item->name : '',
            'item_id' => isset($this->Profile) ? $this->Item->id : '',
            'item_image_url' => isset($this->Profile) ? $this->Item->GetImage() : '',
            'created_at' => $this->created_at->diffForHumans(),
            'created_at2' => $this->created_at
        ];
    }
    public function calculatePoints() {
        $sum = 0;
        if($this->Profile) {
            $rewards = $this->Profile->Rewards;
            foreach ($rewards as $key => $reward) {
                $sum += (int)$reward->number_of_points;
            }
        }
        return $sum;
    }
}
