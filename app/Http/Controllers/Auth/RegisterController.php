<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Model\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Mail;
use App\Model\State;
use Session;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|min:1',
            'surname' => 'required|string|min:1',
            'age' => 'required',
            'sex' => 'required',
            'state_id' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    public function showRegistrationForm()
    {
        $states = State::GetAll();
        return view('auth.register')->with('states', $states);
    }
    public function register(Request $request)
    {
        // Laravel validation
        // $validator = $this->validator($request->all());
        $this->validator($request->all())->validate();
        // if ($validator->fails()) {
        //     $this->throwValidationException($request, $validator);
        // }
        // Using database transactions is useful here because stuff happening is actually a transaction

        DB::beginTransaction();
        try {
            $user = $this->create($request->all());
            // After creating the user send an email with the random token generated in the create method above
            // $email = new EmailVerification(new User(['email_token' => $user->email_token, 'name' => $user->name]));
            try {
                Mail::to($user->email)->send(new EmailVerification($user));
            } catch (\Throwable $th) {
                //throw $th;
            }
            DB::commit();
            //Session::flash('message', 'We have sent you a verification email, check your mail and click on the "Verify" Button!');
            return redirect('register-success')->with('message', 'We have sent you a verification email, check your mail and click on the "Verify" Button!');
        } catch (Exception $e) {
            DB::rollback();
            return back();
        }
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $username = trim(strtolower($data['surname'] . '.' . $data['firstname'] . '.' . date('YmdHis')));
        $result = User::create([
            'name' => $username,
            'email' => $data['email'],
            'role_id' => '1',
            'password' => bcrypt($data['password']),
            // 'email_token' => str_random(10),
            'email_token' => Str::random(20)
        ]);
        $p = new UserProfile();
        $p->user_id = $result->id;
        $p->age = $data['age'];
        $p->sex = $data['sex'];
        $p->firstname = $data['firstname'];
        $p->surname = $data['surname'];
        $p->picture_id = "1";
        $p->state_id = $data['state_id'];
        $p->save();

        return $result;
    }

    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability
        User::where('email_token', $token)->firstOrFail()->verified();
        return redirect('login')->with('message', 'Email successfully verified');
    }

    function RegisterSuccess()
    {
        return view('auth/register-success')->with('user', null);
    }
}
