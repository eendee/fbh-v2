<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Model\Business;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Mail\EmailVerification;
use Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLoginForm()
    {
        if (!session()->has('from')) {
            session()->put('from', url()->previous());
        }
        return view('auth.login');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->isAdmin()) {
            return redirect('admin');
        }
        $business = Business::where('user_id', $user->id)->first();
        if($business && !$business->activated) {
            Auth::logout();
            $request->session()->invalidate();
            // $request->session()->regenerateToken();
            return back()->with('message','Business account has been deactivated, please contact support');
        }
        if ($user->isBusiness()) {
            return redirect('business');
        }

        if(!$user->verified) {
            Auth::logout();
            $token = Str::random(20);
            if(!$user->email_token) {
                $user->email_token = $token;
                $user->save();
            }
            try {
                Mail::to($user->email)->send(new EmailVerification($user));
            } catch (\Throwable $th) {
                //throw $th;
            }
            return back()->with('message','Please verify your account to continue. Email verification link has been sent to your mail');
        }

        if (null != session()->get('from')) {
            return redirect(session()->pull('from', $this->redirectTo));
        }
        return redirect('/home');
    }
}
