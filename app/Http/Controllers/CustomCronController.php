<?php

namespace App\Http\Controllers;


use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class CustomCronController  extends Controller
{
    public function __construct()
    {
    }

    public function NotificationCron()
    {


        $sql = "insert into activity_notifications (activity_id,user_id,created_at,updated_at)

        select a.id as activity_id,f.user_id,CURRENT_TIMESTAMP as created_at, CURRENT_TIMESTAMP as updated_at from activities a inner join follows f on a.object_id=f.object_id inner join activity_types att on att.object_type_id=f.object_type_id and att.id=a.activity_type_id where a.dispatched = 0 and a.user_id<>f.user_id ";




        DB::beginTransaction();
        try {
            DB::insert($sql);
            DB::table('activities')->where('dispatched', 0)->update(['dispatched' => 1]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response('0', 500)
                ->header('Content-Type', 'application/json');
        }

        return response('1', 200)
            ->header('Content-Type', 'application/json');
    }
}
