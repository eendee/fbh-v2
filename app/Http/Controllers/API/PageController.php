<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Resources\PageResource;
use App\Model\Page;
use App\Model\PageSection;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    //

    /**
     * Return all pages
     * @param null
     * @return Collection of pages
     */
    function index(Request $request)
    {

        $pages = Page::All();
        return PageResource::collection($pages);
    }


    /**
     * Return page by id or slug
     * @param id or slug
     * @return single  page
     */
    function show($id = null)
    {

        $page = Page::where('id', $id)->Orwhere('slug', $id)->first();
        if ($page) {
            return  new PageResource($page);
        }
        return response(['message' => 'page not found'], Response::HTTP_NOT_FOUND);
    }

    /**
     * Return pages by section id
     * @param id o
     * @return collection of pages
     */
    function getPageBySection($id)
    {
        if($id == 3) {
            $pages = Page::where('page_section_id', $id)->orWhere('slug', 'management')->orWhere('slug', 'now-customers-can-talk')->get();
        } elseif($id == 6){
            $pages = Page::where('page_section_id', $id)->orderBy('id', 'desc')->get();
        } else {
            $pages = Page::where('page_section_id', $id)->orderBy('id', 'asc')->get();
        }
        return PageResource::collection($pages);
    }


    /**
     * Return all page sections
     * @param null
     * @return collection of  page sections
     */
    function pageSections()
    {

        $sections = PageSection::All();
        return response($sections);
    }
}
