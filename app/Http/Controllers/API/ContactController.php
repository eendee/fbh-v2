<?php

namespace App\Http\Controllers\Api;

use Input;
use App\Model\ItemSuggestion;
use App\Model\Business;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function SaveSuggestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required',
            'producer' => 'required',
            'product' => 'required',
            'locations' => 'required',
            // 'reason' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED);
        }
        $m = ItemSuggestion::Create($request->all());
        return response()->json($m, 200);
    }

    public function SaveBusinessRegistration(Request $request)
    {
        $data = $request->all();
        $v = Validator::make($data, Business::$rules);
        if ($v->fails()) {
            return response()->json($v->errors(), Response::HTTP_PRECONDITION_FAILED);
        }
        $existing = Business::where('email', $data['email'])->first();
        $user = User::where('email', $data['email'])->first();
        if ($existing != null || $user != null) {
            return response(['message' => 'email already in use'], Response::HTTP_BAD_REQUEST);
        }
        $b = Business::Create($data);
        return response()->json($b, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
