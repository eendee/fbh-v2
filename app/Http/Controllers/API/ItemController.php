<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Resources\ItemResource;
use App\Http\Resources\NewItemResource;
use App\Http\Resources\BestResource;
use App\Jobs\UserSummaryJob;
use App\Mail\DailySummary;
use App\Mail\TestMail;
use App\Model\Item;
use App\Model\Category;
use App\Model\Follow;
use App\Model\Review;
use App\Notifications\TestNotification;
use App\User;
use App\UserSummary;
use Carbon\Carbon;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends Controller
{
    //

    /**
     * Return all categories
     * @param (optional) $skip and $take
     * @return Collection of items
     */
    function index(Request $request)
    {
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        //$items=Item::where('id','>','1')->skip($skip)->take($take)->with('Category')->withCount('Reviews')->get();
        $items = Item::GetItemsPaginatedWithScores(null, $skip, $take);

        return ItemResource::collection($items);
    }

    /**
     * Return all items in a category by category Id
     * @param  string $id
     * @return items
     */

    public function categoryItems($id)
    {
        $category = Category::where('id', $id)->first();
        if ($category) {
            $items = $category->Items()->orderBy('name')->get();
            $ids = array();
            foreach ($items as $item) {
                $ids[$item->id] = $item->id;
            }
            if (count($ids)) {
                return ItemResource::collection(Item::GetItemsWithScores($ids));
            }
            return $ids;
        }
        return response(['message' => 'category not found'], Response::HTTP_NOT_FOUND);
    }

    public function getBest($id) {
        $category = Category::where('id', $id)->first();
        if($category) {
            $items = $category->Items()->orderBy('name')->get();
            foreach ($items as $item) {
                $item->reviews = $item->ReviewsWithDetails();
                $item->score = $item->returnAverageScore();
                $item->rating_count = $item->returnAverageScore(true);
            }
            $sorted = array_values(Arr::sort($items, function ($value) {
                return $value['score'];
            }));
            $r_sorted = array_reverse($sorted);
            $first_item = $r_sorted[0];
            if(count($r_sorted) >= 2) {
                // Select items with the same high score as the first item
                $high_scores = [];
                foreach ($r_sorted as $key => $value) {
                    if($first_item->score == $value->score) {
                        array_push($high_scores, $value);
                    }
                }
                // Select items with the same high score as the first item end
                // Sort the high scores according to number of ratings
                usort($high_scores, function($a, $b){
                    return $a->rating_count < $b->rating_count;
                });
                return new BestResource($high_scores[0]);
                // Sort the high scores according to number of ratings ends here
            }
            return new BestResource($first_item);
            // return response()->json(['data' => $first_item]);
        }
        return response(['message' => 'category not found'], Response::HTTP_NOT_FOUND);
    }

    public function sort_by_rating_count($item) {
        return $item;
    }

    /**
     * Return all items in a category by category Id without the ratings
     * @param  string $id
     * @return items
     */

    public function categoryItemsWithoutRating(Request $request, $id) {
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        // if (!$take || !is_numeric($take) || $take > 80) {
        if (!$take || !is_numeric($take)) {
            $take = 10;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        $category = Category::where('id', $id)->first();
        if ($category) {
            $items = $category->Items()->with('file:id,url')->with('category.file:id,url')->orderBy('name')->skip($skip)->take($take)->get();
            // $items = $category->Items()->orderBy('name')->with('file:id,url')->with('category.file:id,url')->get();
            return NewItemResource::collection(($items));
            // return response()->json(['data' => $items]);
        }
        return response(['message' => 'category not found'], Response::HTTP_NOT_FOUND);
    }



    /**
     * Return details of a partucular category by Id
     * @param  string $id
     * @return category
     */
    public function show($id)
    {
        $item = Item::where('id', $id)->withCount('Reviews')->With('ReviewTemplate')->first();
        if ($item != null) {
            $item->LoadReviewTemplateDetails = true;
            return new ItemResource($item);
        }
        return response(['message' => 'item not found'], Response::HTTP_NOT_FOUND);
    }

    public function bestInCategory()
    {

        //get best in each category..
        $items = Item::GetMaxInCategory();
        //dd($items);
        return ItemResource::collection($items);
    }

    public function search(Request $request)
    {
        $term = $request->input("s");
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        $items = Item::like('name', $term)->where('activated', '1')->skip($skip)->take($take)->get();

        $ids = array();
        foreach ($items as $item) {
            $ids[$item->id] = $item->id;
        }
        if (count($ids)) {
            return ItemResource::collection(Item::GetItemsWithScores($ids));
        }
        return $ids;
    }

    public function quickSearch(Request $request)
    {
        $term = $request->input("s");
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        $items = Item::like('name', $term)->where('activated', '1')->select(['id', 'name', 'category_id'])->skip($skip)->take($take)->get();
        return $items;
    }
}
