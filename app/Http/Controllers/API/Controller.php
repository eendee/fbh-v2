<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $reward_types = array(
        "more_than_200" => array("more_than_200", "Review is more than 200 characters", 1),
        "less_than_200" => array("less_than_200", "Review is less than 200 characters", 1),
        "liked_feedback" => array("liked_feedback", "Liked a review", 1),
        "completed_profile" => array("completed_profile", "User completed his or her profile", 5),
        "rating_product" => array("rating_product", "rated a product", 1),
        "respond_to_poll" => array("respond_to_poll", "responded to poll", 2),
        "commented_on_review" => array("commented_on_review", "Commented on reviews", 1),
        "followed_product" => array("followed_product", "Following a product", 1)
    );
}
