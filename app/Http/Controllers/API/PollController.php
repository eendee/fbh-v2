<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\AddPollResponseRequest;
use App\Http\Resources\PollQuestionResponseResource;
use App\Http\Resources\PollResource;
use Illuminate\Http\Request;
use App\Model\Poll;
use App\Model\PollQuestionResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Model\Reward;

use Auth;
use Exception;

class PollController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['show']]);
    }
    /**
     * Return all polls or activee polls
     *
     * @return Collection of polls
     */
    function index($all = null)
    {
        $polls = $all = null ? Poll::GetActivePolls() : Poll::All()->sortByDesc('updated_at');
        // $polls = Poll::All()->sortByDesc('created_at');
        $user = Auth::user();
        $responded = PollQuestionResponse::where('user_id', $user->id)->distinct('poll_id')->get('poll_id')->pluck('poll_id');

        return response(['responded' => $responded, 'polls' => PollResource::collection($polls)], Response::HTTP_OK);
    }

    /**
     * Return poll by id
     *
     * @return Response object
     */
    function GetPollById($id)
    {
        $poll = Poll::Find($id);;
        $user = Auth::user();
        $pollResponse = null;
        if ($user) {
            $pollResponse = PollQuestionResponse::GetResponseByUser($user->id, $id);
        }
        return response(['userResponses' => $pollResponse, 'poll' => new PollResource($poll)], Response::HTTP_OK);
    }


    /**
     * Return details of a partucular category by Id
     * @param  string $id
     * @return category
     */
    public function SavePollResponses(AddPollResponseRequest $request)
    {


        $data = $request->all();
        $poll = Poll::find($data['poll_id']);
        $user = Auth::user();

        if ($poll == null) {
            return response(['message' => 'Poll was not found'], Response::HTTP_NOT_FOUND);
        }
        //check poll validity here
        if ($poll->status() !== "ongoing") {
            return response(['message' => 'Poll is no longer available'], Response::HTTP_BAD_REQUEST);
        }

        if (PollQuestionResponse::HasUserRespondedToPoll($user->id, $poll->id)) {
            return response(['message' => 'You have already completed this poll'], Response::HTTP_BAD_REQUEST);
        }

        PollQuestionResponse::SaveResponse($poll->id, $user->id, $data['questions']);

        $reward = new Reward();
        $reward->user_id = $user->id;
        $reward->description = $this->reward_types["respond_to_poll"][1];
        $reward->type = $this->reward_types["respond_to_poll"][0];
        $reward->number_of_points = $this->reward_types["respond_to_poll"][2];
        $reward->save();

        return response(['message' => 'Poll successfully completed'], Response::HTTP_OK);
    }
}
