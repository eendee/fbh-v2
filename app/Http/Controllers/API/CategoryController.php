<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Resources\CategoryResource;
use App\Model\Category;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    //

    /**
     * Return all categories
     *
     * @return Collection of categories
     */
    function index()
    {
        // return CategoryResource::collection(Category::where('id', '>', '1')->with('Parent')->withCount('Items')->get());
        return CategoryResource::collection(Category::with('Parent')->withCount('Items')->get());
    }


    function getMajorCategories()
    {
        return CategoryResource::collection(Category::GetMajorCategories());
    }

    /**
     * Return details of a partucular category by Id
     * @param  string $id
     * @return category
     */
    public function show($id)
    {
        $category = Category::where('id', $id)->withCount('items')->first();

        if ($category != null) {
            return new CategoryResource($category);
        }
        return response(['message' => 'category not found'], Response::HTTP_NOT_FOUND);
    }

    public function showCategoryWithSub($id)
    {
        $category = Category::where('id', $id)->withCount('items')->first();
        $sub_categories = Category::where('parent_id', $id)->withCount('items')->get();

        if ($category) {
            $cat = new CategoryResource($category);
            $sub_categories = CategoryResource::collection($sub_categories);
            return [
                'category' => $cat,
                'sub_categories' => $sub_categories
            ];
        }
        return response(['message' => 'category not found'], Response::HTTP_NOT_FOUND);
    }


    /**
     * Return all categories under specific Id
     * @param  string $id
     * @return Collection of categories under a specific category
     */
    public function SubCategories($id)
    {
        $sub_categories = Category::where('parent_id', $id)->withCount('items')->get();
        if ($sub_categories != null) {
            return CategoryResource::collection($sub_categories);
        }
        return response(['message' => 'category not found'], Response::OK);
    }
}
