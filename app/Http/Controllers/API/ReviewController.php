<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\AddReviewRequest;
use App\Http\Requests\AddCommentRequest;
use App\Http\Requests\AddVoteRequest;
use App\Http\Resources\ReviewResource;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ReviewDetailResource;
use App\Http\Resources\RewardResource;
use App\Model\ReviewDetail;
use App\Model\Review;
use App\Model\Item;
use App\Model\Comment;
use App\Model\File;
use App\Model\Vote;
use App\Helpers\SiteHelper;
use Symfony\Component\HttpFoundation\Response;
use Auth;
use App\Model\Flag;
use App\Model\Reward;
use App\User;
use App\Model\Follow;
use App\Services\FirebaseMessagingService;


class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT', ['only' => ['addReview', 'addReviewComment', 'addReviewFlag', 'addReviewVote','revertReviewVote']]);
        $this->firebaseMessage = new FirebaseMessagingService();
    }


    /**
     * Returns the latest reviews
     * @param (optional) $skip and $take
     * @return Collection of items
     */
    function index(Request $request)
    {
        $skip = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        $r = Review::GetReviews($skip, $take, null, array('File', 'Item', 'Item.File', 'Profile', 'Profile.Picture'), array('Comments'));
        // return $r;
        // return ReviewResource::collection(Review::GetReviews($skip, $take, null, array('File', 'Item', 'Item.File', 'Profile', 'Profile.Picture'), array('Comments')));
        return ReviewResource::collection(Review::GetReviews($skip, $take, null, array('File', 'Item', 'Item.File', 'Profile', 'Profile.Picture'), array('Comments')));
    }

    /**
     * Return all reviews in an item by itemm Id
     * @param  string $id
     * @return reviews
     */

    public function itemReviews($id, Request $request)
    {
        $item = Item::where('id', $id)->first();
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }

        if ($item) {
            $reviews = Review::GetReviews($skip, $take, array('reviews.item_id' => $id), array('Item'));
            // return $reviews;
            return ReviewResource::collection($reviews);
        }
        return response(['message' => 'item not found'], Response::HTTP_NOT_FOUND);
    }

    /**
     * Return all reviews in an item by itemm Id
     * @param  string $id
     * @return reviews
     */

    public function itemReviewsByUser($id, $user_id, Request $request)
    {
        //dd($user_id);
        $item = Item::where('id', $id)->first();
        $skip = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        $conditions = array('reviews.item_id' => $id, 'reviews.user_id' => $user_id);
        if ($user_id == "-1") {
            $conditions = $conditions + array('reviews.review_type_id' => '4');
        }
        if ($item) {
            $data = Review::GetReviews($skip, $take, $conditions, array('Item'));
            return ReviewResource::collection($data);
        }
        return response(['message' => 'item not found'], Response::HTTP_NOT_FOUND);
    }

    /**
     * Return user reviews
     * @param  string $user_id
     * @return reviews
     */
    public function UserReviews(Request $request, $id)
    {
        $user = User::find($id);
        if ($user == null) {
            return response(['message' => 'User not found, click back to continue'], Response::HTTP_NOT_FOUND);
        }
        $skip = $request->input('skip');
        $take = $request->input('take');

        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        // return ReviewResource::collection(Review::GetReviews($skip, $take, null, array('File', 'Item', 'Item.File', 'Profile', 'Profile.Picture'), array('Comments')));

        // $reviews = Review::where('user_id', $user->id)->with('User')->with('ReviewDetails')->with('ReviewType')->skip($skip)->take($take)->orderBy('id', 'DESC')->get();
        $reviews = Review::where('user_id', $user->id)->with('ReviewDetails')->with('Comments')->with('ReviewType')->skip($skip)->take($take)->orderBy('id', 'DESC')->get();
        return ReviewResource::collection($reviews);
        // return $reviews;

    }


    /**
     * Return details of a partucular review by Id
     * @param  string $id
     * @return composite item
     */
    public function show($id)
    {
        $review = Review::GetSingleReview($id, ['File', 'Item', 'Profile', 'Profile.Picture', 'Votes']);
        if ($review != null) {
            $reviewDetails = ReviewDetail::where('review_id', $id)->with('ReviewTemplateDetail')->get();
            $comments = Comment::where('review_id', $id)->with('Profile.Picture')->get();
            // foreach ($comments as $key => $comment) {
            //     return $comment->User->business->Picture;
            //     // dd($value);
            //     # code...
            // }
            // return $comments[1]->User->business->ReturnPictureUrl();
            return array('data' => [
                'review' => new ReviewResource($review),
                'attribute_ratings' => ReviewDetailResource::collection($reviewDetails),
                'comments' => CommentResource::collection($comments)
            ]);
        }
        return response(['message' => 'review not found'], Response::HTTP_NOT_FOUND);
    }


    /**
     * Return all reviews in an item by itemm Id
     * @param  string $id
     * @return reviews
     */

    public function featuredReviews(Request $request)
    {
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take) || $take > 50) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        return ReviewResource::collection(Review::GetReviews($skip, $take, array('featured' => '1'), array('Item')));
    }

    function addReview(AddReviewRequest $request)
    {
        $firebaseMessage = new FirebaseMessagingService();


        $user = Auth::user();
        $data = $request->all();
        $item = Item::where('id', $data['item_id'])->first();

        if (!$item) {
            return response(['message' => 'item not found'], Response::HTTP_NOT_FOUND);
        }

        if ($data['review_type_id'] == 2) {
            if (!isset($data['file'])) {
                return response(['message' => 'Please attach a valid image'], Response::HTTP_BAD_REQUEST);
            }
        }
        $r = new Review();
        if (isset($data['review_id']) && $data['review_id'] != null) {
            $r = Review::where('id', $data['review_id'])->first();
            if (!$r->IsInEditWindow()) {
                return response(['message' => 'The edit window has expired'], Response::HTTP_BAD_REQUEST);
            }
        } else {
            $r->user_id = $user->id;
            $r->item_id = $data['item_id'];
        }
        $r->review_type_id = $data['review_type_id'];

        isset($data['location']) ? $r->location = $data['location'] : $r->location = "";
        isset($data['body']) ? $r->body = $data['body'] : $r->body = "";
        isset($data['title']) ? $r->title = $data['title'] : $r->title = "";

        if (isset($data['overall_score'])) {
            $r->overall_score = $data['overall_score'];
        }


        $file_id = null;
        if (isset($data['file'])) {

            $file = array_get($data, 'file');
            $image_extentions = File::returnImageFileTypes();
            if (!in_array($file->guessClientExtension(), $image_extentions)) {
                return response(['message' => 'Please select a valid image file'], Response::HTTP_BAD_REQUEST);
            }
            $file_id = SiteHelper::UploadFile($file, $user->id, 'review_' . $data['item_id'] . '_image');
        }
        $r->file_id = $file_id;
        $r->save();



        if (isset($data['rating'])) {

            $details = $data['rating'];
            if ($details != null && count($details)) {

                $reward = new Reward();
                $reward->user_id = $user->id;
                $reward->description = $this->reward_types["rating_product"][1];
                $reward->type = $this->reward_types["rating_product"][0];
                $reward->number_of_points = $this->reward_types["rating_product"][2];
                $reward->save();

                //clear any existing
                ReviewDetail::Where('review_id', $r->id)->delete();
                foreach ($details as $key => $val) {
                    $d = new ReviewDetail();
                    $d->item_id = $r->item_id;
                    $d->review_id = $r->id;
                    $d->review_template_detail_id = $val['review_template_detail_id'];
                    $d->rating = $val['rating'];
                    $d->save();
                }

            }
        }

        $reward = new Reward();
        $reward->user_id = $user->id;
        $countBody = $this->get_num_of_words($r->body);
        if($countBody > 200) {
            $reward->description = $this->reward_types["more_than_200"][1];
            $reward->type = $this->reward_types["more_than_200"][0];
            $reward->number_of_points = $this->reward_types["more_than_200"][2];
        } else {
            $reward->description = $this->reward_types["less_than_200"][1];
            $reward->type = $this->reward_types["less_than_200"][0];
            $reward->number_of_points = $this->reward_types["less_than_200"][2];
        }
        $reward->save();

        $followers = Follow::where('object_id', $data['item_id'])->get();
        foreach ($followers as $key => $follower) {
            $firebaseMessage->user_id = $follower->user_id;
            $firebaseMessage->notification_title = 'Review added';
            $firebaseMessage->notification_body = $user->Profile->Fullname().' made a review of '.$item->name;
            $firebaseMessage->send_notification();
        }

        //return the review with review details
        $review = Review::GetSingleReview($r->id, ['File', 'Item', 'Profile', 'Profile.Picture']);

        $reviewDetails = ReviewDetail::where('review_id', $r->id)->with('ReviewTemplateDetail')->get();
        $comments = Comment::where('review_id', $r->id)->with('Profile.Picture')->get();
        return array('data' => [
            'review' => new ReviewResource($review),
            'attribute_ratings' => ReviewDetailResource::collection($reviewDetails),
            'comments' => CommentResource::collection($comments),
            'reward' => new RewardResource($reward)
        ]);
    }

    public function get_num_of_words($string) {
        $string = preg_replace('/\s+/', ' ', trim($string));
        $words = explode(" ", $string);
        return strlen(implode("",$words));
    }


    public function addReviewComment(AddCommentRequest $request)
    {
        $firebaseMessage = new FirebaseMessagingService();

        $data = $request->all();
        $user = Auth::user();
        $review_id = $data['review_id'];
        $review = Review::where('id', $review_id)->first();
        if ($review) {
            $c =  new Comment();
            $c->user_id = $user->id;
            $c->review_id = $review_id;
            $c->object_type_id = "1";
            $c->title = "Comment on Review $review_id";
            $c->body = $data['body'];
            $c->save();

            $reward = new Reward();
            $reward->user_id = $user->id;
            $reward->description = $this->reward_types["commented_on_review"][1];
            $reward->type = $this->reward_types["commented_on_review"][0];
            $reward->number_of_points = $this->reward_types["commented_on_review"][2];
            $reward->save();

            // Notify the reviewer that someone liked their review
            $firebaseMessage->user_id = $review->user_id;
            $firebaseMessage->notification_title = 'Comment on review';
            $firebaseMessage->notification_body = $user->Profile->Fullname().' commented on your review';
            $firebaseMessage->send_notification();

            return new CommentResource($c);
        }
        return response(['message' => 'review not found'], Response::HTTP_NOT_FOUND);
    }


    public function addReviewVote(Request $request)
    {
        $firebaseMessage = new FirebaseMessagingService();


        $message = new \Stdclass();
        $message->status = '0';
        $message->message = "";

        $data = $request->all();

        $v = new Vote();

        if (Auth::Check()) {

            $user = Auth::user();

            $existing = Vote::Where('review_id', $data['review_id'])->Where('user_id', $user->id)->first();
            if (!$existing) {

                $v->review_id = $data['review_id'];
                $v->value = $data['vote'];
                $v->user_id = $user->id;
                $v->save();
                $message->status = '1';
                $message->message = "Action Successful";

                $review = Review::find($data['review_id']);
                $reward = new Reward();
                $reward->user_id = $review->user_id;
                // $reward->user_id = $user->id;
                $reward->description = $this->reward_types["liked_feedback"][1];
                $reward->type = $this->reward_types["liked_feedback"][0];
                $reward->number_of_points = $this->reward_types["liked_feedback"][2];
                $reward->save();

                // Notify the reviewer that someone liked their review
                $firebaseMessage->user_id = $review->user_id;
                $firebaseMessage->notification_title = 'Liked review';
                $firebaseMessage->notification_body = $user->Profile->Fullname().' liked your review';
                $firebaseMessage->send_notification();
            } else {
                $message->status = '-2';
                $message->message = "You have already reacted to this review";
            }
        } else {
            $message->status = '-1';
            $message->message = "Please Log in to make your vote count";
        }
        return response(['message' => $message], Response::HTTP_OK);
    }

    public function revertReviewVote(Request $request) {
        $firebaseMessage = new FirebaseMessagingService();

        $message = new \Stdclass();
        $message->status = '0';
        $message->message = "";
        $data = $request->all();
        $v = new Vote();
        if (Auth::Check()) {

            $user = Auth::user();
            $existing = Vote::Where('review_id', $data['review_id'])->where('value', $data['vote'])->Where('user_id', $user->id)->first();
            if($existing) {
                $existing->delete();
                $review = Review::find($data['review_id']);
                Reward::where('user_id', $review->user_id)->where('type', $this->reward_types["liked_feedback"][0])->first()->delete();
                // Reward::where('user_id', $user->id)->where('type', $this->reward_types["liked_feedback"][0])->first()->delete();
                $message->status = '1';
                $message->message = "Action Successful";

                // Notify the reviewer that someone disliked their review
                $firebaseMessage->user_id = $review->user_id;
                $firebaseMessage->notification_title = 'Disliked review';
                $firebaseMessage->notification_body = $user->Profile->Fullname().' disliked your review';
                $firebaseMessage->send_notification();
            } else {
                $message->status = '-2';
                $message->message = "You have not reacted to this vote";
            }
        } else {
            $message->status = '-1';
            $message->message = "Please Log in to make your vote count";
        }
        return response(['message' => $message], Response::HTTP_OK);
    }

    public function addReviewFlag(Request $request)
    {
        $firebaseMessage = new FirebaseMessagingService();

        $message = new \Stdclass();
        $message->status = '0';
        $message->message = "";


        $data = $request->all();
        $user = Auth::user();
        $review_id = $data['review_id'];
        $review = Review::where('id', $review_id)->first();
        if ($review) {
            $f =  new Flag();
            $f->user_id = $user->id;
            $f->review_id = $review_id;
            $f->body = $data['body'];
            $f->save();
            $message->status = '1';
            $message->message = "Action Successful";

             // Notify the reviewer about their review been flagged
            $firebaseMessage->user_id = $review->user_id;
            $firebaseMessage->notification_title = 'Flagged review';
            $firebaseMessage->notification_body = $user->Profile->Fullname().' Flagged your review';
            $firebaseMessage->send_notification();
            return response(['message' => $message], Response::HTTP_OK);
        }
        return response(['message' => 'review not found'], Response::HTTP_NOT_FOUND);
    }
}
