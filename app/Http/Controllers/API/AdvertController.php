<?php

namespace App\Http\Controllers\API;

use App\Model\Advert;
use App\Http\Resources\AdvertResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvertController extends Controller
{
    public function mobile_adverts(Request $request) {
        $skip = $take = $request->input('skip');
        $take = $request->input('take');
        if (!$take || !is_numeric($take)) {
            $take = 20;
        }
        if (!$skip || !is_numeric($skip)) {
            $skip = 0;
        }
        $adverts = Advert::where('position', 'app')->where('is_active', 1)->orderBy('id', 'DESC')->get();
        return AdvertResource::collection($adverts);
    }
}
