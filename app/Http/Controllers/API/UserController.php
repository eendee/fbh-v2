<?php

namespace App\Http\Controllers\API;

use App\Model\UserProfile;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\UserProfileResource;
use App\Model\File;
use App\Helpers\SiteHelper;
use App\Http\Requests\FollowItemRequest;
use App\Http\Requests\PasswordUpdateRequest;
use App\Http\Resources\FollowedItemsResource;
use App\Http\Resources\FollowedCategoryResource;
use App\Http\Resources\UserPublicProfileResource;
use App\Model\Follow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Model\Reward;


class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['index', 'show', 'GetPublicProfile']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return CategoryResource::collection(category::latest()->get());
    }

    /**
     * Display a user public  profile
     *
     * @return \Illuminate\Http\Response
     */
    public function GetPublicProfile($id = null)
    {
        if ($id == null) {
            return response(['message' => 'id cannot be null'], Response::HTTP_BAD_REQUEST);
        }
        $profile = UserProfile::where('user_id', $id)->first();
        if (!$profile) {
            return response(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
        }

        return response(['profile' => new UserPublicProfileResource($profile), 'stats' => $profile->stats()], Response::HTTP_OK);
    }


    /**
     * Display the user profile details.
     *
     * @param  \App\user auth  $user
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return  new UserProfileResource(UserProfile::where('user_id', auth()->user()->id)->first());
    }

    function statistics()
    {
        //number of reviews, rank, member since, number of comments
        return Auth::user()->Profile->stats();
    }

    function categoriesFollowed() {
        $user = Auth::user();
        $categories = Follow::getUserCategories($user->id);
        return FollowedCategoryResource::collection($categories);

        // return $categories;
    }

    function itemsFollowed()
    {
        $user = Auth::user();
        $items = Follow::getUserItems($user->id);
        return FollowedItemsResource::collection($items);
    }

    function followItem(FollowItemRequest $request)
    {
        $data = $request->all();
        $user = Auth::user();


        if (count($data['items'])) {
            $existing = Follow::where('object_type_id', 2)->where('user_id', $user->id)->get()->KeyBy('object_id')->toArray();
            // return $data['items'];
            foreach ($data['items'] as $key => $value) {
                if (in_array($value['id'], array_keys($existing))) continue;
                $array[] = array('object_id' => $value['id'], 'user_id' => $user->id, 'object_type_id' => 2, 'created_at' => date("Y-m-d H:i:s"));
            }
            // return $array;
            Follow::insert($array);

            $reward = new Reward();
            $reward->user_id = $user->id;
            $reward->description = $this->reward_types["followed_product"][1];
            $reward->type = $this->reward_types["followed_product"][0];
            $reward->number_of_points = $this->reward_types["followed_product"][2];
            $reward->save();

            return response(['message' => 'update successful', 'items' => Follow::getUserItems($user->id)], Response::HTTP_OK);
        }
    }
    function followCategory(Request $request){
        $data = $request->all();
        $user = Auth::user();

        if (count($data['categories'])) {
            $existing = Follow::where('object_type_id', 1)->where('user_id', $user->id)->get()->KeyBy('object_id')->toArray();

            foreach ($data['categories'] as $key => $value) {
                if (in_array($value['id'], array_keys($existing))) continue;
                $array[] = array('object_id' => $value['id'], 'user_id' => $user->id, 'object_type_id' => 1, 'created_at' => date("Y-m-d H:i:s"));
            }
            Follow::insert($array);

            $reward = new Reward();
            $reward->user_id = $user->id;
            $reward->description = $this->reward_types["followed_product"][1];
            $reward->type = $this->reward_types["followed_product"][0];
            $reward->number_of_points = $this->reward_types["followed_product"][2];
            $reward->save();

            return response(['message' => 'update successful', 'categories' => Follow::getUserCategories($user->id)], Response::HTTP_OK);
        }
    }

    function unfollowItem(FollowItemRequest $request)
    {
        $data = $request->all();
        $user = Auth::user();
        if (count($data['items'])) {
            foreach ($data['items'] as $key => $value) {
                $array[$value['id']] =  $value['id'];
            }
            Follow::where('user_id', $user->id)->where('object_type_id', 2)->whereIn('object_id', array_keys($array))->delete();
            Reward::where('user_id', $user->id)->where('type', $this->reward_types["followed_product"][0])->first()->delete();
            return response(['message' => 'update successful', 'items' => Follow::getUserItems($user->id)], Response::HTTP_OK);
        }
    }

    function unfollowCategory(Request $request) {
        $data = $request->all();
        $user = Auth::user();
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $file_id = null;
        if (isset($data['file'])) {
            $file = array_get($data, 'file');
            $image_extentions = File::returnImageFileTypes();
            if (!in_array($file->guessClientExtension(), $image_extentions)) {
                return response(['message' => 'invalid image file type'], Response::HTTP_BAD_REQUEST);
            }
            $file_id = SiteHelper::UploadFile($file, $user->id, 'profile_' . $user->id);
            $data['picture_id'] = $file_id;
        }
        $user->profile->update($data);
        if($user->profile->about !== null && $user->profile->picture_id > 1) {
            $found = Reward::where('user_id', $user->id)->where('type', $this->reward_types["completed_profile"][0])->first();
            if(!$found || $found === null) {
                $reward = new Reward();
                $reward->user_id = $user->id;
                $reward->description = $this->reward_types["completed_profile"][1];
                $reward->type = $this->reward_types["completed_profile"][0];
                $reward->number_of_points = $this->reward_types["completed_profile"][2];
                $reward->save();
            }

        }
        return response(['message' => 'update successful', 'profile' => new UserProfileResource($user->profile)], Response::HTTP_OK);
    }

    public function updatePassword(PasswordUpdateRequest $request)
    {
        $data = $request->validated();

        $user = Auth::user();

        if (password_verify($data['old_password'], $user->password)) {
            $user->password = bcrypt($data['password']);

            $user->save();

            return response()->json([]);
        }

        return response()->json([
            'message' => 'Password did not match'
        ], 400);
    }
}
