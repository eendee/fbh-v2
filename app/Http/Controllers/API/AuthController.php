<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\SignUpRequest;
use App\User;
use App\Model\UserProfile;
use App\Mail\EmailVerification;
use App\Model\Reward;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

use Mail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['login', 'signup']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = auth('api')->user();
        if(!$user->verified) {
            try {
                Mail::to($user->email)->send(new EmailVerification($user));
            } catch (\Throwable $th) {
                //throw $th;
            }
            return response()->json([
                'message' => 'Please verify your account to continue, We have sent you a verification email, check your mail and click on the "Verify" Button!',
                'verified' => false
            ]);
        }
        // return $user;
        return $this->respondWithToken($token);
    }

    public function signup(SignUpRequest $request)
    {
        // return response(['message' => 'We have sent you a verification email, check your mail and click on the "Verify" Button!'], Response::HTTP_CREATED);
        // return response()->json([
        //     'message' => 'Registration was successful !!, We have sent you a verification email, check your mail and click on the "Verify" Button!',
        //     'verified' => false
        // ]);
        $request['name'] = trim(strtolower($request['surname'] . '.' . $request['firstname'] . '.' . date('YmdHis')));
        $raw_pass = $request['password'];
        $request['password'] = bcrypt($raw_pass);
        $request['email_token'] = Str::random(20);
        $user = User::create($request->all());

        //initialize user profile
        $request['user_id'] = $user->id;

        $profile = UserProfile::CreateProfile($request->all());
        try {
            Mail::to($user->email)->send(new EmailVerification($user));
        } catch (\Throwable $th) {
            //throw $th;
        }
        return response()->json([
            'message' => 'Registration was successful !!, We have sent you a verification email, check your mail and click on the "Verify" Button!',
            'verified' => false
        ]);

        //send activation email
        // $email = new EmailVerification(new User(['email_token' => $user->email_token, 'name' => $user->name]));

        // $request['password'] = $raw_pass;

        // return $this->login($request);
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()->name,
            'profile' => auth('api')->user()->Profile->ProfileResource(),
            'points' => $this->calculatePoints()
        ]);
    }

    protected function calculatePoints() {
        $sum = 0;
        $rewards = auth('api')->user()->Profile->Rewards;
        foreach ($rewards as $key => $reward) {
            $sum += (int)$reward->number_of_points;
        }
        return $sum;
    }
}
