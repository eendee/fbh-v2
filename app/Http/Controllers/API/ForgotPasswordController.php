<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\User;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ForgotPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['forgot']]);
    }
    public function forgot() {
        $validator = Validator::make(request()->all(), ['email' => 'required|email']);
        // $credentials = request()->validate(['email' => 'required|email']);
        // return $credentials;
        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED);
        }
        $user = User::where('email', request()->email)->first();
        if ($user == null) {
            return response(['message' => "email doesn't exist"], Response::HTTP_BAD_REQUEST);
        }
        // Password::sendResetLink($credentials);
        Password::sendResetLink(request()->all());
        return response()->json(["msg" => 'Reset password link has been sent on your email.']);
    }
}
