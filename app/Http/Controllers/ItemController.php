<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Item;
use Input;
use Illuminate\Support\Facades\Redirect;
use View;


class ItemController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static $PerPage = 20;
    public function __construct()
    {

        //$this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Items";
        $this->menu['items'] = 'active';
        View::share('menu', $this->menu);
    }

    public function index()
    {
        $items = Item::OrderByDesc('id')->paginate(self::$PerPage);
        $this->obj->action = "Reviewable Items";
        return view('admin/item/index')->with('items', $items);
    }

    public function SearchItems()
    {
        $term = Input::get("s");
        $items = Item::where('name', 'like', "%$term%")->orWhere('description', 'like', "%$term%")->paginate(self::$PerPage);
        $this->obj->action = "Search Results for $term";

        return view('admin/item/index')->with('items', $items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $item = new Item();
        $this->obj->action = "Add New Item";
        $item->name = "";
        return view('admin/item/edit')->with('item', $item);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        $page = Item::Create($data);
        return Redirect::to('admin/items/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->obj->action = "Edit Item";
        $item = Item::find($id);
        return view('admin/item/edit')->with('item', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Item $item)
    {
        $data = Input::all();
        $item->update($data);
        return Redirect::to('admin/items/')->with('message', 'Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
