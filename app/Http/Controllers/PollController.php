<?php

namespace App\Http\Controllers;


use Input;
use App\User;
use App\Model\Poll;
use App\Model\PollQuestion;
use App\Model\PollQuestionOption;
use App\Model\PollQuestionResponse;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use View;
use App\Services\FirebaseMessagingService;

class PollController extends AdminController
{
    public $firebaseMessage;
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Polls";
        $this->menu['polls'] = 'active';
        View::share('menu', $this->menu);
        $this->firebaseMessage = new FirebaseMessagingService();
    }

    public function Index()
    {

        $this->obj->action = "View Polls";
        $polls = Poll::All();
        //dd($categories);
        return view('admin/poll/index')->with('polls', $polls);
    }

    public function create()
    {
        //return "Here";
        $poll = new Poll();
        $poll->name = "";
        //$poll->slug="url friendly name";
        $this->obj->action = "Add New Poll";
        return view('admin/poll/edit')->with('poll', $poll);
    }

    public function edit($id)
    {
        $this->obj->action = "Edit Poll";
        $poll = Poll::find($id);
        return view('admin/poll/edit')->with('poll', $poll);
    }

    public function delete($id)
    {
        $poll = Poll::find($id);
        return view('admin/poll/delete')->with('poll', $poll);
    }

    public function update(Poll $poll)
    {

        $data = Input::all();
        //dd($data);
        $poll->update($data);
        return Redirect::to(route('poll.index'));
    }

    public function store()
    {
        $data = Input::all();
        $poll = Poll::Create($data);


         // Notify users about new polls

         $this->firebaseMessage->notification_title = 'New Polls';
         $this->firebaseMessage->notification_body = 'A poll has been created, please check your app to participate in the poll';
         $this->firebaseMessage->send_general_message();
        return Redirect::to(route('poll.index'));
    }

    public function destroy($id)
    {
        //dd($id);
        PollQuestionOption::MassDeleteByPollId($id);
        PollQuestion::MassDeleteByPollId($id);
        Poll::find($id)->delete();

        return Redirect::to('admin/poll/')->with('message', 'Poll Successfully Deleted ');
    }

    public function PollQuestions($id)
    {
        //dd($id);
        $poll = Poll::where('id', $id)->with('Questions.Options')->first();
        //dd($poll);
        //dd($poll);
        if ($poll == null) {
            //catch
        }
        $question = new PollQuestion();

        $questions = $poll->Questions;
        //dd($questions);

        return view('admin/poll/questions')->with('poll', $poll)->with('questions', $questions)->with('question', $question);
    }

    public function EditQuestion($Pollid, $QuestionId = null)
    {
        $question = null;
        $options = null;
        $poll = Poll::find($Pollid);
        //dd($poll)
        if ($poll == null) {
            //error;
            exit();
        }
        if ($QuestionId != null) {
            $question = PollQuestion::find($QuestionId);
            $options = $question->Options;
        } else {
            $question = new PollQuestion();
            $question->id = null;
            $question->name = null;
        }
        $this->obj->action = "Add/Edit Questions for Poll: " . $poll->name;
        //dd($question);
        return view('admin/poll/questions-edit')->with('poll', $poll)->with('question', $question)->with('options', $options);
    }

    public function SaveQuestion($id = null)
    {
        $data = Input::all();
        //dd($data);
        $q = PollQuestion::find($id);
        if ($q == null) {
            $q = PollQuestion::Create($data);
        } else {
            $q->body = $data['body'];
            $q->update();
        }

        if (isset($data['option'])) {
            PollQuestionOption::MassDeleteByQuestionId($q->id);
            $array = array();
            foreach ($data['option'] as $key => $value) {
                if ($value != null) {
                    $array[] = array('poll_id' => $q->poll_id, 'poll_question_id' => $q->id, 'body' => $value);
                }
            }
            PollQuestionOption::insert($array);
        }
        return Redirect::to(route('poll.questions', $q->poll_id));
    }

    public function DeleteQuestionPreview($id)
    {
        $q = PollQuestion::find($id);
        return view('admin/poll/delete-question')->with('question', $q);
    }

    public function DeleteQuestion($questionId)
    {
        if (true) {
            $q = PollQuestion::find($questionId);
            PollQuestion::where('id', $questionId)->delete();
            PollQuestionOption::MassDeleteByQuestionId($questionId);
            return Redirect::to(route('poll.questions', $q->poll_id))->With('message', 'operation successful');
        }
    }

    public function PollResponses($Pollid)
    {
        $poll = Poll::find($Pollid);
        if ($poll == null) {
            exit();
            //handle later
        }
        $questions = $poll->Questions;
        //dd($questions);

        $responses = User::GetPollResponses($Pollid);

        $array = array();
        foreach ($responses as $r) {
            $array[$r->email][$r->qid] = $r;
        }
        return view('admin/poll/poll-results')->with('poll', $poll)
            ->with('questions', $questions)
            ->with('responses', $array);
    }
}
