<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Helpers\SiteHelper;

use Input;
use App\File;
use App\Mail\DailySummary;
use App\User;
use App\UserProfile;
use App\Model\PageMeta;
use App\Model\Review;
use App\Model\Comment;
use App\Model\Category;
use App\Model\Vote;
use App\Model\State;
use App\Model\Item;
use App\Model\Follow;
use App\Model\Reward;
use View;
use Hash;
use stdClass;
use App\Model\ActivityNotification;
use App\UserSummary;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserController extends MyController
{

    protected $user;
    public function __construct()
    {
        //bounce non logged in users at this point
        $this->middleware('auth')->except(['sendSummary']);
        parent::__construct();
        $this->meta = new PageMeta();
        $this->meta->description = "Share your product experiences, review products and services.";
        $this->meta->title = "My Dashboard";
        $this->menu['user'] = $this->active_class = "active";
        View::share('menu', $this->menu);
        View::share('meta', $this->meta);
    }


    public function index()
    {
        //shows the profile

        $user = Auth::user();

        if ($user->isBusiness()) {
            return Redirect::to('business');
        }
        $data = User::GetActivityLog($user->id);

        $activities = $data;


        $stats = new stdClass();
        $stats->reviews = Review::getUserReviewCount($user->id);
        $stats->comments = Comment::getUserCommentCount($user->id);
        $stats->votes = Vote::getUserVoteCount($user->id);
        $stats->TotalVotes = Vote::getVotesForUser($user->id);
        $stats->points = $this->calculatePoints($user);
        $stats->badges = $this->getPoints($stats->points);

        $reviews = Review::where('user_id', $user->id)->paginate(10);

        //process individual notification
        $notifications_count = ActivityNotification::where('user_id', $user->id)->where('viewed', '0')->count();

        return view('user.index')->with('user', $user)->with('activities', $activities)
            ->with('profile', $user->profile)->with('stats', $stats)->with('notifications_count', $notifications_count)->with('reviews', $reviews);
    }
    protected function calculatePoints($user) {
        $sum = 0;
        $rewards = $user->Profile->Rewards;
        foreach ($rewards as $key => $reward) {
            $sum += (int)$reward->number_of_points;
        }
        return $sum;
    }
    protected function getPoints($points) {
        if($points <= 250) {
            return 1;
        } else if($points > 250 && $points <= 500 ) {
            return 2;
        } else if($points > 500 && $points <= 1500) {
            return 3;
        } else if($points > 1500 && $points <= 5000) {
            return 4;
        } else if($points > 5000 ) {
            return 5;
        }
    }

    function ViewNotifications()
    {
        $user = Auth::user();
        $notifications = ActivityNotification::where('user_id', $user->id)->where('viewed', '0')->OrderBy('id')->with('Activity')->get();
        $count = count($notifications);
        ActivityNotification::where('user_id', $user->id)->where('viewed', '0')->update(array('viewed' => '1'));
        //update their status

        //dd($notifications);
        return view('user.notifications')->with('user', $user)->with('notifications', $notifications);
    }

    public function profile()
    {
        $user = Auth::user();
        $p = $user->profile;
        $this->meta->title = "Update Profile";
        $states = State::GetAll();
        //dd($states);
        return view('user.profile')->with('user', $user)->with('profile', $p)->with('states', $states);
    }

    function FollowCategories()
    {
        $user = Auth::user();
        $categories = Category::getCategoresThatHaveItems()->sortBy('name');
        $mappings = Follow::where('object_type_id', '2')->where('user_id', $user->id)->get(['object_id'])->KeyBy('object_id')->toArray();
        //dd($mappings);
        $items = Item::whereIn('id', array_keys($mappings))->with('Category')->get();
        $catsCount = array();
        foreach ($items as $i) {
            $catsCount[$i->Category->id][] = $i->id;
        }
        //dd($catsCount);
        return view('user/category-follow')->with('user', $user)->with('categories', $categories)->with('catsCount', $catsCount);
    }

    public function FollowObjects($id = 0)
    {

        $user = Auth::user();
        $items = Item::where('category_id', $id)->get();
        $mappings = Follow::where('object_type_id', '2')->where('user_id', $user->id)->get()->KeyBy('object_id')->toArray();
        return view('user/item-follows')->with('user', $user)->with('items', $items)->with('mappings', $mappings)->with('id', $id);
    }

    function SaveFollows($id = null)
    {

        $data = Input::All();
        $user = Auth::user();

        $items_in_cat = Item::where('category_id', $id)->get()->KeyBy('id')->toArray();
        $un_follow_count = Follow::where('user_id', $user->id)->where('object_type_id', $data['object_type_id'])->whereIn('object_id', array_keys($items_in_cat))->delete();

        for ($i=0; $i < $un_follow_count; $i++) {
            $followedProduct = Reward::where('user_id', $user->id)->where('type', $this->reward_types["followed_product"][0])->first();
            if($followedProduct) {
                $followedProduct->delete();
            }
        }
        if(array_key_exists('value', $data)) {
            $array = array();
            if (count($data['value'])) {
                foreach ($data['value'] as $item_id) {
                    $array[] = array('object_id' => $item_id, 'user_id' => $user->id, 'object_type_id' => $data['object_type_id'], 'created_at' => date("Y-m-d H:i:s"));

                    $reward = new Reward();
                    $reward->user_id = $user->id;
                    $reward->description = $this->reward_types["followed_product"][1];
                    $reward->type = $this->reward_types["followed_product"][0];
                    $reward->number_of_points = $this->reward_types["followed_product"][2];
                    $reward->save();
                }
                Follow::insert($array);
            }
        }

        return Redirect::to('user/follow-categories/')->with('message', 'Update Successful');
    }


    public function update()
    {
        $user = Auth::user();
        $data = Input::All();
        $profile = $user->profile;
        //dd($data);

        $file_id = null;
        if (isset($data['file'])) {
            $file = array_get($data, 'file');

            $file_id = SiteHelper::UploadFile($file, $user->id, 'profile_' . $user->id);
            $profile->picture_id = $file_id;
        }

        $profile->surname = $data['surname'];
        $profile->firstname = $data['firstname'];
        if (isset($data['sex'])) {
            $profile->sex = $data['sex'];
        }
        if (isset($data['state_id'])) {
            $profile->state_id = $data['state_id'];
        }
        $profile->age = $data['age'];
        $profile->about = $data['about'];

        $profile->save();
        if($profile->about !== null && $profile->picture_id > 1) {
            $found = Reward::where('user_id', $user->id)->where('type', $this->reward_types["completed_profile"][0])->first();
            if(!$found || $found === null) {
                $reward = new Reward();
                $reward->user_id = $user->id;
                $reward->description = $this->reward_types["completed_profile"][1];
                $reward->type = $this->reward_types["completed_profile"][0];
                $reward->number_of_points = $this->reward_types["completed_profile"][2];
                $reward->save();
            }

        }

        return Redirect::to('user')->with('message', 'Update Successful');
    }


    public function Password()
    {
        $user = Auth::user();
        $p = $user->profile;
        $this->meta->title = "Change Password";
        return view('user.change-password')->with('user', $user)->with('profile', $p);
    }


    public function SavePassword()
    {

        $user = Auth::user();
        $data = Input::All();
        //dd($data);
        if ($data['newpassword'] !== $data['newpasswordconfirm']) {
            return redirect()->back()->with('message', 'the new password and the confirm password fields must be the same value');
        }
        if (Hash::check($data['oldpassword'], $user->getAuthPassword())) {
            $user->password = Hash::make($data['newpassword']);
            $user->save();
            if ($user->role_id == "3") {
                return redirect('business')->with('message', 'Password Successfully Changed');
            }
            return Redirect::to('user')->with('message', 'Update Successful');
        } else {
            return redirect()->back()->with('message', 'What you supplied as the current password did not match what we have as your current password');
        }
    }

    public function sendSummary() {
        $users = User::whereIn('email', ['chiugokanu@gmail.com', 'dosky.ogbo91@gmail.com'])->get();

        $startDate = Carbon::now()->addHours(-24);
        $reviews = Review::whereDate('created_at', '>=', $startDate)->get();
        $groupedReviews = $reviews->groupBy('item_id');

        Log::info('Total of ' . count($reviews) . ' since ' . $startDate->toString());

        foreach ($users as $user) {
            $followItems = Follow::getUserItems($user->id);

            $userReviews = [];

            foreach ($followItems as $followItem) {
                $item = $followItem->item;

                if ($item == null) {
                    continue;
                }

                if (!Arr::has($groupedReviews, $item->id)) {
                    continue;
                }

                $itemReviews = $groupedReviews[$item->id];

                if (count($itemReviews) > 0) {
                    array_push($userReviews, new UserSummary($item, $itemReviews));
                }
            }

            if (count($userReviews) > 0) {
                Log::info('Total of ' . count($userReviews) . ' for ' . $user->email);

                Log::info('Sending email to ' . $user->email);

                Mail::to($user)->send(new DailySummary($user, $userReviews));
            }
        }

        return response()->json([
            'message' => 'Ok'
        ]);
    }
}
