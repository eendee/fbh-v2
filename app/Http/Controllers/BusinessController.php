<?php

namespace App\Http\Controllers;


use Input;
use App\Model\Business;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use View;
use App\User;
use App\Mail\BusinessAccountCreated;
use App\Mail\BusinessAccountDeactivated;
use App\Mail\BusinessAccountReactivated;
use App\Model\Item;
use Mail;
use App\Model\BusinessItem;

class BusinessController  extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "businesses";
        $this->menu['businesses'] = 'active';
        View::share('menu', $this->menu);
    }

    public function Index()
    {

        $this->obj->action = "View Businesses";
        $businesses = Business::orderBy('id', 'DESC')->paginate(10);
        //dd($messages);
        return view('admin/businesses/index')->with('businesses', $businesses);
    }

    public function view($id)
    {
        $b = Business::find($id);
        $this->obj->action = "View Business Details for " . $b->name;
        $items = BusinessItem::where('business_id', $id)->with('Item')->get();
        //dd($items);
        return view('admin/businesses/view')->with('b', $b)->with('items', $items);

        //to create
        //check if email already exists
        //then creat an accont with the email
        //then send email notification to address with further details.
        //when they log in
        // show them products associated with them.

    }


    function CreateAccount($id)
    {
        // pass = 'dsqPJ0ifR0'
        $b = Business::find($id);

        if ($b->activated) {
            return redirect()->back()->with('message', "Account already activated");
        }
        $exists = User::where('email', $b->email)->where('role_id', '3')->first();

        if ($exists) {
            // $exists->delete();
            if($b && $b->activated) {
                return redirect()->back()->with('message', "Account already created");
            } else {
                $b->activated = "1";
                $b->save();
                Mail::to($b->email)->send(new BusinessAccountReactivated($b->name));
                return Redirect::to('admin/businesses/')->with('message', 'Account activated');
            }
        }
        //safe to create account
        $_name = $b->name;
        $b->activated = "1";
        $b->save();
        // $b->name = $b->email;

        $pass = User::GetRandomPassword(10, "lower_case,upper_case,numbers");
        $user = User::CreateUser($b->toArray() + array('username' => $b->email, 'password' => $pass));

        $user->role_id = "3";
        $user->save();

        // $b->name = $_name;
        $b->user_id = $user->id;
        $b->save();
        // dd($pass);
        Mail::to($b->email)->send(new BusinessAccountCreated($b, $pass));
        //return (new BusinessAccountCreated($b,$pass))->render();
        return Redirect::to('admin/businesses/')->with('message', 'Account created');
    }

    function DeactivateAccount($id) {
        $b = Business::find($id);

        if ($b->activated) {
            $user = User::where('email', $b->email)->where('role_id', '3')->first();
            $b->activated = "0";
            $b->save();
            if($user) {
                Mail::to($b->email)->send(new BusinessAccountDeactivated($b->name));
            }
            return Redirect::to('admin/businesses/')->with('message', 'Account deactivated');
        } else {
            return Redirect::to('admin/businesses/')->withErrors('message', 'Account deactivation failed');
        }

    }

    function AddItemsToBusiness($id, Request $request)
    {
        $b = Business::find($id);
        if($request->item_search) {
            $items = Item::where('name', 'like', "%{$request->item_search}%")
                            ->get();
        } else {
            //get all items
            //load
            $items = Item::All();
            //dd($mappings);
            //dd( array_keys($mappings));
        }
        $mappings = BusinessItem::where('business_id', $id)->get()->KeyBy('item_id')->toArray();
        return view('admin/businesses/business-items')->with('b', $b)->with('items', $items)->with('mappings', $mappings);
    }

    function SaveItemsToBusiness($id)
    {
        //delete all first;
        $data = Input::All();
        //dd($data);
        BusinessItem::where('business_id', $id)->delete();
        $array = array();
        if (count($data['value'])) {
            foreach ($data['value'] as $item_id) {
                $array[] = array('item_id' => $item_id, 'business_id' => $id);
            }
            BusinessItem::insert($array);
        }
        return Redirect::to('admin/businesses/view/' . $id)->with('message', 'Update  Successful');
    }
}
