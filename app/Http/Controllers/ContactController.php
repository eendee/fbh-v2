<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\User;
use App\Model\Message;
use App\Model\ItemSuggestion;
use App\Model\Business;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Model\PageMeta;
use App\Model\AdvertRequest;
use View;


class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $meta;
    public function __construct()
    {
        $this->meta = new PageMeta();
        $this->meta->description = "Share your product experiences, review products and services.";
        $this->menu['contact-us'] = "active";
        View::share('meta', $this->meta);
        View::share('menu', $this->menu);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ContactUs()
    {
        $m = new Message();
        $this->meta->title = "Contact us";
        return view('front/contact/contact')->with('m', $m);
    }

    public function SaveContactUs()
    {
        $data = Input::all();
        $m = Message::Create($data);
        $this->meta->title = "Contact us";
        //dd($data);
        $this->menu['contact-us'] = $this->active_class;
        return view('front/contact/saved')->with('m', $m)->with('menu', $this->menu);
    }


    public function Suggest()
    {
        $this->meta->title = "List a new product or service";
        $m = new ItemSuggestion();
        return view('front/contact/suggest')->with('m', $m)->with('menu', $this->menu);
    }

    public function SaveSuggestion()
    {
        $data = Input::all();
        $this->meta->title = "List an Item";
        $m = ItemSuggestion::Create($data);
        return view('front/contact/saved')->with('m', $m)->with('menu', $this->menu);
    }


    public function SubmitAdvert()
    {
        $data = Input::all();
        //dd($data);

        $m = AdvertRequest::Create($data);
        $this->meta->title = "Message Received!";
        return view('front/contact/saved')->with('menu', $this->menu)->with('m', $m);
    }

    public function RegisterBusiness()
    {
        $m = new Business();
        $this->meta->title = "Register Your Business With Us";
        return view('front/contact/business-registration')->with('business', $m)->with('message', null);
    }

    public function SaveBusinessRegistration()
    {
        $data = Input::all();

        //dd($data);
        $v = Validator::make($data, Business::$rules);
        if ($v->fails()) {
            //dd($v->errors());
            return redirect()->back()->with('message', $v->errors()->first())->withInput();
        }

        $existing = Business::where('email', $data['email'])->first();
        $user = User::where('email', $data['email'])->first();
        if ($existing != null || $user != null) {
            return redirect()->back()->with('message', 'email already in use')->withInput();
        }
        $this->meta->title = "Business details received!";
        $b = Business::Create($data);
        return view('front/contact/business-form-saved')->with('business', $b)->with('menu', $this->menu);
    }
}
