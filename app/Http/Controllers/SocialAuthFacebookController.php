<?php

namespace App\Http\Controllers;

use App\Model\UserProfile;
use Illuminate\Http\Request;
use Socialite;
use Input;
use App\Services\SocialFacebookAccountService;

class SocialAuthFacebookController extends Controller
{
  /**
   * Create a redirect method to facebook api.
   *
   * @return void
   */
  public function redirect()
  {
    //return Socialite::driver('facebook')->redirect();
    return Socialite::driver('facebook')->fields([
      'first_name', 'last_name', 'email', 'gender', 'birthday'
    ])->scopes([
      'email',
      //'user_birthday',
      //'user_gender'
    ])->redirect();
  }

  /**
   * Return a callback method from facebook api.
   *
   * @return callback URL from facebook
   */
  public function callback(SocialFacebookAccountService $service)
  {

    $data = Input::all();
    if (isset($data['error_code']) || isset($data['error'])) {
      $message = "something went wrong with the login request";
      if (isset($data['error_message'])) {
        $message = $data['error_message'];
      }
      if (isset($data['error_description'])) {
        $message = $data['error_description'];
      }

      //dd($data);
      return redirect()->to('/login')->with('message', $message);
    } else {

      try {
        //$u=Socialite::driver('facebook')->user();
        $driver = Socialite::driver('facebook')
          ->fields([
            'first_name', 'last_name', 'email',
            //'gender', 
            //'birthday'
          ]);
        $driver_user = $driver->stateless()->user();
        $data = $driver_user->user;
        //dd($driver_user);
        if ($driver_user->name == null) {
          $driver_user->name = $driver_user->email;
        }
        $user = $service->createOrGetUser($driver_user);


        auth()->login($user);

        //create user profile if not existing
        $profile = UserProfile::where('user_id', $user->id)->first();
        //dd($profile);
        if ($profile == null) {
          $p = new UserProfile();
          $p->user_id = $user->id;
          if (isset($data)) {
            if (isset($data['birthday'])) {
              $p->age = $data['birthday'];
            }
            if (isset($data['first_name'])) {
              $p->firstname = $data['first_name'];
            }
            if (isset($data['last_name'])) {
              $p->surname = $data['last_name'];
            }
            if (isset($data['gender'])) {
              $p->sex = $data['gender'];
            } else {
              $p->sex = '-';
            }
            if (isset($data['age'])) {
              $p->age = $data['age'];
            } else {
              $p->age = '01/01/2000';
            }
          }

          $p->picture_id = "1";
          $p->save();

          //take to profile edit
          return redirect()->to('/user/profile')->with('message', 'Please take a moment to update your profile.');
        }
        return redirect()->to('/home');
      } catch (Exception $e) {
        return redirect()->to('/login')->with('message', 'Something went wrong with the login process. ' . $e->getMessage());
      }
    }
  }
}
