<?php

namespace App\Http\Controllers;


use Input;
use App\Model\Category;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use View;

class CategoryController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Categories";
        $this->menu['categories'] = 'active';
        View::share('menu', $this->menu);
    }

    public function Index()
    {

        $this->obj->action = "View Categories";
        $categories = Category::with('Parent')->get();
        //dd($categories);
        return view('admin/category/index')->with('cats', $categories);
    }

    public function create()
    {
        //return "Here";
        $cat = new Category();
        $cat->name = "";
        $cat->slug = "url friendly name";
        $this->obj->action = "Add New Category";
        return view('admin/category/edit')->with('cat', $cat);
    }

    public function edit($id)
    {
        $this->obj->action = "Edit Category";
        $cat = Category::find($id);
        return view('admin/category/edit')->with('cat', $cat);
    }

    public function delete($id)
    {
        $category = Category::find($id);
        return view('admin/category/delete')->with('category', $category);
    }

    public function update(Category $category)
    {

        $data = Input::all();
        //dd($data);
        $category->update($data);
        return Redirect::to(route('categories.index'));
    }

    public function store()
    {
        $data = Input::all();
        $cat = Category::Create($data);
        return Redirect::to(route('categories.index'));
    }

    public function destroy(Category $category)
    {

        $category->delete();
        return Redirect::to('admin/categories/')->with('message', 'Category Successfully Deleted ');
    }
}
