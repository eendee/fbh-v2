<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\SiteHelper;
use App\Model\Advert;
use App\Model\File;
use Input;

use Illuminate\Support\Facades\Storage;

class AdvertController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adverts = Advert::orderBy('id', 'DESC')->paginate(10);
        foreach ($adverts as $key => $advert) {
            $this->checkPosition($advert);
        }
        return view('admin/advert/index')->with('adverts', $adverts);
    }

    public function checkPosition($advert) {
        switch ($advert->position) {
            case 'top':
                $advert->position = 'Top of Page';
                break;
            case 'left':
                $advert->position = 'Left Side of Page';
                break;
            case 'right':
                $advert->position = 'Right Side of Page';
                break;
            case 'bottom':
                $advert->position = 'Bottom of Page';
            default:
                $advert->position = 'Show in App';
                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/advert/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'title' => 'required|max:255',
            'file' => 'required',
            'position' => 'required',
        ]);
        $filename = time().'_'.$request->file('file')->getClientOriginalName();
        $path = $request->file('file')->storeAs('uploads/ad', $filename, 'upload');
        // $path = $request->file('file')->storeAs('ads', $filename, 'upload');
        $advert = new Advert([
            'title' => $request->title,
            'position' => $request->position,
            'image' => $path,
            'description' => $request->description,
            'url' => $request->url
        ]);
        $advert->save();
        if($advert) {
            return redirect()->back()->with('message','Advert uploaded successfully!!');
        }
        return redirect()->back()->withErrors('message', 'An error occurred with upload');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advert = Advert::find($id);
        // $visibility = Storage::getVisibility(public_path($advert->url));
        // dd($visibility);
        return view('admin/advert/edit')->with('advert', $advert);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'position' => 'required',
        ]);
        $advert = Advert::find($id);
        if($request->file) {
            Storage::disk('upload')->delete($advert->image);
            $filename = time().'_'.$request->file('file')->getClientOriginalName();
            $path = $request->file('file')->storeAs('uploads/ad', $filename, 'upload');
            $advert->image = $path;
        }
        $advert->url = $request->url;
        $advert->title = $request->title;
        $advert->description = $request->description;
        $advert->position = $request->position;
        if($advert->save()) {
            return redirect()->back()->with('message', 'Advert Updated successfully !');
        }
        return redirect()->back()->withErrors('message','Advert could not be updated, try again, if error persists contact support');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advert = Advert::find($id);
        // $store = Storage::disk('upload')->delete('uploads/ad'.$advert->image);
        Storage::disk('upload')->delete($advert->image);
        if($advert->delete()){
            return redirect()->back()->with('message', 'Advert deleted !');
        }
        return redirect()->back()->withErrors('message','Advert could not be deleted, try again, if error persists contact support');
    }

    public function makeActive(Request $request,  $id) {

        $advert = Advert::find($id);
        if($request->makeactive) {
            $advert->is_active = 0;
        } else {
            $advert->is_active = 1;
        }
        $advert->save();
        return redirect()->back();

    }
}
