<?php

namespace App\Http\Controllers;


use Input;
use App\Model\Review;
use App\Model\Item;
use App\Model\ReviewTemplate;
use App\Model\ReviewTemplateDetail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\ReviewDetail;
use DB;
use App\User;
use View;
use App\Model\Reward;


class ReviewController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Reviews";
        $this->menu['reviews'] = 'active';
        View::share('menu', $this->menu);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public static $PerPage = 10;
    public function Index()
    {
        $this->obj->action = "All Reviews Submitted";
        $reviews = Review::Where('user_id', '!=', -1)->OrderByDesc('id')
            ->with('User')->with('Profile')->with('ReviewDetails')->with('ReviewType')->paginate(self::$PerPage);

        return view('admin/review/index')->with('reviews', $reviews);
    }

    public function UserReviews($id)
    {
        $user = User::find($id);
        if ($user == null) {
            echo "User not found, click back to continue"; //TODO::show more friendly page.
        }
        $this->obj->action = "All Reviews by " . $user->Profile->Fullname();
        $reviews = Review::where('user_id', $user->id)->with('User')->with('ReviewDetails')->with('ReviewType')->paginate(self::$PerPage);

        return view('admin/review/index')->with('reviews', $reviews);
    }

    public function FeaturedReviews()
    {
        $this->obj->action = "All Featured Reviews";
        $reviews = Review::where('featured', true)->with('ReviewDetails')->with('ReviewType')->with('User')->paginate(self::$PerPage);;

        return view('admin/review/index')->with('reviews', $reviews);
    }


    public function SearchReview()
    {
        $term = Input::get("s");
        $reviews = Review::OrderByDesc('id')->where('title', 'like', "%$term%")->orWhere('body', 'like', "%$term%")->with('User')->with('ReviewDetails')->with('ReviewType')->paginate(self::$PerPage);
        $this->obj->action = "Search Results for $term";
        return view('admin/review/index')->with('reviews', $reviews)->with('term', $term);
    }

    public function Edit($id)
    {

        $r = Review::find($id);
        if ($r == null) {
        }
        $this->obj->action = "Edit Review";
        return view('admin/review/edit')->with('review', $r)
            ->with('ti', $r->Item->ReviewTemplate->ReviewTemplateItems);
    }


    public function update($id)
    {

        $data = Input::all();
        $r = Review::find($id);
        $r->activated = $data['activated'];
        $r->update($data);

        return Redirect::to("admin/reviews/")->with('message', 'Update Successful!');
    }

    public function Add($id)
    {

        $i = Item::find($id);
        if ($i == null) {
        }
        $r = Review::where('user_id', -1)->where('item_id', $id)->first();
        $review_details = new ReviewDetail();
        if ($r == null) {
            $r = new Review();
            $r->title = "";
            $r->body = "";
        } else {
            $review_details = $r->ReviewDetails;
        }

        //dd($review_details);
        $this->obj->action = "Edit Confidence Score for " . $i->name;

        return view('admin/review/add')->with('item', $i)->with('rd', $review_details)
            ->with('ti', $i->ReviewTemplate->ReviewTemplateItems)->with('review', $r);
    }

    public function SaveInsert($id)
    {

        $r = Review::where('user_id', -1)->where('item_id', $id)->first();
        $data = Input::all();
        if ($r == null) {
            $r = new Review();
            $r->user_id = -1;
            $r->item_id = $id;
            $r->review_type_id = 4;
        }
        $title = "";
        $location = "";
        $body = "";
        if (isset($data['body'])) {
            $body = $data['body'];
        }
        if (isset($data['title'])) {
            $title = $data['title'];
        }
        $r->body = $body;
        $r->title = $title;
        if (isset($data['overall_score'])) {
            $r->overall_score = $data['overall_score'];
        }
        $r->save();

        if (isset($data['rating'])) {

            $details = $data['rating'];
            if ($details != null && count($details)) {
                //clear existing
                //delete from review_Details where user_id=-1 and item_id=$item_id

                ReviewDetail::MassDeleteByReviewId($r->id);
                foreach ($details as $key => $val) {
                    $d = new ReviewDetail();
                    $d->item_id = $id;
                    $d->review_id = $r->id;
                    $d->review_template_detail_id = $key;
                    $d->rating = $val;
                    $d->save();
                }
            }
        }
        return Redirect::to("admin/items/")->with('message', 'Update Successful!');
    }

    public function get_num_of_words($string) {
        $string = preg_replace('/\s+/', ' ', trim($string));
        $words = explode(" ", $string);
        return strlen(implode("",$words));
    }

    public function ToggleActivation($id)
    {


        $r = Review::find($id);
        $r->activated = !$r->activated;
        $r->save();
        return Redirect::to("admin/reviews/")->with('message', 'Update Successful!');
    }


    public function ToggleFeatured($id)
    {


        $r = Review::find($id);
        $r->featured = !$r->featured;
        $r->save();
        return Redirect::to("admin/reviews/")->with('message', 'Update Successful!');
    }


    public function Delete($id)
    {
        $r = Review::find($id);
        // dd($r->user);
        $reward = Reward::where('user_id', $r->user_id)->where('type', $this->reward_types["rating_product"][0])->first();
        if($reward) {
            $reward->delete();
        }
        //delete assocaited ratings;
        $ratings = ReviewDetail::MassDeleteByReviewId($id);
        Review::destroy($id);

        return Redirect::to("admin/reviews/")->with('message', "Delete Successful");
    }
}
