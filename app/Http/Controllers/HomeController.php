<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Redirect;
use App\Model\PageMeta;
use App\Model\Setting;
use App\Model\Page;
use App\Model\Category;
use App\Model\Item;
use App\Model\Review;
use App\Model\Comment;
use App\Model\Flag;
use App\Model\File;
use App\Model\ReviewDetail;
use App\Helpers\SiteHelper;
use App\Model\AjaxResponse;
use App\Model\Vote;
use App\Model\UserProfile;
use App\Model\Poll;
use App\Model\AdvertRequest;
use App\User;
use App\Model\Follow;
use App\Model\ItemSuggestion;
use App\Model\PollQuestionResponse;
use App\Model\Reward;
use App\Model\Advert;
use View;
use Input;
use Auth;
use stdClass;
use App\Model\BusinessItem;
use App\Model\Business;
use App\Services\FirebaseMessagingService;
use Illuminate\Support\Facades\DB;



class HomeController extends Controller
{

    public $meta;
    public $menu;
    public $active_class;
    public $firebaseMessage;

    public function __construct()
    {

        $this->middleware('auth')->only('FlagReview', 'review', 'saveComment', 'WriteReview', 'RespondToPoll', 'SubmitPoll');
        //parent::__construct();
        $this->meta = new PageMeta();
        $this->meta->description = "Share your product experiences, review products and services.";
        View::share('meta', $this->meta);
        $this->firebaseMessage = new FirebaseMessagingService();
    }

    public function index()
    {
        // dd(DB::connection()->getDatabaseName());
        $settings = Setting::Get();
        $slider = $settings[2]->value;
        $sliders = Page::getSliders($slider);
        $this->meta->title = "The Reality";
        $this->menu['home'] = $this->active_class;
        $this->meta->load_app_js = 1;
        $header_adverts = Advert::where('position', 'top')->where('is_active', 1)->orderBy('id', 'DESC')->get();
        // dd($header_adverts);
        return view('front/homepage', compact(['sliders', 'header_adverts']))->with('skip_header', true);
    }

    public function About()
    {
        $p = Page::where('slug', 'about')->first();
        $teaser = Page::where('slug', 'now-customers-can-talk')->first();

        $this->meta->title = $p->name;
        $this->menu[$p->slug] = $this->active_class;

        $pages = Page::where('page_section_id', '3')->get();

        $about_pages = array();
        foreach ($pages as $pp) {
            $about_pages[$pp->slug] = $pp;
        }

        //dd($about_pages);
        $managementPage = Page::where('slug', 'management')->first();
        return view('front.about.about')
            ->with('page', $p)->with('menu', $this->menu)->with('pages', $about_pages)->with('teaser', $teaser)->with('management', $managementPage);
    }

    public function ShowAbout($slug)
    {
        $p = Page::where('slug', $slug)->first();
        if (
            !in_array(
                $slug,
                array('overview', 'goals', 'core-values', 'governance', 'mission-and-vision')
            )
        ) {

            return view('front.not-found')->with('page', $p);
        }

        //dd($pages);
        $this->menu['about'] = $this->active_class;
        $this->meta->title = $p->name;
        return view('front.about.sub_page')->with('page', $p)->with('menu', $this->menu);
    }

    public function page($slug)
    {
        $p = Page::where('slug', $slug)->with('Image')->first();

        if ($p == null) {
            return view('front.not-found')->with('page', $p);
        }
        $this->menu[$p->slug] = $this->active_class;
        $this->meta->title = $p->name;
        return view('front.page')->with('page', $p)->with('menu', $this->menu);
    }

    public function Management()
    {
        $p = Page::where('slug', 'management')->with('Image')->first();
        $managementPages = Page::where('page_section_id', '2')->get();
        $adivisoryPages = Page::where('page_section_id', '4')->get();
        $this->menu[$p->slug] = $this->active_class;
        $this->meta->title = $p->name;
        return view('front.about.management')->with('page', $p)
            ->with('managementPages', $managementPages)
            ->with('adivisoryPages', $adivisoryPages)
            ->with('menu', $this->menu);
    }

    public function TeamDetails($slug)
    {
        $p = Page::where('slug', $slug)->first();

        if ($p == null) {
            return view('front.not-found')->with('page', $p);
        }
        $this->menu[$p->slug] = $this->active_class;
        $this->meta->title = $p->name;
        return view('front.page')->with('page', $p)->with('menu', $this->menu);
    }

    /** # Categories */
    public function categories()
    {
        //shows the base categories

        $AllCats = Category::ReturnMainSubCategoriesTogether();
        $this->meta->load_app_js = 1;
        $this->meta->title = "All Categories";

        return view('front.categories.categories')->with('categories', $AllCats)->with('menu', $this->menu);
    }

    public function showCategoryItems($id)
    {

        $c = Category::find($id);
        $type = "";
        if (1) {
            $type = Input::get('type');
        }

        if ($c == null) {
            return view('front.not-found')->with('page', new Page());
        }
        $this->meta->load_app_js = 1;
        $this->meta->title = "Items in " . $c->name . " Category";
        return view('front.categories.category-details')->with('id', $id)->with('category', $c)
            ->with('menu', $this->menu)->with('type', $type);
    }

    public function ShowBestInCategory()
    {

        $this->meta->title = "Best In Each Category";
        $this->menu['categories'] = $this->active_class;
        $this->meta->load_app_js = 1;
        return view('front.categories.best-in-each-category')->with('menu', $this->menu);
    }


    public function CompareCategoryItems($id)
    {
    }

    public function News()
    {
        //loads news pages
        $news = Page::getNews(4);
    }

    public function NewsById($id)
    {
        //loads each news
        $n = Page::where('id', $id)->get();
        dd($n);
    }

    public function Blog()
    {

        $this->meta->title = "Blog Entries";
        $this->menu["blog"] = $this->active_class;
        $posts = Page::getBlogEntries(10);
        return view('front.blog.blog')->with('menu', $this->menu)->with('posts', $posts);
    }

    public function BlogBySlug($slug)
    {
        $p = Page::where('slug', $slug)->with('Image')->first();

        if ($p == null) {
            return view('front.not-found')->with('page', $p);
        }
        $this->menu["blog"] = $this->active_class;
        $this->meta->title = $p->name;
        $posts = Page::getBlogEntries(10);
        return view('front.blog.blog-display')->with('page', $p)->with('menu', $this->menu)->with('posts', $posts);;
    }

    public function items($id = 0)
    {

        $item = Item::find($id);
        if (!$item || $item->activated != 1) {
            $p = new Page();
            $this->meta->title = "Item not found";
            return view('front.not-found')->with('page', $p);
        }
        $template = $item->reviewTemplate;

        $itemWithDetails = Item::getItemWithMisc($id);


        $templateItems = $template->ReviewTemplateItems;
        $reviews = null;
        $posts = Review::getLatestReviewPosts(5, $id, $reviews);

        $reviewDetails = ReviewDetail::getReviewSummaryForItem($id);
        $c_score = ReviewDetail::getReviewSummaryForItem($id, -1);

        $r = new Review();

        $isReviewed = "0";


        $isBusiness = false;

        $user = Auth::user();
        if ($user) {
            if ($user->isBusiness()) {
                $isBusiness = true;
            }
        }

        $tab = 1;
        $_type = Input::get('type');
        $tab = SiteHelper::getActiveTab($_type);

        $this->meta->title = $item->name;
        $this->meta->load_app_js = 0;
        return view('front.items.item')->with('item', $item)->with('itemDetails', $itemWithDetails)
            ->with('template', $template)->with('posts', $posts)->with('reviews', $reviews)->with('review', $r)
            ->with('reviewDetails', $reviewDetails)->with('ti', $templateItems)
            ->with('hasReviewed', $isReviewed)->with('tab', $tab)->with('ComplementaryScore', $c_score)->with('isBusiness', $isBusiness);
    }

    public function showReview($id = 0)
    {
        $review = Review::where('id', $id)->with('Item')->first();
        if (!$review) {
            $this->meta->title = "Not Found";
            $p = new Page();
            return view('front.not-found')->with('page', $p);
        }

        //if not yet activated, then the review can only be viewed by the creator
        if (!$review->activated) {
            $user = Auth::user();
            $isReviewer = ($user != null) && ($review->user->id == $user->id);
            if (!$isReviewer) {
                $p = Page::where('slug', 'pending-approval')->first();
                return view('front.items.awaiting-approval')->with('page', $p);
            }
        }
        //is it a picture review?
        $review_rating = null;


        $item = Item::find($review->item_id);
        $item_details = Item::getItemWithMisc($review->item_id);

        if ($review->review_type_id === 1) {

            $review_rating = Review::getReviewsForItemWithScore($item->id, $id);
        }

        $reviewDetails = $review->ReviewDetails;
        //dd($reviewDetails);
        $template = $item->reviewTemplate;
        $templateItems = $template->ReviewTemplateItems;


        $comments = Comment::Where('review_id', $id)->with('user.profile')->get();
        $isBusiness = false;
        $showComment = true;
        $user = Auth::user();
        $business_items = null;
        if ($user) {
            if ($user->isBusiness()) {
                $isBusiness = true;
                $showComment = false;
                $business = Business::where('user_id', $user->id)->first();
                //dd($business);
                $business_items = BusinessItem::where('business_id', $business->id)->where('item_id', $item->id)->get();
                //dd($business_items);
                if (count($business_items)) {
                    $showComment = true;
                }
            }
        }

        $this->meta->title = $review->User->Profile->Fullname() . " 's Post for " . $item->name . ' ';
        $points = $this->calculatePoints($review->User);
        $badges = $this->getPoints($points);
        //dd($templateItems);
        // dd($item);
        return view('front.items.review')->with('review_rating', $review_rating)->with('item', $item)
            ->with('item_details', $item_details)->with('template', $template)
            ->with('reviewDetails', $reviewDetails)->with('menu', $this->menu)
            ->with('review', $review)->with('comments', $comments)->with('showCommentBox', $showComment)
            ->with('points', $points)->with('badges', $badges);
    }

    protected function calculatePoints($user) {
        $sum = 0;
        $rewards = $user->Profile->Rewards;
        foreach ($rewards as $key => $reward) {
            $sum += (int)$reward->number_of_points;
        }
        return $sum;
    }
    protected function getPoints($points) {
        if($points <= 250) {
            return 1;
        } else if($points > 250 && $points <= 500 ) {
            return 2;
        } else if($points > 500 && $points <= 1500) {
            return 3;
        } else if($points > 1500 && $points <= 5000) {
            return 4;
        } else if($points > 5000 ) {
            return 5;
        }
    }

    public function ShowFaq()
    {


        $this->menu['about'] = $this->active_class;
        $this->meta->title = "Frequently asked questions";
        $pages = Page::where('page_section_id', '8')->OrderBy('id')->get();
        return view('front.faq')->with('faqs', $pages)->with('menu', $this->menu);
    }

    public function ShowUser($id = null)
    {
        $profile = UserProfile::where('id', $id)->first();
        if (!$profile) {
            return view('front.not-found')->with('page', new Page());
        }
        $data = User::GetActivityLog($profile->user_id);

        $activities = $data;
        $this->meta->title = "User profile";


        $reviews = Review::where('user_id', $profile->user_id)->paginate(10);
        //dd($reviews);

        $stats = new stdClass();
        $stats->reviews = Review::getUserReviewCount($profile->user_id);
        $stats->comments = Comment::getUserCommentCount($profile->user_id);
        $stats->votes = Vote::getUserVoteCount($profile->user_id);
        $stats->TotalVotes = Vote::getVotesForUser($profile->user_id);


        //dd($profile->User);


        return view('front.users')->with('activities', $activities)
            ->with('profile', $profile)->with('stats', $stats)->with('reviews', $reviews);
    }

    public function FlagReview($id)
    {
        $review = Review::where('id', $id)->with('Item')->first();

        if (!$review) {
            $this->meta->title = "Item not found";
            $p = new Page();
            return view('front.not-found')->with('page', $p);
        }

        $user = Auth::user();
        $this->meta->title = "Report an issue";
        $flag = new Flag();
        return view('front.items.flag')->with('review', $review)->with('flag', $flag)->with('user', $user);
    }


    public function EditReview($id = 0)
    {
        $review = Review::where('id', $id)->with('Item')->first();
        if (!$review) {
            $this->meta->title = "Item not found";
            $p = new Page();
            return view('front.not-found')->with('page', $p);
        }

        if (!$review->IsInEditWindow()) {
            return Redirect::to('items/' . $review->item_id)->with('tab', $review->review_type_id)->with('message', 'Edit window has expired!');
        }
        $item = Item::find($review->item_id);
        if (!$item) {
            $p = new Page();
            $this->meta->title = "Item not found";
            return view('front.front.not-found')->with('page', $p);
        }
        $template = $item->reviewTemplate;



        $templateItems = $template->ReviewTemplateItems;
        $reviewDetails = ReviewDetail::getReviewSummaryForItem($item->id);

        $isReviewed = "0";
        $tab = 1;
        $_type = Input::get('type');
        $tab = SiteHelper::getActiveTab($_type);

        $user = Auth::user();
        $this->meta->title = "Edit review for " . $item->name;
        $this->meta->load_app_js = 0;
        return view('front.items.edit-review')->with('item', $item)
            ->with('template', $template)->with('review', $review)
            ->with('reviewDetails', $reviewDetails)->with('ti', $templateItems)->with('user', $user)
            ->with('hasReviewed', $isReviewed)->with('tab', $tab);
    }


    public function  search()
    {
        $term = Input::get("s");
        $items = Item::like('name', $term)->where('activated', '1')->get();
        $this->meta->load_app_js = 1;
        $this->meta->title = "Search Results for " . $term;
        return view('front.search')->with('term', $term)->with('menu', $this->menu)->with('results', $items)->with('hideTitle', true);
    }
    public function Advertise()
    {

        $p = Page::where('slug', 'advertise')->first();

        $this->meta->title = $p->name;
        $this->menu[$p->slug] = $this->active_class;
        $advertise = new AdvertRequest();

        return view('front.advertise')->with('m', $advertise)
            ->with('page', $p)->with('menu', $this->menu);
    }

    public function WriteReview($type = "review")
    {

        if (!in_array($type, array('review', 'post', 'question'))) {
            $type = "review";
        }

        $p = Page::where('slug', 'write-a-review')->first();
        $details = array();
        $details['_type'] = $type;
        $details['ShowFilePicker'] = true;
        $details['ShowReviewTitle'] = true;
        $details['ShowReviewBody'] = true;
        $details['ShowStar'] = true;
        if ($type == "review") {
            $details['review_type_id'] = 1;
            $details['type'] = "Add review";
            $this->meta->title = "Write a review";
            $this->menu[$p->slug] = $this->active_class;
        }

        if ($type == "post") {
            $details['review_type_id'] = 2;
            $details['type'] = "Add image";
            $details['ShowStar'] = false;
            $details['ShowReviewTitle'] = false;
            $details['ShowReviewBody'] = false;
            $this->meta->title = "Add an image";
            $this->menu['post-an-image'] = $this->active_class;
        }

        if ($type == "question") {
            $details['review_type_id'] = 3;
            $details['type'] = "Ask a question ";
            $details['ShowStar'] = false;
            $details['ShowReviewTitle'] = false;
            $details['ShowReviewBody'] = true;
            $details['ShowFilePicker'] = false;
            $this->meta->title = "Ask a question";
            $this->menu['ask-a-question'] = $this->active_class;
        }

        $CatsWithItems = Category::getCategoresThatHaveItems()->sortBy('name');

        $first_review = Page::where('slug', 'first-review')->first();
        $AllCats = Category::ReturnMainSubCategories();


        $defaultSelect = array('' => 'Select');
        $cList = $defaultSelect + array_combine($CatsWithItems->pluck('id')->toArray(), $CatsWithItems->pluck('name')->toArray());


        return view('front.write_a_review_page')->with('HasReview', "0")->with('hideTitle', true)->with('CatsWithItems', $cList)
            ->with('page', $p)->with('first_review', $first_review)->with('type', $type)
            ->with('categories', $AllCats)->with('menu', $this->menu)->with('postDetails', $details);
    }


    # Posts starts here

    public function review($id = 0, $review_id = null)
    {
        $firebaseMessage = new FirebaseMessagingService();

        //force login for this action
        $user = Auth::user();
        $data = Input::All();
        //dd($data);
        if (!isset($data['id'])) {
        }
        $id = $data['id'];

        $item = Item::where('id', $id)->first();

        $r = new Review();
        if ($review_id != null) {
            $r = Review::where('id', $review_id)->first();
            if (!$r->IsInEditWindow()) {
                return Redirect::to('items/' . $r->item_id)->with('tab', $r->review_type_id)->with('message', 'Edit window has expired!');
            }
        } else {
            $r->user_id = $user->id;
            $r->item_id = $id;
        }
        $r->review_type_id = $data['review_type_id'];

        $title = "";
        $location = "";
        $body = "";
        if (isset($data['location'])) {
            $location = $data['location'];
        }
        if (isset($data['body'])) {
            $body = $data['body'];
        }
        if (isset($data['title'])) {
            $title = $data['title'];
        }
        if (isset($data['overall_score'])) {
            $r->overall_score = $data['overall_score'];
        }
        $r->body = $body;
        $r->title = $title;
        $r->location = $location;
        $file_id = null;
        if (isset($data['file'])) {

            $file = array_get($data, 'file');
            $image_extentions = File::returnImageFileTypes();
            if (!in_array($file->guessClientExtension(), $image_extentions)) {
                return Redirect::to('items/' . $id)->with('message', 'Please add an image file')->with('tab', $r->review_type_id);
            }
            $file_id = SiteHelper::UploadFile($file, $user->id, 'review_' . $id . '_image' . date('YmdHis'));
        }
        $r->file_id = $file_id;
        //$r->activated=false;
        $r->save();

        $reward = new Reward();
        $reward->user_id = $user->id;
        $countBody = $this->get_num_of_words($r->body);
        if($countBody > 200) {
            $reward->description = $this->reward_types["more_than_200"][1];
            $reward->type = $this->reward_types["more_than_200"][0];
            $reward->number_of_points = $this->reward_types["more_than_200"][2];
        } else {
            $reward->description = $this->reward_types["less_than_200"][1];
            $reward->type = $this->reward_types["less_than_200"][0];
            $reward->number_of_points = $this->reward_types["less_than_200"][2];
        }
        $reward->save();

        //save review details
        if (isset($data['rating'])) {
            $details = $data['rating'];
            if ($details != null && count($details)) {
                //clear any existing
                ReviewDetail::Where('review_id', $r->id)->delete();
                foreach ($details as $key => $val) {
                    $d = new ReviewDetail();
                    $d->item_id = $id;
                    $d->review_id = $r->id;
                    $d->review_template_detail_id = $key;
                    $d->rating = $val;
                    $d->save();
                }
                $reward = new Reward();
                $reward->user_id = $user->id;
                $reward->description = $this->reward_types["rating_product"][1];
                $reward->type = $this->reward_types["rating_product"][0];
                $reward->number_of_points = $this->reward_types["rating_product"][2];
                $reward->save();
            }
        }
        $followers = Follow::where('object_id', $id)->get();
        foreach ($followers as $key => $follower) {
            $firebaseMessage->user_id = $follower->user_id;
            $firebaseMessage->notification_title = 'Review added';
            $firebaseMessage->notification_body = $user->Profile->Fullname().' made a review of '.$item->name;
            $firebaseMessage->send_notification();
        }

        return Redirect::to('items/' . $id)->with('tab', $r->review_type_id)->with('message', 'Post successfully saved!');
    }

    public function get_num_of_words($string) {
        $string = preg_replace('/\s+/', ' ', trim($string));
        $words = explode(" ", $string);
        return strlen(implode("",$words));
    }


    public function PictureReview($id = 0, $review_id = null)
    {

        //force login for this action
        $user = Auth::user();
        $data = Input::All();
        //dd($data);
        $r = new Review();
        if ($review_id != null) {
            $r = Review::where('id', $review_id)->first();
            if (!$r->IsInEditWindow()) {
                return Redirect::to('items/' . $r->item_id)->with('tab', $r->review_type_id)->with('message', 'Edit window has expired!');
            }
        } else {
            $r->user_id = $user->id;
            $r->item_id = $id;
        }
        $r->body = "";
        $r->title = "";
        $r->location = "";
        //$r->activated=false;
        $tab = 2;

        $file_id = null;
        if (isset($data['file'])) {
            $file = array_get($data, 'file');
            $image_extentions = File::returnImageFileTypes();
            if (!in_array($file->guessClientExtension(), $image_extentions)) {

                return Redirect::to('items/' . $id . "?type=post")->with('message', 'Please add an image file')->with('tab', $tab);
            }

            $file_id = SiteHelper::UploadFile($file, $user->id, 'review_' . $id . '_image' . date('YmdHis'));
            $r->review_type_id = "2";
            $r->file_id = $file_id;
            $r->save();
            return Redirect::to('items/' . $id . "?type=post")->with('message', 'Post Added')->with('tab', $tab);
        } else {
            //notify the individual that a picture must be attached.
            return Redirect::to('items/' . $id . "?type=post")->with('message', 'Please Add an image')->with('tab', $tab);
        }
    }

    public function saveComment($id = 0)
    {
        $data = Input::All();

        //dd($data);
        $user = Auth::user();
        $review = Review::where('id', $id)->with('Item')->first();
        //dd($data);
        $c =  new Comment();
        $c->user_id = $user->id;
        $c->review_id = $id;
        $c->title = "Comment on review $id";
        $c->object_type_id = "1";
        $c->title = "Comment on Review $id";
        $c->body = $data['body'];
        //dd($c);
        $c->save();

        $reward = new Reward();
        $reward->user_id = $user->id;
        $reward->description = $this->reward_types["commented_on_review"][1];
        $reward->type = $this->reward_types["commented_on_review"][0];
        $reward->number_of_points = $this->reward_types["commented_on_review"][2];
        $reward->save();

        // Notify the reviewer that someone liked their review
        $this->firebaseMessage->user_id = $review->user_id;
        $this->firebaseMessage->notification_title = 'Comment on review';
        $this->firebaseMessage->notification_body = $user->Profile->Fullname().' commented on your review';
        $this->firebaseMessage->send_notification();

        return Redirect::to('reviews/show/' . $id)->with('message', 'Comment Added');
    }



    public function saveVote(Request $request)
    {
        //Log::info("here");
        $message = new AjaxResponse();
        $message->status = '0';
        $message->message = "";

        $data = Input::all();

        $v = new Vote();

        if (isset($data['vote']) && isset($data['review'])) {
            if (Auth::Check()) {

                $user = Auth::user();
                $existing = Vote::Where('review_id', $data['review'])->Where('user_id', $user->id)->first();
                if (!$existing) {
                    $v->review_id = $data['review'];
                    $v->value = $data['vote'];
                    $v->user_id = $user->id;
                    $v->save();
                    $message->status = '1';
                    $message->message = "Liked";
                    $review = Review::find($data['review_id']);
                    $reward = new Reward();
                    $reward->user_id = $review->user_id;
                    // $reward->user_id = $user->id;
                    $reward->description = $this->reward_types["liked_feedback"][1];
                    $reward->type = $this->reward_types["liked_feedback"][0];
                    $reward->number_of_points = $this->reward_types["liked_feedback"][2];
                    $reward->save();

                    // Notify the reviewer that someone liked their review
                    $this->firebaseMessage->user_id = $review->user_id;
                    $this->firebaseMessage->notification_title = 'Liked review';
                    $this->firebaseMessage->notification_body = $user->Profile->Fullname().' liked your review';
                    $this->firebaseMessage->send_notification();
                } else {
                    $message->status = '-2';
                    $message->message = "You have already reacted to this review";
                }
            } else {
                $message->status = '-1';
                $message->message = "Please Log in to make your vote count";
            }
        }
        //Log::info($message);
        return Response::json($message);
    }

    public function SaveFlag($id = 0)
    {
        $data = Input::All();
        $user = Auth::user();
        $f = new Flag();
        $f->review_id = $id;
        $f->user_id = $user->id;
        $f->body = $data['body'];
        $f->save();
        $review = Review::find($id);

        // Notify the reviewer about their review been flagged
        $this->firebaseMessage->user_id = $review->user_id;
        $this->firebaseMessage->notification_title = 'Flagged review';
        $this->firebaseMessage->notification_body = $user->Profile->Fullname().' Flagged your review';
        $this->firebaseMessage->send_notification();

        $this->meta->title = "Flag A Review";
        $m = new ItemSuggestion();
        $m->message = "Your report has been received. We will do our checks in accordance with our content guidelines and act accordingly";
        return view('front/contact/saved')->with('m', $m)->with('menu', $this->menu);

        //return Redirect::to('items/' . $review->item_id)->with('message', 'Flag Added');
    }

    public function Polls()
    {
        $this->meta->title = 'Polls';
        $this->menu['polls'] = $this->active_class;
        $polls = Poll::GetActivePolls();
        //dd($polls);

        return view('front.polls.polls')->with('polls', $polls)->with('menu', $this->menu);
    }

    public function RespondToPoll($id)
    {
        $this->meta->title = 'Poll';
        $this->menu['polls'] = $this->active_class;
        $poll = Poll::find($id);
        $type = "warning";
        if ($poll == null) {
            return view('front.polls.poll-completed')->with('message', 'Poll was not found')->with('type', $type);;
        }
        //dd($poll);
        if ($poll->status() !== "ongoing") {
            return view('front.polls.poll-completed')->with('message', 'Poll is no longer available')->with('type', $type);;
        }
        $user = Auth::user();
        if (PollQuestionResponse::HasUserRespondedToPoll($user->id, $poll->id)) {
            return view('front.polls.poll-completed')->with('message', 'You have already completed this poll')->with('type', $type);
        }
        $questions = $poll->Questions;

        return view('front.polls.poll-questions')->with('poll', $poll)
            ->with('questions', $questions)->with('menu', $this->menu);
    }

    function SubmitPoll($id)
    {
        $this->meta->title = 'Poll Response';
        $this->menu['polls'] = $this->active_class;
        $poll = Poll::find($id);
        $type = "warning";
        if ($poll == null) {
            return view('front.polls.poll-completed')->with('message', 'Poll was not found')->with('type', $type);;
        }
        //check poll validity here
        if ($poll->status() !== "ongoing") {
            return view('front.polls.poll-completed')->with('message', 'Poll is no longer available')->with('type', $type);;
        }

        $user = Auth::user();
        if (PollQuestionResponse::HasUserRespondedToPoll($user->id, $poll->id)) {
            return view('front.polls.poll-completed')->with('message', 'You have already completed this poll')->with('type', $type);;
        }
        $data = Input::All();
        $array = array();
        foreach ($data['questions'] as $key => $value) {
            if ($value != null) {
                $array[] = array(
                    'poll_id' => $poll->id,
                    'poll_question_id' => $key,
                    'option_id' => $value,
                    'user_id' => $user->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
            }
        }
        PollQuestionResponse::insert($array);
        $reward = new Reward();
        $reward->user_id = $user->id;
        $reward->description = $this->reward_types["respond_to_poll"][1];
        $reward->type = $this->reward_types["respond_to_poll"][0];
        $reward->number_of_points = $this->reward_types["respond_to_poll"][2];
        $reward->save();
        $type = "success";
        return view('front.polls.poll-completed')->with('message', 'Poll successfully completed')->with('type', $type);
    }
}
