<?php

namespace App\Http\Controllers;

use View;
use \stdClass;
use App\Model\PageMeta;

class MyController extends Controller
{
	public $obj;
	public $meta;
	public function __construct()
	{

		$this->obj = new stdClass();
		$this->obj->actor = "Admin";
		$this->meta = new PageMeta();
		$this->meta->load_app_js = 1;
		View::share('meta', $this->meta);
		View::share('a', $this->obj);
	}
}
