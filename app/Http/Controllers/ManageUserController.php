<?php

namespace App\Http\Controllers;

use View;
use Input;
use stdClass;
use App\User;
use App\Model\Review;
use App\Model\Comment;
use App\Model\Vote;
use App\Model\PageMeta;
use Auth;

class ManageUserController extends AdminController
{
    public $obj;
    public $meta;
    public $menu;
    public $active_class;
    public $user;
    public static $PerPage = 10;

    public function __construct()
    {

        parent::__construct();

        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Users";
        $this->menu['users'] = 'active';
        View::share('menu', $this->menu);
    }

    public function Users()
    {

        $this->obj->action = "All Users";
        $users = User::with('Profile')->paginate(self::$PerPage);

        return view('admin/user/index')->with('users', $users);
    }

    public function SearchUsers()
    {
        $term = Input::get("s");
        $users = User::where('name', 'like', "%$term%")->orWhere('email', 'like', "%$term%")->with('Profile')->paginate(self::$PerPage);
        $this->obj->action = "Search Results for $term";
        return view('admin/user/index')->with('users', $users);
    }

    public function UserProfile($id)
    {
        $user = User::find($id);
        if ($user == null) {
            echo "User not found, click back to continue"; //TODO::show more friendly page.
        }
        $this->obj->action = "User Profile:  " . $user->Profile->Fullname();
        $data = User::GetActivityLog($user->id);

        $activities = $data;


        $stats = new stdClass();
        $stats->reviews = Review::getUserReviewCount($user->id);
        $stats->comments = Comment::getUserCommentCount($user->id);
        $stats->votes = Vote::getUserVoteCount($user->id);
        $stats->TotalVotes = Vote::getVotesForUser($user->id);

        return view('admin.user.profile')->with('user', $user)->with('activities', $activities)
            ->with('profile', $user->profile)->with('stats', $stats);
    }
}
