<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Helpers\SiteHelper;
use Input;
use App\Model\File;
use App\User;
use App\Model\Business;
use App\Model\PageMeta;
use App\Model\BusinessItem;
use View;
use stdClass;

class BusinessAccountController  extends MyController
{

    protected $user;
    public function __construct()
    {
        //bounce non logged in users at this point
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            if (!$this->user->isBusiness()) {
                return Redirect::to('login');
            }
            if ($this->user->created_at == $this->user->updated_at) {
                return Redirect::to('user/password')->with('message', 'Please change your password to continue');
            }
            return $next($request);
        });
        parent::__construct();
        $this->meta = new PageMeta();
        $this->meta->description = "Share your product experiences, review products and services.";
        $this->meta->title = "Business Dashboard";
        $this->menu['user'] = $this->active_class = "active";
        View::share('menu', $this->menu);
        View::share('meta', $this->meta);
    }


    public function index()
    {
        //shows the profile
        $user = Auth::user();
        // dd($user->id);
        $business = Business::where('user_id', $user->id)->first();
        $items = BusinessItem::where('business_id', $business->id)->with('Item.Category')->get();
        return view('business.index')->with('user', $user)->with('business', $business)->with('items', $items);
    }

    public function Profile()
    {
        $user = Auth::user();
        $business = Business::where('user_id', $user->id)->first();
        //dd($business);

        $this->meta->title = "Update Profile";
        return view('business.profile')->with('business', $business)->with('user', $user);
    }


    public function update()
    {
        $user = Auth::user();
        $data = Input::All();

        $business = Business::where('user_id', $user->id)->first();

        $file_id = null;
        if (isset($data['file'])) {
            $file = array_get($data, 'file');

            $file_id = SiteHelper::UploadFile($file, $user->id, 'profile_' . $user->id);
            $business->image_id = $file_id;
        }

        if (isset($data['description'])) {
            $business->description = $data['description'];
        }

        if (isset($data['rc_no'])) {
            $business->rc_no = $data['rc_no'];
        }

        $business->save();

        return Redirect::to('business')->with('message', 'Update Successful');
    }
}
