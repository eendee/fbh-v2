<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\File;
use Input;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\SiteHelper;
use View;
use Auth;
use App\User;

class FileController extends AdminController
{

    public static $PerPage = 20;
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['indexx']]);
        parent::__construct();
        $this->menu['files'] = 'active';
        View::share('menu', $this->menu);
    }

    public function index()
    {

        $file = new File();
        $this->obj->action = "All Files";
        $files = File::OrderByDesc('id')->with('User')->paginate(self::$PerPage);
        return view('admin/upload/index')->with('file', $file)->with('files', $files);
    }

    public function SearchFiles()
    {
        $term = Input::get("s");

        $files = File::where('name', 'like', "%$term%")->orWhere('mimetype', 'like', "%$term%")->with('User')->paginate(self::$PerPage);
        $file = new File();
        $this->obj->action = "Search Results for $term";
        return view('admin/upload/index')->with('file', $file)->with('files', $files);
    }

    public function upload()
    {



        $data = Input::all();
        $file = array_get($data, 'file');
        $user = Auth::user();
        if (SiteHelper::UploadFile($file, $user->id) > 0) {
            return Redirect::to('admin/files/upload')->with('message', 'file uploaded successfully');
        } else {
            //show some sort of error;
        }

        /*
    	$f->name=$file->getClientOriginalName();
    	$f->mimetype=$file->getClientMimeType();
        $f->extension = $file->getClientOriginalExtension(); 
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $temp_name=str_replace(' ', '-', $filename).'-'.date('YmdHis');
        $f->url=$temp_name. '.' . $f->extension;


        $destinationPath = 'uploads'; 
        $upload_success = $file->move($destinationPath, $f->url); 
        if ($upload_success) {
            if($f->extension=='jpg'){
                SiteHelper::CreateThumbnail('uploads/'.$f->url,$temp_name);
            }
        	FILe::Create(array(
        			'name'=>$f->name,
        			'mimetype'=>$f->mimetype,
        			'extension'=>$f->extension,
                    'user_id'=>'1',
        			'url'=>$f->url));
            return Redirect::to('admin/files/upload')->with('message', 'file uploaded successfully');
        } 
        */
    }

    public function view()
    { }
}
