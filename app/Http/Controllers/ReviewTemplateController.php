<?php

namespace App\Http\Controllers;


use Input;
use App\Model\ReviewTemplate;
use App\Model\ReviewTemplateDetail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use View;

class ReviewTemplateController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Review Templates";
        $this->menu['review_templates'] = 'active';
        View::share('menu', $this->menu);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index()
    {
        $this->obj->action = "All Review Templates";
        $templates = ReviewTemplate::all();
        return view('admin/reviewTemplate/index')->with('templates', $templates);
    }


    public function Items($id)
    {
        $this->obj->action = "Review Template Items";
        $template = ReviewTemplate::find($id);
        $items = $template->ReviewTemplateItems;
        return view('admin/reviewTemplate/items')->with('template', $template)->with('items', $items);
    }

    public function addTemplateItem($id)
    {

        $template = ReviewTemplate::find($id);
        $item = new ReviewTemplateDetail();
        $item->review_template_id = $template->id;

        $this->obj->action = "Add Template Item To $template->name";
        return view('admin/reviewTemplate/edit_item')->with('template', $template)->with('item', $item);
    }

    public function storeTemplateItem($id)
    {

        $data = Input::all();
        $v = Validator::make($data, ReviewTemplateDetail::$rules);
        if ($v->fails()) {
            //dd($v);
            return redirect("admin/templates/$id/items/add")
                ->with('message', $v->errors()->first())->withInput();
        }
        $item = ReviewTemplateDetail::Create($data);
        return Redirect::to("admin/templates/items/" . $id);
    }


    public function editTemplateItem($id)
    {
        $this->obj->action = "Edit Template Item";
        $item = ReviewTemplateDetail::find($id);
        $template = $item->ReviewTemplate;


        return view('admin/reviewTemplate/edit_item')->with('template', $template)->with('item', $item);
    }

    public function updateTemplateItem($id)
    {

        $data = Input::all();
        $item = ReviewTemplateDetail::find($id);
        $item->update($data);
        return Redirect::to("admin/templates/items/" . $item->ReviewTemplate->id);
    }



    public function create()
    {
        $this->obj->action = "Add Review Template";
        $template = new ReviewTemplate();
        $template->name = "";
        return view('admin/reviewTemplate/edit')->with('template', $template);
    }

    public function edit($id)
    {
        $this->obj->action = "Edit Review Template";
        $template = ReviewTemplate::find($id);
        return view('admin/reviewTemplate/edit')->with('template', $template);
    }

    public function delete($id)
    {
        $template = ReviewTemplate::find($id);
        return view('admin/reviewTemplate/delete')->with('template', $template);
    }



    public function update(ReviewTemplate $template)
    {


        $data = Input::all();
        $template->update($data);
        return Redirect::to(route('templates.index'));
    }

    public function store()
    {
        $data = Input::all();
        $t = ReviewTemplate::Create($data);
        return Redirect::to(route('templates.index'));
    }

    public function destroy(ReviewTemplate $template)
    {

        $template->delete();
        return Redirect::to(route('templates.index'))->with('message', 'Template Successfully Deleted ');
    }
}
