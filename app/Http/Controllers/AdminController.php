<?php

namespace App\Http\Controllers;

use View;
use \stdClass;
use App\Model\PageMeta;
use Auth;
use App\User;
use App\Model\Review;
use App\Model\Item;
use App\Model\Category;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public $obj;
    public $meta;
    public $menu;
    public $active_class;
    public $user;
    public function __construct()
    {

        //handle menu issues here
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();
            if (!$this->user->isAdmin()) {
                return Redirect::to('login');
            }
            return $next($request);
        });
        $this->obj = new stdClass();
        $this->user = Auth::user();
        $this->obj->actor = "Admin";
        $this->obj->action = "";
        $this->obj->controller = "";
        $this->meta = new PageMeta();
        View::share('a', $this->obj);
        View::share('user', $this->user);
    }


    public function index()
    {
        $user = Auth::user();


        $data = User::GetActivityLog($user->id);

        $activities = $data;


        $stats = new stdClass();
        $stats->reviews = Review::count();
        $stats->category = Category::count();
        $stats->users = User::count();
        $stats->items = Item::count();
        $this->menu['dashboard'] = 'active';
        $this->obj->controller = "Pages";

        return view('admin.dashboard')->with('stats', $stats)->with('menu', $this->menu);
    }


}
