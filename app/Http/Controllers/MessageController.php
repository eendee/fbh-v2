<?php

namespace App\Http\Controllers;


use Input;
use App\Model\Message;
use App\Model\ItemSuggestion;
use App\Model\Flag;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use View;

class MessageController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->obj->controller = "Messages";
        $this->menu['messages'] = 'active';
        View::share('menu', $this->menu);
    }

    public function Index()
    {

        $this->obj->action = "View Messages";
        $messages = Message::orderBy('id', 'DESC')->paginate(10);
        //dd($messages);
        return view('admin/messages/index')->with('messages', $messages);
    }

    public function IndexOfItemSuggestion()
    {

        $this->obj->action = "View Suggestions";
        $messages = ItemSuggestion::orderBy('id', 'DESC')->paginate(10);
        //dd($messages);
        return view('admin/messages/item-suggestion')->with('messages', $messages);
    }

    public function IndexOfFlaggedReviews()
    {

        $this->obj->action = "View Flags";
        $messages = Flag::orderBy('id', 'DESC')->with('Review.Item')->paginate(10);
        //dd($messages);
        return view('admin/messages/flags')->with('messages', $messages);
    }
}
