<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use DB;
use App\Model\Page;
use Illuminate\Support\Facades\Redirect;
use View;

use App\Services\FirebaseMessagingService;


class PageController extends AdminController
{
    public $firebaseMessage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->menu['pages'] = 'active';
        $this->obj->controller = "Pages";
        View::share('menu', $this->menu);
        $this->firebaseMessage = new FirebaseMessagingService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index()
    {

        //dd($this->menu);
        $pages = Page::with('Section')->OrderBy('page_section_id')->get();
        $this->obj->action = "View Pages";
        return view('admin/page/index')->with('pages', $pages);
    }

    public function create()
    {
        //return "Here";
        $this->obj->action = "Add a new page";
        $page = new Page();
        $page->name = "";
        // $page->slug = "url friendly name";
        $page->slug = "";
        return view('admin/page/edit')->with('page', $page);
    }

    public function edit($id)
    {
        //return "Here";
        $page = Page::find($id);
        return view('admin/page/edit')->with('page', $page);
    }

    public function delete($id)
    {
        $this->obj->action = "Confirm Delete";
        $page = Page::find($id);
        return view('admin/page/delete')->with('page', $page);
    }
    public function update($id)
    {


        $data = Input::all();
        //dd($data);
        $page = Page::find($id);
        //check for existing slug
        $check = Page::where('slug', $page->slug)->where('id', '!=', $page->id)->get();
        if (count($check)) {
            return redirect()->back()->with('message', 'new slug already in use.')->withInput();
        }
        $page->update($data);
        return Redirect::to('admin/pages/')->with('message', 'Update Successful');
        //var_dump($data);
    }



    public function store(Request $request)
    {

        $validated = $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|unique:pages',
            'body' => 'required',
            'page_section_id' => 'required'
        ]);
        $data = Input::all();
        $page = Page::Create($request->all());
        if($data['page_section_id'] == 6) {
            $this->firebaseMessage->notification_title = 'New Blog Post';
            $this->firebaseMessage->notification_body = $data['name'];
            $this->firebaseMessage->send_general_message();
        }
        // dd('Got here 2');
        if($data['page_section_id'] == 5) {
            $this->firebaseMessage->notification_title = 'News Update';
            $this->firebaseMessage->notification_body = $data['name'];
            $this->firebaseMessage->send_general_message();
        }
        return Redirect::to('admin/pages/')->with('message', 'Page creation was Successful');
    }

    public function destroy($id)
    {

        $page = Page::find($id);
        // dd($page);
        if($page) {
            $page->delete();
        }
        // if (count($page)) {
        //     $page->delete();
        // }

        return Redirect::to('admin/pages/')->with('message', 'Page Successfully Deleted ');
    }
}
