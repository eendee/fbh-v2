<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Model\Item;
use App\Model\Page;
use DB;
use App\Model\Setting;
use Illuminate\Support\Facades\Redirect;
use View;

class SettingsController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->menu['pages'] = 'active';
        $this->obj->controller = "Pages";
        View::share('menu', $this->menu);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index()
    {

        //dd($this->menu);
        //shows all settings
        $settings = Setting::get();
        $this->obj->action = "Settings";
        return view('admin/settings/index')->with('settings', $settings);
    }

    public function edit($template, $id)
    {
        //return "Here";
        $setting = Setting::find($id);
        //get top level categories
        $cats = DB::select(Item::GetMaxInCategoryString() . "inner join categories on categories.id=topmaxcat2.category_id");
        $checked = explode(',', $setting->value);

        $view = "";
        switch ($setting->edit_template) {
            case 'chbxs':
                $view = 'checkboxes';
                break;

            case 'sldr':
                return Redirect::to('admin/settings/slider');
                break;

            case 'nmbr':
                return Redirect::to('admin/settings/numbers');
                break;

            default:
                dd();
                break;
        }
        return view('admin/settings/checkboxes')->with('setting', $setting)
            ->with('cats', $cats)->with('checked', $checked);
    }




    public function slider()
    {
        //load pages with slider 
        $setting = Setting::find(3);
        $pages = Page::where('page_section_id', '7')->get();
        $checked = explode(',', $setting->value);

        return view('admin/settings/checkboxes_sliders')->with('setting', $setting)
            ->with('pages', $pages)->with('checked', $checked);
    }

    public function numbers()
    {
        //load pages with slider 
        $settings = Setting::where('edit_template', 'nmbr')->get();
        //dd($settings);
        return view('admin/settings/numbers')->with('settings', $settings);
    }

    public function saveSettings($template, $id)
    {


        $data = Input::all();
        //dd($data);
        $_id = array();
        $collection = array();
        if ($template == "sldr" || $template == "chbxs") {
            $setting = Setting::find($id);
            if (isset($data['value'])) {
                $value = implode(',', $data['value']);
                //dd($value);
                $setting->value = $value;
                $setting->save();
            }
        } else {
            foreach ($data as $key => $value) {
                //get each setting and update
                if ($key != '_token') {
                    $_id[] = str_replace('vals_', '', $key);
                    $collection[str_replace('vals_', '', $key)] = $value;
                }
            }
            //dd($collection);
            foreach ($collection as $key => $value) {
                Setting::where('id', $key)->update(['value' => $value]);
            }
        }
        return Redirect::to('admin/settings/')->with('message', 'Update Successful');
    }

    public function delete($id)
    {
        $this->obj->action = "Confirm Delete";
        $page = Page::find($id);
        return view('admin/page/delete')->with('page', $page);
    }
    public function update($id)
    {


        $data = Input::all();
        //dd($data);
        $page = Page::find($id);
        //check for existing slug
        $check = Page::where('slug', $page->slug)->where('id', '!=', $page->id)->get();
        if (count($check)) {
            return redirect()->back()->with('message', 'new slug already in use.')->withInput();
        }
        $page->update($data);
        return Redirect::to('admin/pages/')->with('message', 'Update Successful');
        //var_dump($data);
    }



    public function store()
    {
        $data = Input::all();
        $page = Page::Create($data);
        return Redirect::to('admin/pages/');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        if (count($page)) {
            $page->delete();
        }

        return Redirect::to('admin/pages/')->with('message', 'Page Successfully Deleted ');
    }
}
