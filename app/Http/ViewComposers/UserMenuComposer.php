<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

use Auth;
use App\Model\Category;
use App\Model\Item;
use App\Model\Page;
use Illuminate\Support\Facades\Cache;

class UserMenuComposer
{

    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = null;
        if (Auth::check()) {
            $user = Auth::user();
            $view->with('user', $user);
        }
    }

    public function categories(View $view)
    {
        //$cats=
        $cats = Cache::remember('cats', 60 * 20, function () {
            return Category::ReturnTree();
        });
        //dd($cats);


        $items = Cache::remember('items', 60, function () {
            return Item::take(5)->orderBy('id', 'DESC')->with('Category')->with('File')->get();
        });

        $disclaimer = Cache::remember('disclaimer', 60 * 20, function () {
            return Page::where('slug', 'disclaimer')->first();
        });



        $view->with('cats', $cats)->with('items', $items)->with('disclaimer', $disclaimer);
    }
}
