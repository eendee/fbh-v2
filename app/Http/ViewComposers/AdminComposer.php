<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Model\Category;
use App\Model\File;
use App\Model\ReviewTemplate;
use App\User;
use App\Model\PageSection;

class AdminComposer
{
    public $CategoryList = [];
    public $ImageList = [];
    public $defaultSelect = array('' => 'Select');
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $c = Category::All('id', 'name');
        $images = array('png', 'jpg', 'jpeg');
        $i = File::whereIn('extension', $images)->get();
        $t = ReviewTemplate::All('id', 'name');
        $p = array('1' => '1', '2' => '2', '3' => '3');
        $g = PageSection::All('id', 'name');
        //dd($i);
        $iList = $this->defaultSelect + array_combine($i->pluck('id')->toArray(), $i->pluck('name')->toArray());
        $cList = $this->defaultSelect + array_combine($c->pluck('id')->toArray(), $c->pluck('name')->toArray());
        $tList = $this->defaultSelect + array_combine($t->pluck('id')->toArray(), $t->pluck('name')->toArray());
        $gList = $this->defaultSelect + array_combine($g->pluck('id')->toArray(), $g->pluck('name')->toArray());
        $view->with('categories', $cList)->with('images', $iList)
            ->with('reviewTemplates', $tList)->with('pageTemplates', $p)->with('pageSections', $gList);
    }
}
