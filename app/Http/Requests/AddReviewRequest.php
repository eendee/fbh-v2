<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' => 'required|string|min:1',
            'review_type_id'=>'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'state_id.required' => 'The state of residence is required',
        ];
    }
}
