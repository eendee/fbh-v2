<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PollQuestionOption extends Model
{


    public static function MassDeleteByQuestionId($QuestionId)
    {
        PollQuestionOption::where('poll_question_id', $QuestionId)->delete();
    }
    public static function MassDeleteByPollId($PollId)
    {
        PollQuestionOption::where('poll_id', $PollId)->delete();
    }
}
