<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PollQuestionResponse extends Model
{
    //


    public  function  Question()
    {
        return $this->hasOne('App\Model\PollQuestion', 'id', 'poll_question_id');
    }

    public  function  Option()
    {
        return $this->hasOne('App\Model\PollQuestionOption', 'id', 'option_id');
    }

    public  function  User()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public static function HasUserRespondedToPoll($user_id, $poll_id)
    {
        $results = PollQuestionResponse::where('user_id', $user_id)->where('poll_id', $poll_id)->get();
        if (count($results)) {
            return true;
        }
        return false;
    }

    public static function GetResponseByUser($user_id, $poll_id)
    {
        return PollQuestionResponse::where('user_id', $user_id)->where('poll_id', $poll_id)->get();
    }

    public static function GetResponsesForPoll($poll_id)
    {
        return PollQuestionResponse::where('poll_id', $poll_id)->with('Question')->with('Option')->with('User')->get();
    }

    public static function SaveResponse($poll_id, $user_id, $data)
    {
        $array = array();
        foreach ($data as $key => $value) {
            if ($value != null) {
                $array[] = array(
                    'poll_id' => $poll_id,
                    'poll_question_id' => $value["poll_question_id"],
                    'option_id' => $value["option_id"],
                    'user_id' => $user_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
            }
        }
        PollQuestionResponse::insert($array);
    }
}
