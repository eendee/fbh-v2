<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemSuggestion extends Model
{
	protected $fillable = ['name', 'email', 'product', 'producer', 'locations', 'reason'];
	public static $rules = array(

		'email'     => 'Required',
		'name'  => 'Required',
		'producer' => 'Required',
		'product' => 'Required',
		'reason' => 'Required',
		'locations' => 'Required'
	);
}
