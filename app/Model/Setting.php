<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

	protected $fillable = ['name', 'value'];
	public static $rules = array(

		'name'     => 'Required',
		'value'  => 'Required'
	);
}
