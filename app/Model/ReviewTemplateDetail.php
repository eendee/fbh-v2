<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ReviewTemplate;

class ReviewTemplateDetail extends Model
{

    public $timestamps = false;
    protected $fillable = ['attribute', 'description', 'activated', 'review_template_id'];
    public static $rules = array(

        'attribute'     => 'Required',
        'description'  => 'Required',
        'review_template_id' => 'Required|Integer'
    );


    public  function  ReviewTemplate()
    {
        return $this->belongsTo(ReviewTemplate::class);
    }

    public  function  ReviewDetails()
    {
        return $this->hasMany(ReviewDetail::class);
    }
}
