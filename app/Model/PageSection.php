<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PageSection extends Model
{
    //
    public $timestamps = false;

    public  function  Pages(){
        return $this->hasMany('App\Model\Page');
    }
}
