<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRank extends Model
{
    //
    public $timestamps = false;

    public  function  Users(){
        return $this->hasMany('App\User');
    }
}
