<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\ReviewTemplateDetail;
use App\Item;

class ReviewTemplate extends Model
{
	protected $fillable=['name','description'];
    public $timestamps = false;

    public  function  ReviewTemplateItems(){
        return $this->hasMany('App\Model\ReviewTemplateDetail');
    }

    public  function  Items(){
        return $this->hasMany('App\Model\Item');
    }
   
}
