<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Flag extends Model
{
    protected $fillable = ['name', 'email', 'body'];

    public static $rules = array(
        'email' => 'Required|Min:3|Max:80',
        'name'     => 'Required',
        'body'       => 'Required'
    );


    public  function  User()
    {
        return $this->belongsTo('App\User');
    }
    public  function  Review()
    {
        return $this->belongsTo('App\Model\Review');
    }
}
