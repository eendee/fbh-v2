<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PollQuestion extends Model
{
    //
    protected $fillable = ['body', 'sn', 'poll_id'];

    public static function GetByPollId($poll_id)
    {
        return PollQuestion::where('poll_id', $poll_id)->get();
    }

    public  function  Options()
    {
        return $this->hasMany('App\Model\PollQuestionOption', 'poll_question_id', 'id');
    }

    public  function  Poll()
    {
        return $this->belongsTo('App\Model\Poll');
    }

    public static function MassDeleteByPollId($PollId)
    {
        PollQuestionOption::where('poll_id', $PollId)->delete();
    }
}
