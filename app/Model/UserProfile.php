<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\File;
use App\Http\Resources\UserProfileResource;

class UserProfile extends Model
{
    //
    protected $fillable = ['user_id', 'description', 'sex', 'state_id', 'age', 'firstname', 'surname', 'picture_id', 'about', 'rank_id'];

    public  function  Picture()
    {
        return $this->belongsTo('App\Model\File', 'picture_id', 'id');
    }
    //backward compatibility
    public  function  File()
    {
        return $this->belongsTo('App\Model\File', 'picture_id', 'id');
    }

    public  function  Reviews()
    {
        return $this->hasMany('App\Model\Review', 'user_id', 'user_id');
    }
    public  function  Rewards()
    {
        return $this->hasMany('App\Model\Reward', 'user_id', 'user_id');
    }

    public  function  Comments()
    {
        return $this->hasMany('App\Model\Comment', 'user_id', 'user_id');
    }

    // public  function  State(){
    //     return $this->belongsTo('App\State');
    // }

    public function Fullname()
    {
        return $this->surname . ' ' . $this->firstname;
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public  function  Rank()
    {
        return $this->belongsTo('App\Model\UserRank', 'rank_id', 'id');
    }

    public static function CreateProfile($data)
    {
        $data['picture_id'] = "1";
        $data['rank_id'] = '1';
        UserProfile::create($data);
    }

    public function ReturnPictureUrl()
    {
        return $this->Picture->ReturnUrl();
    }

    public function ProfileResource()
    {
        return new UserProfileResource($this);
    }

    public function stats()
    {

        $data['comments_count'] = $this->Comments->count();
        $data['reviews_count'] = $this->Reviews->count();
        $data['rank'] = $this->Rank->rank;
        $data['member_since'] = $this->created_at->diffForHumans();
        $data['points'] = $this->calculatePoints();

        return $data;
    }
    public function calculatePoints() {
        $sum = 0;
        $rewards = $this->Rewards;
        foreach ($rewards as $key => $reward) {
            $sum += (int)$reward->number_of_points;
        }
        return $sum;
    }
}
