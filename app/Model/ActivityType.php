<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cache;

class ActivityType extends Model
{
    //

    public static function GetActivityTypeById($id)
    {
        $types = Cache::remember('activity_types', 60, function () {
            return ActivityType::All()->KeyBy('id')->toArray();
        });
        return $types[$id];
    }
}
