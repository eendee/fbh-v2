<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //

    public  function  Review(){
        return $this->belongsTo('App\Model\Review');
    }


    public static function getUserVoteCount($userId)
    {
        return Vote::Where('user_id',$userId)->count();
    }

    public static function getVotesForUser($userId)
    {
    	return Vote::selectRaw("votes.id")
        ->join('reviews', 'reviews.id','=','votes.review_id')
        ->where('reviews.user_id',$userId)->count();;
    }
}
