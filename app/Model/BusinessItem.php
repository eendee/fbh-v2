<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BusinessItem extends Model
{
    //
    protected $table = 'business_item';
    public  function  Item()
    {
        return $this->hasOne('App\Model\Item', 'id', 'item_id');
    }

    public  function  Business()
    {
        return $this->hasOne('App\Model\Business', 'id', 'business_id');
    }
}
