<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'category_id', 'image_id', 'description', 'review_template_id', 'featured', 'activated'];

    public static $rules = array(
        'name' => 'Required|Min:3|Max:80',
        'category_id'     => 'Required|Integer',
        'image_id'       => 'Required|Integer',
        'description'  => 'Required|AlphaNum',
        'review_template_id' => 'Required|Integer'
    );
    public  function  Category()
    {
        return $this->belongsTo('App\Model\Category');
    }

    public  function  ReviewTemplate()
    {
        return $this->belongsTo('App\Model\ReviewTemplate');
    }

    public  function  File()
    {
        return $this->belongsTo('App\Model\File', 'image_id', 'id');
    }

    public  function  Reviews()
    {
        return $this->hasMany('App\Model\Review');
    }

    public function ReviewsWithDetails() {
        foreach ($this->Reviews as $key => $review) {
            // $review->ReviewDetails;
            $review->averageRating = $review->averageRating();
        }
        return $this->Reviews;
    }

    public function returnAverageScore($rating_count = false) {
        //Count reviews with review_details
        $review_with_details = array();
        $score = 0;
        foreach ($this->Reviews as $key => $review) {
            $review->averageRating = $review->averageRating();
            if($review->averageRating > 0) {
                array_push($review_with_details, $review);
            }

            $score += $review->averageRating();
        }
        // return $review_with_details;
        $review_details_count = count($review_with_details);
        // return $review_details_count;
        if($rating_count) {
            return $review_details_count;
        }
        return $review_details_count == 0 ? 0 : round($score/$review_details_count, 2);
    }

    public  function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }

    public function GetImage()
    {
        if($this->File) {
            return $this->File->ReturnUrl();
        }
        return '';
    }

    public function GetReviewTemplateDetails()
    {
        return $this->ReviewTemplate->ReviewTemplateItems;
    }

    public  function  AdminScore()
    {
        $r = Review::getAdminItemReview($this->id);
        if (isset($r)) {
            return $r->score;
        }
    }



    public static function getItemWithMisc($id, $ids = null)
    {
        $result = self::_GetItemsWithScores($ids)->with('ReviewTemplate');

        if ($ids) {
            return $result->get();
        }
        return $result->where('items.id', $id)->first();
    }


    private static function _GetItemsWithScores($ids = null)
    {
        $result = Item::selectRaw("items.id,image_id,category_id,review_template_id, name,description,AVG(review_details.rating) as score,
            COUNT(distinct(reviews.id)) as ratings")
            ->leftjoin('reviews', function ($join) {
                $join->on('items.id', '=', 'reviews.item_id')
                    ->where('reviews.activated', '1')
                    ->where('reviews.review_type_id', '!=', 4);
            })
            ->leftjoin('review_details', 'items.id', '=', 'review_details.item_id')
            ->groupBy("items.id", 'image_id', 'category_id', 'review_template_id', 'name', 'description', "review_details.item_id")
            ->with('File')->with('Category');

        if ($ids) {
            return $result->wherein('items.id', $ids);
        }
        return $result;
    }

    public static function GetItemsWithScores($ids = null)
    {
        return self::_GetItemsWithScores($ids)->get();
    }

    public static function GetItemsPaginatedWithScores($ids = null, $skip = 0, $take = 1, $with = null)
    {
        $result = self::_GetItemsWithScores()->skip($skip)->take($take);
        if (isset($with) && count($with)) {
            foreach ($with as $graph) {
                $result->with($graph);
            }
        }
        return $result->get();
    }

    public static function GetMaxInCategoryString($limit = null)
    {
        $sql = @"SELECT  * FROM topmaxcat2  ";

        if ($limit) {
            $sql .= "LIMIT $limit";
        }
        return $sql;
    }

    public static function GetMaxInCategory($limit = null)
    {
        $sql = @"SELECT  * FROM topmaxcat2  WHERE NoOfReviews >2 "
            . " ORDER by  `NoOfReviews`  DESC,  `category_id` DESC ";

        if ($limit) {
            $sql .= "LIMIT $limit";
        }
        $entries = DB::select($sql);
        $items = array();
        $ids = array();

        foreach ($entries as $d => $r) {
            $ids[] = $r->Id;
        }
        $tops = self::_GetItemsWithScores($ids, null, null, 'Category.File');
        return $tops->get();
    }
}
