<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\ReviewTemplateDetail;
use App\Item;

class ReviewType extends Model
{

    public $timestamps = false;

    public  function  ReviewTemplateItems()
    {
        return $this->hasMany('App\Model\Review');
    }
}
