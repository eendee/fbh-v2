<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    public $timestamps = false;

    public static function GetAll()
    {
        return State::OrderBy('name')->Pluck('name', 'id')->ToArray();
    }
}
