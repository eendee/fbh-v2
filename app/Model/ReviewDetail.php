<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReviewDetail extends Model
{
    public $timestamps = false;

    public  function  Review(){
        return $this->belongsTo('App\Model\Review');
    }

    public  function  ReviewTemplateDetail(){
        return $this->belongsTo('App\Model\ReviewTemplateDetail','review_template_detail_id','id');
    }

    public static function getReviewDetailsForItem($item_id){
    	return ReviewDetail::where('item_id',$item_id)->get();
    }

    public static function getReviewSummaryForItem($item_id,$user_id=null){
        $result=ReviewDetail::selectRaw(" review_template_detail_id,review_details.item_id, AVG(rating) as rating")
        ->join('reviews', 'reviews.id','=','review_details.review_id')
        ->groupBy("review_details.item_id",'review_template_detail_id')
        ->where('review_details.item_id',$item_id)->where('reviews.activated','1');
        if($user_id!=null){
            $result->where('reviews.user_id',$user_id);
        }
        return $result->with('ReviewTemplateDetail')->get();
    }

    public static function MassDeleteByReviewId($review_id)
    {
        ReviewDetail::where('review_id', $review_id)->delete();
    }
}
