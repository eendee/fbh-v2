<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ActivityNotification extends Model
{
    //

    public  function  Activity()
    {
        return $this->belongsTo('App\Model\Activity');
    }
}
