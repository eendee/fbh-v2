<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $fillable = ['title', 'url', 'position', 'description', 'is_active', 'image' ];
}
