<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdvertRequest extends Model
{
	protected $fillable = ['name', 'email', 'phone', 'organization', 'role', 'message'];
	public static $rules = array(

		'email'     => 'Required',
		'name'  => 'Required',
		'phone' => 'Required',
		'organization' => 'Required',
		'role' => 'Required',
		'message' => 'Required'
	);
}
