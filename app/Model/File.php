<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name', 'mimetype', 'extension', 'url'];

    public  function  Items()
    {
        return $this->hasMany('App\Model\Item');
    }

    public  function  Categories()
    {
        return $this->hasMany('App\Model\Categories', 'id', 'image_id');
    }

    public  function  Reviews()
    {
        return $this->hasMany('App\Model\Review');
    }

    public  function  User()
    {
        return $this->belongsTo('App\User');
    }

    public function ReturnThumbnail()
    {
        $url = str_replace('.' . $this->extension, '', $this->url);
        $url = 'uploads/' . $url . '_200x200' . '.' . $this->extension;
        return $url;
    }

    public function ReturnUrl()
    {
        $url = str_replace('.' . $this->extension, '', $this->url);
        $url = 'uploads/' . $url . '.' . $this->extension;
        return $url;
    }


    public static function returnImageFileTypes()
    {
        return array('jpeg', 'jpg', 'png', 'gif');
    }
}
