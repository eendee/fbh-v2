<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    //
    protected $fillable = ['number_of_points', 'description', 'user_id', 'type' ];

    public  function  User()
    {
        return $this->belongsTo('App\User');
    }
}
