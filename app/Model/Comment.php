<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    public  function  File()
    {
        return $this->belongsTo('App\Model\File');
    }

    public  function  User()
    {
        return $this->belongsTo('App\User');
    }

    public  function  Profile()
    {
        return $this->belongsTo('App\Model\UserProfile', 'user_id', 'user_id');
    }

    public static function getUserCommentCount($userId)
    {
        return Comment::Where('user_id', $userId)->count();
    }
    public function GetProfile()
    {
        if ($this->profile != null) {
            return $this->profile->file;
        }
        if ($this->business != null) {
            return $this->business->image;
        }
    }
    public function GetCommenterName()
    {
        if ($this->profile != null) {
            return $this->profile->fullname();
        }
        if ($this->business != null) {
            return $this->business->name;
        }
    }

    public function business() {
        return $this->hasOne('App\Model\Business', 'user_id', 'user_id');
    }
}
