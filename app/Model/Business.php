<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    //
    protected $fillable = ['name', 'address', 'phone', 'products', 'website', 'email', 'rc_no'];

    public static $rules = array(
        'email' => 'Required|Min:3|Max:80',
        'address'     => 'Required|Min:3',
        'name'     => 'Required|Min:3',
        'products'     => 'Required|Min:3',
        'website'     => 'Required|Min:3',
        'phone'       => 'Required|Min:3',
        'rc_no'     => 'Required|Min:3'
    );
    public function returnFillable()
    {
        return $this->fillable;
    }

    public  function  Items()
    {
        return $this->belongsToMany('App\Model\Item');
    }

    public  function  Image()
    {
        return $this->belongsTo('App\Model\File', 'image_id', 'id');
    }
    public  function  Picture()
    {
        return $this->belongsTo('App\Model\File', 'image_id', 'id');
    }
    public function ReturnPictureUrl()
    {
        return $this->Picture->ReturnUrl();
    }

    // public function ReturnThumbnail()
    // {
    //     $url = str_replace('.' . $this->extension, '', $this->url);
    //     $url = 'uploads/' . $url . '_200x200' . '.' . $this->extension;
    //     return $url;
    // }

    // public function ReturnUrl()
    // {
    //     $url = str_replace('.' . $this->extension, '', $this->url);
    //     $url = 'uploads/' . $url . '.' . $this->extension;
    //     return $url;
    // }
    public function getImage() {

    }

    public function user() {
        return $this->hasOne('App\User');
    }
    public function comments() {
        return $this->hasMany('App\Model\Comment', 'user_id', 'user_id');
    }
}
