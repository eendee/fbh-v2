<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

	protected $fillable = ['name', 'email', 'title', 'body'];
	public static $rules = array(

		'email'     => 'Required',
		'name'  => 'Required',
		'title' => 'Required',
		'body' => 'Required'
	);

	public  function  MessageType()
	{
		return $this->belongsTo('App\MessageType');
	}
}
