<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $fillable = ['name', 'slug', 'parent_id', 'description', 'image_id', 'icon_id'];
    protected $table = 'categories';

    public  function  Items()
    {
        return $this->hasMany('App\Model\Item');
    }
    public  function  Parent()
    {
        return $this->hasOne('App\Model\Category', 'id', 'parent_id');
    }

    public  function  File()
    {
        return $this->hasOne('App\Model\File', 'id', 'image_id');
    }

    public  function  Icon()
    {
        return $this->hasOne('App\Model\File', 'id', 'icon_id');
    }

    public static function ReturnTree()
    {
        $all = Category::All();

        //dd($all);
        $master = array();
        //get the root
        foreach ($all as $cat) {
            $master[$cat->id] = $cat->attributes;
        }

        return self::buildTree($master);
    }

    public static function buildTree(array &$elements, $parentId = 0)
    {

        $branch = array();

        foreach ($elements as &$element) {

            if ($element['parent_id'] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['id']] = $element;
                unset($element);
            }
        }
        return $branch;
    }


    public function NumberOfReviews()
    {
        $result = DB::select(@"select COUNT(id) as id from reviews r where r.item_id IN 
            (select i.id from items i where i.category_id=?)", [$this->id]);

        return $result[0]->id;
    }

    public function GetImage()
    {
        return $this->File->ReturnUrl();
    }

    public function GetIcon()
    {
        if (isset($this->Icon)) {
            return $this->Icon->ReturnUrl();
        }
        return  $this->File->ReturnUrl();
    }


    public static function GetMajorCategories()
    {
        return Category::wherein('parent_id', array('1', '2'))->orderBy('priority')->get()->keyBy('id');
    }

    public static function GetLevelTwoCategories($cat, &$parents = array())
    {

        $parent = Category::find($cat->parent_id);
        if ($parent != null) {
            $parents[] = $parent;
            self::getLevelTwo($parent, $parents);
        }
        return $parents;
    }

    public static function GetCategoresThatHaveItems()
    {
        $result = Category::whereRaw(DB::raw("id IN (SELECT CATEGORY_ID FROM items)"));
        return $result->get();
    }

    public static function ReturnMainSubCategories()
    {
        $AllCats = array();
        $cats = Category::getMajorCategories()->sortBy('name');
        if (count($cats)) {
            foreach ($cats as $key => $c) {
                if ($c->parent_id == 1) {
                    $AllCats['products'][] = $c;
                } else {
                    $AllCats['services'][] = $c;
                }
            }
        }

        return $AllCats;
    }


    public static function ReturnMainSubCategoriesTogether()
    {
        $AllCats = array();
        $cats = Category::getMajorCategories();
        if (count($cats)) {
            foreach ($cats as $key => $c) {

                $AllCats['All'][] = $c;
            }
        }

        return $AllCats;
    }
}
