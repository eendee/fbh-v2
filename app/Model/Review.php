<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $fillable = ['title', 'body', 'file_id', 'item_id', 'review_type_id', 'location', 'overall_score', 'user_id'];
    public static $rules = array(
        'title' => 'Required|Min:3|Max:80',
        'body'     => 'Required|Integer',
        'file_id'       => 'Required|Integer',
        'item_id' => 'Required|Integer'
    );


    public  function  Item()
    {
        return $this->belongsTo('App\Model\Item');
    }

    public  function  File()
    {
        return $this->belongsTo('App\Model\File');
    }

    public  function  User()
    {
        return $this->belongsTo('App\User');
    }
    public  function  ReviewType()
    {
        return $this->belongsTo('App\Model\ReviewType');
    }

    public  function  Profile()
    {
        return $this->belongsTo('App\Model\UserProfile', 'user_id', 'user_id');
    }


    public  function  ReviewDetails()
    {
        return $this->hasMany('App\Model\ReviewDetail');
    }

    public function averageRating() {
        $count = count($this->ReviewDetails);
        $score = 0;
        foreach ($this->ReviewDetails as $key => $detail) {
            $score += (int)$detail->rating;
        }
        return $count == 0 ? 0 : round($score/$count, 2);
    }

    public  function  Comments()
    {
        return $this->hasMany('App\Model\Comment');
    }
    public  function  Votes()
    {
        return $this->hasMany('App\Model\Vote');
    }

    public function getBodyAttribute($body)
    {
        return $body == 'null' ? null : $body;
    }

    public function IsInEditWindow()
    {
        $status = false;
        $now = new \DateTime("now");
        $window = new \DateTime($this->created_at);
        $window->modify('+15 minutes');
        if ($now <= $window) {
            $status = true;
        }
        return $status;
    }

    public function GetImage()
    {
        return $this->File->ReturnUrl();
    }
    public static function GetReviewsForItemWithScore($item_id = null, $review_id = null, $order_by_desc = false, $take_featured = false, $get_by_review_ids = null)
    {
        $result = Review::selectRaw("reviews.id, title,body,reviews.item_id,file_id,reviews.user_id,reviews.created_at,
        AVG(review_details.rating) as score")
            ->join('review_details', 'reviews.id', '=', 'review_details.review_id')
            ->groupBy("reviews.id", 'title', 'body', "reviews.item_id", 'file_id', 'reviews.user_id', 'created_at')
            ->with('Comments')->with('Votes')->with('Item.Category')
            ->with('Item.File', 'File', 'Profile', 'Profile.File')->with('ReviewDetails.ReviewTemplateDetail')->where('reviews.review_type_id', "1")->where('reviews.activated', '1');

        if ($get_by_review_ids != null) {
            $result = $result->wherein('reviews.id', $get_by_review_ids);
        }

        if ($order_by_desc) {
            $result->orderBy('id', 'DESC');
        }
        if ($item_id != null) {
            $result = $result->where('reviews.item_id', $item_id);
        }

        if ($take_featured) {
            $result = $result->where('reviews.featured', '1');
        }

        if ($review_id == null) {
            return $result->get();
        }


        return $result->find($review_id);
    }

    public static function GetReviews($skip = 0, $take = 20, $conditions = null, $graphs = null, $counts = null)
    {
        $result = Review::with('ReviewDetails')
            ->with('ReviewDetails.ReviewTemplateDetail')
            ->selectRaw('*, (SELECT AVG(review_details.rating) FROM review_details WHERE reviews.id = review_details.review_id) as score');

        // $result = Review::selectRaw("reviews.id,reviews.review_type_id, reviews.overall_score, title,body,reviews.item_id,file_id,reviews.user_id,reviews.created_at,
        // AVG(review_details.rating) as score")
        //     ->join('review_details', 'reviews.id', '=', 'review_details.review_id', 'inner')
        //     ->groupBy("reviews.id", 'reviews.review_type_id', "overall_score", 'title', 'body', "reviews.item_id", 'file_id', 'reviews.user_id', 'created_at');

        if ($graphs != null && count($graphs)) {
            foreach ($graphs as $graph) {
                $result->with($graph);
            }
        }
        if ($counts != null && count($counts)) {
            foreach ($counts as $toCount) {
                $result->withCount($toCount);
            }
        }
        if ($conditions != null && count($conditions)) {
            // if (!isset($conditions['reviews.review_type_id'])) {
            //     $result->where('reviews.review_type_id', '1');
            // }
            $result->where($conditions);
        } else {
            $result->where('reviews.review_type_id', '1');
        }
        $results = $result->where('reviews.activated', '1')->skip($skip)->take($take)->orderBy('id', 'DESC')->get();
        return $results;
    }

    public static function GetSingleReview($id, $graphs = null)
    {
        $result = self::GetReviews(0, 1, array('reviews.id' => $id), $graphs);
        if ($result != null && count($result)) {
            return $result[0];
        }
        return null;
    }

    public static function getLatestReviewPosts($limit = 5, $item_id = null, &$reviewsCollection = null)
    {

        $results = $latest = Review::where('activated', '1')->where('user_id', '!=', -1)->orderBy('id', 'DESC');
        if ($latest != null) {
            //$results=$results->take($limit);
        }
        if ($item_id != null) {
            $results = $results->where('item_id', $item_id);
        }
        $results = $results->with('Profile')->paginate($limit);
        $reviewsCollection = $results;
        //dd($reviewsCollection->items());

        $a = array();
        if (count($results)) {
            foreach ($results as $r) {
                $a[$r->id]['type'] = $r->review_type_id;
                if ($r->review_type_id == 1) {
                    //get score
                    $a[$r->id]['review'] = $r;
                    $a[$r->id]['rating'] = Review::getReviewsForItemWithScore(null, $r->id);
                } else {
                    $a[$r->id]['review'] = $r;
                    $a[$r->id]['rating'] = null;
                }
            }
        }

        return $a;
    }

    public static function getUserReviewCount($userId)
    {
        return Review::Where('user_id', $userId)->count();
    }

    public static function getAdminItemReview($item_id = null, $review_id = null)
    {
        $result = Review::selectRaw("reviews.id, reviews.title,reviews.body,reviews.item_id,
        AVG(review_details.rating) as score")
            ->join('review_details', 'reviews.id', '=', 'review_details.review_id')
            ->groupBy("reviews.id", 'title', 'body', "reviews.item_id")
            ->with('ReviewDetails.ReviewTemplateDetail')
            ->where('reviews.review_type_id', "4")->where('reviews.activated', '1')
            ->where('reviews.item_id', $item_id);
        return $result->first();
    }
}
