<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $fillable = ['name', 'slug', 'page_section_id', 'excerpt', 'featured_image_id', 'template_id', 'body'];


    public  function  Section()
    {
        return $this->belongsTo('App\Model\PageSection', 'page_section_id', 'id');
    }
    public  function  Image()
    {
        return $this->hasOne('App\Model\File', 'id', 'featured_image_id');
    }

    public static function getNews($limit = 0)
    {
        $result = Page::where('page_section_id', 5);
        if ($limit != 0) {
            $result->Take($limit);
        }

        return $result->OrderByDesc('id')->get();
    }

    public static function getBlogEntries($limit = 0)
    {
        $result = Page::where('page_section_id', 6);
        if ($limit != 0) {
            $result->Take($limit);
        }

        return $result->OrderByDesc('id')->get();
    }

    public function GetImage()
    {
        return $this->Image->ReturnUrl();
    }

    public static function getSliders($filter = null)
    {
        $result = Page::where('page_section_id', 7);
        if ($filter != null) {
            $ids = explode(',', $filter);
            if (count($ids))
                $result->WhereIn('id', $ids);
        }
        return $result->with('Image')->OrderByDesc('id')->get();
    }
}
