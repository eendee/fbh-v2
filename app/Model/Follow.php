<?php

namespace App\Model;;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    function Item()
    {
        return $this->hasOne('App\Model\Item', 'id', 'object_id');
    }
    function Category()
    {
        return $this->hasOne('App\Model\Category', 'id', 'object_id');
    }

    static function getUserItems($user_id)
    {
        return self::with('Item')->where('object_type_id', 2)->where('user_id', $user_id)->get();
    }
    static function getUserCategories($user_id)
    {
        return self::with('Category')->where('object_type_id', 1)->where('user_id', $user_id)->get();
    }

}
