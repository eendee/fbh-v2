<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    //
    protected $fillable = ['name', 'description', 'start', 'end', 'activated'];

    public  function  Questions()
    {
        return $this->hasMany('App\Model\PollQuestion');
    }

    public  function  Responses()
    {
        return $this->hasMany('App\Model\PollQuestionResponse');
    }

    public static function GetActivePolls()
    {
        return Poll::where('activated', '1')->where('end', '>=', date('Y-m-d H:i:s'))->get();
    }

    public function status()
    {
        if ($this->end < date('Y-m-d H:i:s')) {
            return "ended";
        }
        return "ongoing";
    }
}
