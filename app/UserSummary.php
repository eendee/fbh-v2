<?php

namespace App;

use App\Model\Item;
use App\Model\Review;

class UserSummary
{
    public $item;
    public $reviews;

    public function __construct(Item $item, $reviews)
    {
        $this->item = $item;
        $this->reviews = $reviews;
    }
}
