<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;

Route::get('/', "HomeController@index");

Route::get('/about/', 'HomeController@About');
Route::get('/about/{slug}', 'HomeController@ShowAbout');
Route::get('write-review/{type?}', 'HomeController@WriteReview');
Route::get('management/{slug}', 'HomeController@TeamDetails');
Route::get('management', 'HomeController@Management');


Route::get('advertise', 'HomeController@Advertise');
Route::post('advertise', 'ContactController@SubmitAdvert');
Route::get('contact-us', 'ContactController@ContactUs');
Route::post('contact-us/success', 'ContactController@SaveContactUs');
Route::get('contact/list-item', 'ContactController@Suggest');
Route::post('contact/list-item/save', 'ContactController@SaveSuggestion');




Route::get('best-in-category', 'HomeController@ShowBestInCategory');
Route::get('category/compare/{id}', 'HomeController@CompareCategoryItems');

Route::get('categories', 'HomeController@categories');
Route::get('category/{id}', 'HomeController@showCategoryItems');
Route::get('category/{id}/{slug}', 'HomeController@showCategoryItems');


Route::get('items/{id}', 'HomeController@items');
Route::get('items/{id}/{slug}', 'HomeController@items');



Route::get('reviews/show/{id}', 'HomeController@showReview')->name('reviews.show');
Route::get('reviews/show/{id}/{slug}', 'HomeController@showReview');
Route::get('reviews/edit/{id}', 'HomeController@EditReview');
Route::post('review/comment/{id}', 'HomeController@SaveComment');
Route::get('review/flag/{id}', 'HomeController@FlagReview');
Route::post('review/flag/save/{id}', 'HomeController@SaveFlag');
Route::post('review/{id}/{review_id?}', 'HomeController@review');
Route::post('reviewPicture/{id}/{review_id?}', 'HomeController@PictureReview');

Route::get('users/{id}', 'HomeController@ShowUser');
#user
Route::get('users/{id}', 'HomeController@ShowUser');

Route::post('vote', 'HomeController@saveVote');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('register/verify/{token}', 'Auth\RegisterController@verify');
Route::get('register-success', 'Auth\RegisterController@RegisterSuccess')->name('register-success');

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('notificationcron', 'CustomCronController@NotificationCron');

#Businesses
Route::get('business-signup', 'ContactController@RegisterBusiness');
Route::post('business-signup-save', 'ContactController@SaveBusinessRegistration');
Route::get('business', 'BusinessAccountController@index');
Route::get('business/profile', 'BusinessAccountController@Profile');
Route::post('business/profile', 'BusinessAccountController@update');


Route::get('search', 'HomeController@Search');
Route::get('faq', 'HomeController@ShowFaq');

Auth::routes();

/**

 * Admin/user routes starts here
 *
 */

Route::get('user', 'UserController@index');
Route::get('user/profile', 'UserController@profile');
Route::post('user/profile', 'UserController@update');

Route::get('user/follow-categories', 'UserController@FollowCategories');
Route::get('user/follow-items/{id}', 'UserController@FollowObjects');
Route::post('user/follows/{id}', 'UserController@SaveFollows');
Route::get('user/notifications', 'UserController@ViewNotifications');

Route::get('user/password', 'UserController@password');
Route::post('user/password', 'UserController@savePassword');



#Begin Admin
Route::get('admin/dashboard', 'AdminController@index');
Route::get('admin', 'AdminController@index');

Route::get('admin/pages/', 'PageController@index');
Route::get('admin/pages/create/', 'PageController@create');
Route::post('admin/pages/create/', 'PageController@store');

Route::get('admin/pages/{id}/edit', 'PageController@edit');

Route::any('admin/pages/{id}/update', 'PageController@update');
Route::get('admin/pages/delete/{id}', 'PageController@delete');
Route::any('admin/pages/{id}/delete', 'PageController@destroy');


Route::resource('admin/categories', 'CategoryController');
Route::get('admin/categories/delete/{id}', 'CategoryController@delete');


Route::resource('admin/templates', 'ReviewTemplateController');
Route::get('admin/templates/delete/{id}', 'ReviewTemplateController@delete');
Route::get('admin/templates/items/{id}', 'ReviewTemplateController@items');

Route::get('admin/templates/{id}/items/add', 'ReviewTemplateController@addTemplateItem');
Route::post('admin/templates/{id}/items/add', 'ReviewTemplateController@storeTemplateItem');

Route::get('admin/templates/item/edit/{id}', 'ReviewTemplateController@editTemplateItem');
Route::put('admin/templates/item/edit/{id}', 'ReviewTemplateController@updateTemplateItem');


Route::get('admin/files/upload', 'FileController@index');
Route::post('admin/files/upload', 'FileController@upload');
Route::get('admin/files/search', 'FileController@SearchFiles');


Route::get('admin/items/search', 'ItemController@SearchItems');
Route::resource('admin/items', 'ItemController');
Route::get('admin/items/delete/{id}', 'ItemController@delete');

Route::get('admin/item/review/score/{id}', 'ReviewController@Add');
Route::post('admin/item/review/update/{id}', 'ReviewController@SaveInsert');




Route::get('admin/reviews', 'ReviewController@index');
Route::get('admin/review/edit/{id}', 'ReviewController@Edit');
Route::post('admin/review/update/{id}', 'ReviewController@Update');
Route::post('admin/review/activate/{id}', 'ReviewController@ToggleActivation');
Route::post('admin/review/feature/{id}', 'ReviewController@ToggleFeatured');
Route::post('admin/review/delete/{id}', 'ReviewController@Delete');



Route::get('admin/reviews/featured', 'ReviewController@FeaturedReviews');
Route::get('admin/reviews/search', 'ReviewController@SearchReview');
Route::get('admin/reviews/user/{id}', 'ReviewController@UserReviews');


Route::get('admin/users', 'ManageUserController@users');
Route::get('admin/users/search', 'ManageUserController@SearchUsers');
Route::get('admin/users/profile/{id}', 'ManageUserController@UserProfile');

//
Route::get('admin/settings', 'SettingsController@index');
Route::get('admin/settings/edit/{template}/{id}', 'SettingsController@edit');
Route::get('admin/settings/slider', 'SettingsController@slider');
Route::get('admin/settings/numbers', 'SettingsController@numbers');
Route::post('admin/settings/edit/{template}/{id}', 'SettingsController@saveSettings');


//

Route::get('admin/messages', 'MessageController@Index');
Route::get('admin/suggestions', 'MessageController@IndexOfItemSuggestion');
Route::get('admin/flags', 'MessageController@IndexOfFlaggedReviews');


Route::resource('admin/poll', 'PollController');
Route::get('admin/poll/delete/{id}', 'PollController@delete');
Route::post('admin/poll/dodelete/{id}', 'PollController@destroy');

Route::get('admin/poll/questions/{id}', 'PollController@PollQuestions')->name('poll.questions');

Route::get('admin/poll/questions/edit/{PollId}/{QuestionId?}', 'PollController@EditQuestion')->name('poll.questions.edit');
Route::post('admin/poll/questions/edit/{id?}', 'PollController@SaveQuestion');
Route::get('admin/poll/questions/delete/{id}', 'PollController@DeleteQuestionPreview');
Route::post('admin/poll/questions/dodelete/{id}', 'PollController@DeleteQuestion');
Route::get('admin/poll/responses/{id}', 'PollController@PollResponses');

Route::get('admin/businesses', 'BusinessController@index');
Route::get('admin/businesses/view/{id}', 'BusinessController@View');
Route::post('admin/businesses/view/{id}', 'BusinessController@CreateAccount');
Route::post('admin/businesses/deactivate/{id}', 'BusinessController@DeactivateAccount');
Route::get('admin/businesses/items/{id}', 'BusinessController@AddItemsToBusiness');
Route::post('admin/businesses/items/{id}', 'BusinessController@SaveItemsToBusiness');

Route::resource('admin/advert', 'AdvertController');
Route::get('admin/advert/{id}/makeactive', 'AdvertController@makeActive')->name('advert.makeactive');

// Route::get('admin/adverts', 'AdminController@Adverts');
// Route::post('admin/adverts', 'AdminController@CreateAdvert');
// Route::get('admin/adverts/{id}', 'AdminController@ShowAdvert');
// Route::post('admin/adverts/{id}', 'AdminController@EditAdvert');
// Route::any('admin/adverts/{id}/delete', 'AdminController@DeleteAdvert');

Route::get('admin/schedule-list', function () {
    Artisan::call('schedule:list');
    dd(Artisan::output());
});

#End Admin



Route::get('/news/', 'HomeController@News');
Route::get('/news/{slug}', 'HomeController@Page');

Route::get('/blog/', 'HomeController@Blog');
Route::get('/blog/{slug}', 'HomeController@BlogBySlug');

Route::get('/polls', 'HomeController@Polls');
Route::get('/poll/{id}', 'HomeController@RespondToPoll');
Route::post('/poll/submit/{id}', 'HomeController@SubmitPoll');

//catch all
Route::get('/{slug}', 'HomeController@Page');
