<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/adverts', 'API\AdvertController@mobile_adverts');

Route::get('/pages', 'API\PageController@index');

Route::get('/pages/sections', 'API\PageController@pageSections');
Route::get('/pages/sections/{id}', 'API\PageController@getPageBySection');
Route::get('/pages/slug/{id}', 'API\PageController@show');
Route::get('/pages/{id}', 'API\PageController@show');

Route::get('/reviews', 'API\ReviewController@index');
Route::get('/reviews/featured', 'API\ReviewController@featuredReviews');
Route::get('/reviews/{user_id}', 'API\ReviewController@userReviews');
Route::get('/item-reviews/{id}/{user_id}', 'API\ReviewController@itemReviewsByUser');
Route::get('/item/{id}/reviews', 'API\ReviewController@ItemReviews')->middleware('throttle:200,1');
Route::get('/reviews/{review}/show', 'API\ReviewController@show');




Route::get('/items', 'API\ItemController@index');
Route::get('/items/search', 'API\ItemController@search');
Route::get('/items/quick-search', 'API\ItemController@quickSearch');
Route::get('/items/bests', 'API\ItemController@bestInCategory');
Route::get('/category/{id}/items', 'API\ItemController@categoryItems');
Route::get('/category/items/{id}', 'API\ItemController@categoryItemsWithoutRating');
Route::get('/items/followed', 'API\UserController@itemsFollowed');
Route::get('/categories/followed', 'API\UserController@categoriesFollowed');
Route::get('/items/{item}', 'API\ItemController@show');
Route::get('/categories/items/{id}/best', 'API\ItemController@getBest');

Route::post('/contact/list-item/save', 'API\ContactController@SaveSuggestion');
Route::post('/business-signup-save', 'API\ContactController@SaveBusinessRegistration');


Route::get('/category', 'API\CategoryController@index');
Route::get('/category/level-two', 'API\CategoryController@getMajorCategories');
Route::get('/category/sub/{id}', 'API\CategoryController@showCategoryWithSub');
Route::get('/category/{category}', 'API\CategoryController@show');

Route::get('/polls/{all?}', 'API\PollController@index');
Route::get('/user/public/{id}', 'API\UserController@GetPublicProfile');
Route::get('/user/summary', 'UserController@sendSummary');




Route::group([

    'middleware' => 'api'

], function ($router) {
    Route::post('/user/profile', 'API\UserController@profile');
    Route::post('/user/profile/save', 'API\UserController@updateProfile');
    Route::put('/user/password', 'API\UserController@updatePassword');
    Route::post('/user/stats', 'API\UserController@statistics');
    Route::post('/reviews', 'API\ReviewController@addReview');
    Route::post('/comments', 'API\ReviewController@addReviewComment');
    Route::post('/votes', 'API\ReviewController@addReviewVote');
    Route::post('/revert-votes', 'API\ReviewController@revertReviewVote');
    Route::post('/flag', 'API\ReviewController@addReviewFlag');
    Route::post('/poll/{id}', 'API\PollController@SavePollResponses');
    Route::get('/poll/{id}', 'API\PollController@GetPollById');
    Route::post('items/followed', 'API\UserController@followItem');
    Route::post('items/unfollowed', 'API\UserController@unfollowItem');

    Route::post('categories/followed', 'API\UserController@followCategory');
    Route::post('categories/unfollowed', 'API\UserController@unfollowCategory');
});


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');
    Route::post('logout', 'API\AuthController@logout');
    Route::post('refresh', 'API\AuthController@refresh');
    Route::post('me', 'API\AuthController@me');
    Route::post('forgot-password', 'API\ForgotPasswordController@forgot');
});
