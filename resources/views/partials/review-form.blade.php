<div class="review-form">
						<br>
						<ul class="nav nav-tabs" style="font-size:.8em;">

						  <li id="review_" class="<?php echo  isset($tab)&&$tab==1?"active":"" ?>"><a data-toggle="tab" href="#review">Write review</a></li>
						  <li id="post_" class="<?php echo  isset($tab)&&$tab==2?"active":"" ?>"><a data-toggle="tab" href="#post">Add a Photo</a></li>
						</ul>

						<div class="tab-content">
						  <div id="review" class="tab-pane fade in active">
						    <div class="review-form">
								<h2>Write a review</h2>
								@if(Auth::Check())
									@if($hasReviewed==="0")
										@if(isset($template))
											{!! Form::model($review,array('url' => "review/".$item->id.$sender,'style'=> 'max-width: 100%; padding-left: 20px; padding-right:20px;','method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
										    	<div class="form-group">
										    		<span class="glyphicon glyphicon-tag"></span>
													{{Form::label('Title [optional]')}}
													{{Form::text('title',$review->title,array('class'=>'form-control','placeholder'=>'Optionally add a caption for your review'))}}

												</div>
												@if($template->id!=1)
													<div class="form-group">
													 <span class="glyphicon glyphicon-map-marker"></span>	{{Form::label('Located Near? [optional]')}}
														{{Form::text('location',$review->location,array('class'=>'form-control','placeholder'=>'Optionally add location details for vendor/service'))}}

													</div>
												@endif
												<div class="form-group">
													<span class="glyphicon glyphicon-edit"></span>
													{{Form::label('Details')}}
													{{Form::textarea('body',$review->body, array('class'=>'textarea form-control', 'rows'=>'2', 'id'=>'wysihtml5', 'required'=>'required' ))}}
												</div>
												<div class="form-group">
													<span class="glyphicon glyphicon-picture"></span>
													{{Form::label('Picture(Optional)')}}
													{{Form::file('file')}}
												</div>
												<div>
													<div class="alert alert-warning" role="alert">Legend: 1- very bad; 2- Bad; 3 – Okay; 4 – Good; 5 – excellent.</div>
												</div>
												<div class="form-group">
													@if(isset($ti))
														<ul class="star-ul">


															@foreach($ti as $t)

																<li>{{{$t->attribute}}}: <span class="text-muted"><small> {!!$t->description!!}</small> </span></li>
																<div class="stars">
																	<input class="star star-5" value="5" id="star-{{$t->id}}-5" type="radio" name="rating[{{$t->id}}]"/>
																    <label class="star star-5" for="star-{{$t->id}}-5"></label>
																    <input class="star star-4" value="4" id="star-{{$t->id}}-4" type="radio" name="rating[{{$t->id}}]"/>
																    <label class="star star-4" for="star-{{$t->id}}-4"></label>
																    <input class="star star-3" value="3" id="star-{{$t->id}}-3" type="radio" name="rating[{{$t->id}}]"/>
																    <label class="star star-3" for="star-{{$t->id}}-3"></label>
																    <input class="star star-2" value="2" id="star-{{$t->id}}-2" type="radio" name="rating[{{$t->id}}]"/>
																    <label class="star star-2" for="star-{{$t->id}}-2"></label>
																    <input class="star star-1" value="1" id="star-{{$t->id}}-1" type="radio" name="rating[{{$t->id}}]"/>
																    <label class="star star-1" for="star-{{$t->id}}-1"></label>
																</div>

															@endforeach

															<li><b>Overall Score:</b> <span class="text-muted"><small></small> </span></li>
															<div class="stars">
																<input class="star star-5" value="5" id="star-z-5" type="radio" name="overall_score"/>
															    <label class="star star-5" for="star-z-5"></label>
															    <input class="star star-4" value="4" id="star-z-4" type="radio" name="overall_score"/>
															    <label class="star star-4" for="star-z-4"></label>
															    <input class="star star-3" value="3" id="star-z-3" type="radio" name="overall_score"/>
															    <label class="star star-3" for="star-z-3"></label>
															    <input class="star star-2" value="2" id="star-z-2" type="radio" name="overall_score"/>
															    <label class="star star-2" for="star-z-2"></label>
															    <input class="star star-1" value="1" id="star-z-1" type="radio" name="overall_score]"/>
															    <label class="star star-1" for="star-z-1"></label>
															    <br>
															    <hr>
															</div>
														<ul>

													@endif
													{{ Form::hidden('id',$item->id) }}
													{{ Form::hidden('review_type_id','1') }}
													{{ Form::token() }}
													{{ Form::submit('Post review',array('class'=>'btn btn-success btn-dark pull-right'))}}
												</div>

											{!! Form::close() !!}

										@endif
									@else
										<span>You have already reviewed this item</span>
									@endif
								@else
									@include('layouts.login',array('url'=>'abc'))
								@endif
							</div>
						  </div>
						  <div id="post" class="tab-pane fade">
						    	@if(Auth::Check())
									{!! Form::model($review,array('url' => "reviewPicture/$item->id",'style'=> 'max-width: 100%; padding-left: 20px;','method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}

										<div>
											<div class="form-group">
									    		<span class="glyphicon glyphicon-tag"></span>
												{{Form::label('Title [optional]')}}
												{{Form::text('title',$review->title,array('class'=>'form-control','placeholder'=>'Optionally add a caption'))}}

											</div>

											<div class="form-group">
												<span class="glyphicon glyphicon-picture"></span>
												{{Form::label('Picture')}}
												{{Form::file('file')}}
											</div>

											<div class="form-group">

												{{ Form::token() }}
												{{ Form::hidden('id',$item->id) }}
												{{ Form::hidden('review_type_id','2') }}
												<p class="pull-left">
													{{ Form::submit('Post Picture',array('class'=>'btn btn-success btn-dark'))}}
												</p>
											</div>
										</div>

									{!! Form::close() !!}
								@else
									@include('layouts.login',array('url'=>'abc'))
								@endif
						  </div>
						  {{-- <div id="question" class="tab-pane fade">
						    	@if(Auth::Check())
									{!! Form::model($review,array('url' => "review/$item->id",'style'=> 'max-width: 100%; padding-left: 20px;','method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}

										<div>

											<div class="form-group">
									    		<span class="glyphicon glyphicon-tag"></span>
												{{Form::label('Title [optional]')}}
												{{Form::text('title',$review->title,array('class'=>'form-control','placeholder'=>'Optionally add a caption'))}}

											</div>
											<div class="form-group">
												<span class="glyphicon glyphicon-question-sign"></span>
												{{Form::label('Question')}}
												{{Form::textarea('body',$review->body, array('class'=>'textarea form-control', 'rows'=>'4',  'id'=>'wysihtml5', 'required'=>'required' ))}}
											</div>

											<div class="form-group">

												{{ Form::token() }}
												{{ Form::hidden('id',$item->id) }}
												{{ Form::hidden('review_type_id','3') }}
												<p class="pull-left">
													{{ Form::submit('Save question',array('class'=>'btn btn-success btn-dark'))}}
												</p>
											</div>
										</div>

									{!! Form::close() !!}
								@else
									@include('layouts.login',array('url'=>'abc'))
								@endif
						  </div> --}}
						</div>
						<br>

					</div>

					<br>
