<!-- Styles -->	
<style>
.home-marquee {

 overflow: hidden;
 /*position: relative;*/
 white-space: nowrap;
}
.home-marquee h5 {
 font-size: 2em;
 color: #6B0707;
/* position: absolute;*/
/* width: 100%;*/
 height: 100%;
 margin: 0;
 line-height: 20px;
/* text-align: center;*/
 /* Starting position */
 -moz-transform:translateX(100%);
 -webkit-transform:translateX(100%);	
 transform:translateX(100%);
 /* Apply animation to this element */	
 -moz-animation: home-marquee 15s linear infinite;
 -webkit-animation: home-marquee 15s linear infinite;
 animation: home-marquee 15s linear infinite;
}
/* Move it (define the animation) */
@-moz-keyframes home-marquee {
 0%   { -moz-transform: translateX(100%); }
 100% { -moz-transform: translateX(-100%); }
}
@-webkit-keyframes home-marquee {
 0%   { -webkit-transform: translateX(100%); }
 100% { -webkit-transform: translateX(-100%); }
}
@keyframes home-marquee {
 0%   { 
 -moz-transform: translateX(100%); /* Firefox bug fix */
 -webkit-transform: translateX(100%); /* Firefox bug fix */
 transform: translateX(100%); 		
 }
 100% { 
 -moz-transform: translateX(-100%); /* Firefox bug fix */
 -webkit-transform: translateX(-100%); /* Firefox bug fix */
 transform: translateX(-100%); 
 }
}
.space-50{
	 height: 50px;	
	 margin-top: 20px;
}
</style>

<?php  
	$class1="";
	$class2="";
	if(!isset($pages['scrolling-text'])){
		$class1="hidden";
		$class2="space-50";
	}

?>
<div class="home-marquee {{$class1}} {{$class2}}" style="margin-top: 20px;">
	@if(isset($pages['scrolling-text']))
		<h5>{!!$pages['scrolling-text']->body!!}</h5>
	@endif
</div>