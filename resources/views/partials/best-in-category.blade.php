<?php 
  use app\Helpers\SiteHelper ;
  $cats_in_array=array();
?>
  
<?php $_item_count=0; ?>
<div class="row">
	@if(isset($best_items))
	    @foreach($best_items as $i)


	    	<?php 
	    		if(in_array($i->category_id, $cats_in_array)){
	    			continue;
	    		}
	    	?>
	    	
	    	<?php $_item_count++  ?>
      			<div class="<?php  echo $_item_count%3==0? 'row': '' ?>">

      		
	    	<?php $cats_in_array[]=$i->category_id    ?>
			      	<div class="best-in-cat col-sm-4">
				        <div class="col-sm-5">
				          <img class="b-i-c-image img-responsive" src="{{asset($i->Category->File->ReturnUrl())}}">
				        </div>
				        <div class="col-sm-7">
				          <h5><a href="{{url('category/'.$i->Category->id)}}">{{$i->Category->name}}</a></h5>
				          <h5>
				            <a href="{{url('items/'.$i->id)}}">{{$i->name}} </a> | 
				            <a href="{{url('category/compare/'.$i->category_id)}}">Top five in category</a>
				            <br>
				            @include('partials.star-review', array('score'=>$i->score))  
				         </h5>
				          <p>from {{$i->Category->NumberOfReviews()}} review(s) </p>
				        </div>
			      	</div>

      			</div>

	    @endforeach
	@endif
</div>


<style type="text/css">
.best-in-cat{
	margin-bottom: 20px;
}

.b-i-c-image{
	height: 100px;
	max-width: 140px;
}

</style>