<?php 
  use app\Helpers\SiteHelper ;
?>
<div class="thumbnail2">


    <h2 class="entry-title">

        <a class="user-link" href="{{url('users/'.$r->Profile->id)}}" > 
          <span class="text-bold"> {{$r->Profile->Fullname()}} </span> 
          @include('partials.badge', array('rank'=>$r->Profile->Rank)) 
        </a>
        <a class="post-link" href="{{url('reviews/show/'.$r->id)}}">
           reviewed
        </a>
         <br>
        <a class="item-link" href="{{url('items/'.$r->Item->id)}}">{{$r->Item->name}}</a>
    </h2>

  <div class="item-media  border-style-1">
                 
    <a class="category-block block-v2 hidden" href="{{url('category/'.$r->Item->Category->id)}}">
      {{$r->Item->Category->name}}
    </a>
    <div style="width:96%;">
      
      <p></p>
      <div class="frame">
        <a href="{{url('reviews/show/'.$r->id)}}"> 
          <img class="image-responsive" src="{{asset('uploads/'.$r->Item->File->url)}}" 
        style="max-width:250px; max-height:150px;">                 
      </a> 
      </div>
    </div>
                   
  </div>
  
  <div>   

    @if(isset($r->title) && strlen($r->title)>2)
      <h5>{{$r->title}}</h5>
    @endif
    <?php $_score=isset($r->overall_score)?$r->overall_score:$r->score ?>
    @include('partials.star-review', array('score'=>$_score))  
    <?php 

        $body_array=null;
        $body_array=SiteHelper::ReturnWordsWithFlag($r->body,50);
    ?>
    <p>
      {{$body_array[0]}} 
    	@if($body_array[1])
    	<a href="{{url('reviews/show/'.$r->id)}}">... read more</a>
    </p>
    	@endif
     @include('partials.review-actions',$r) 
 </div>
</div>