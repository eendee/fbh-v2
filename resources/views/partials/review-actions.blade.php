<div class="action-div">
	<div class="col-sm-10">
		<i class="fa   fa-comment-o fa-lg"></i> {{$r->Comments->count()}}
		<a href="{{url('reviews/show/'.$r->id)}}">View</a>
		@include('partials.review-like-button')
	</div>
	<span class="col-sm-offset-1">
		<a class="action-flag" href="{{url('review/flag/'.$r->id)}}"><i class="fa   fa-flag fa-lg"></i> </a>

	</span>
</div>
