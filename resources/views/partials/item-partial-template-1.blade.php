<div class="col-sm-4">

	<div class="media item-1" style="margin-top:10px; margin-bottom:10px; min-height:200px;">
	  <div class="media-top">
	    <a href="{{url('items/'.$i->id)}}">
	      <div class="frame border-style-1">
	      		<?php  
	      			$_url="";
	      			try {
	      				$_url=isset($i->File)?$i->File->ReturnUrl():"uploads/default-category.jpg";      				
	      			} catch (Exception $e) {
	      				
	      			}
	      		?>
	      		<img class="media-object" style="max-height:150px; margin:auto" src="{{asset($_url)}}">
	      </div>
	    </a>
	  </div>
	  <div class="media-body">
	  	<?php  
	  		$query_string="";
	  		if(isset($type)){
	  			$query_string='?type='.$type;
	  		}
	  	?>
	    <h4 class="media-heading"><a href="{{url('items/'.$i->id.$query_string)}}">{{$i->name}}</a></h4>
	    @if(isset($rated[$i->id]))				 	
		 	@include('partials.star-review', array('score'=>$rated[$i->id]->score))	            	
        	<p>from {{$i->Reviews->count()}} rating(s)</p>
        @else
        	<p>No reviews for this item yet, be the first to review it!</p>
        @endif
	  </div>
	</div>
  
</div>

