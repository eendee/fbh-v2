<?php 
  use app\Helpers\SiteHelper ;
?>
@if(isset($news))
	@foreach($news as $n)
		<div class="col-sm-6">
			

			
			<div class="news-img frame border-style-1">
				<?php
					$img=asset('img/home023.jpg');
					if(isset($n->Image))
					{
						$img=asset($n->Image->ReturnUrl());
					}
				?>
				<img class="media-object"  style="max-height:120px;" class="img-responsive" src="{{$img}}">

			</div>
			<h4>
				<span class="news-title">
					{{$n->name}}
				</span>
			</h4>
			
		    	
			<div class="news-excerpt">
				<?php 
			        $body_array=null;
			        $body_array=SiteHelper::ReturnWordsWithFlag($n->body,20);
			    ?>
				{!!$body_array[0]!!} 
		    	@if($body_array[1])
		    	<a href="{{url('news/'.$n->slug)}}">... read more</a>
		    	@endif
			</div>
		</div>
	@endforeach
@endif