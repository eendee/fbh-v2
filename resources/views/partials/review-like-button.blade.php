<span class="like-span">
			
	<button data-review="{{$r->id}}" class="r-vote btn btn-default btn-sm" value="1">
		<i class="fa fa-thumbs-up"></i>		
		<span class="{{'like-count-1-'.$r->id}}">
			{{$r->Votes->where('value','1')->count()}} 
		</span>										
	</button>
	<button data-review="{{$r->id}}" class="r-vote btn btn-default btn-sm" value="0">
		<i class="fa fa-thumbs-down"></i>		
		<span class="{{'like-count-0-'.$r->id}}">
			{{$r->Votes->where('value','0')->count()}} 
		</span>										
	</button>
	<span class="like-notice" id="{{'like-notice-'.$r->id}}">
		@if(Auth::check())
			
		@else

			<span><a href="{{ url('login')}}">Please login first</a></span>
		@endif
		<label id="{{'ajaxNotify-'.$r->id}}"></label>
	</span>
</span> 