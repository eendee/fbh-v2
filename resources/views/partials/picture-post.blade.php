<?php 
  use app\Helpers\SiteHelper ;
?>
<div class="thumbnail2">
    <h2 class="entry-title">


      <a class="user-link" href="{{url('users/'.$r->Profile->id)}}" >
         <span class="text-bold"> {{$r->Profile->Fullname()}} </span>
         @include('partials.badge', array('rank'=>$r->Profile->Rank))
      </a>
        <a class="post-link" href="{{url('reviews/show/'.$r->id)}}">
           posted  a picture for
        </a>
         <br>
        <a class="item-link" href="{{url('items/'.$r->Item->id)}}">{{$r->Item->name}}</a>
    </h2>
  <div class="item-media  border-style-1">
                 
    <a class="category-block block-v2 hidden" href="{{url('category/'.$r->Item->Category->id)}}">
      {{$r->Item->Category->name}}
    </a>


    <div style="width:96%;">
      @if(isset($r->title) && strlen($r->title)>2)
        <h5>{{$r->title}}</h5>
      @endif
      <div class="frame">
        <a href="{{url('reviews/show/'.$r->id)}}"> 
          @if(isset($r->File))
            <img class="image-responsive" src="{{asset('uploads/'.$r->File->url)}}" 
            style="max-width:250px; max-height:150px;"> 
          @endif                        
      </a> 
      </div>
    </div>
                   
  </div>
  
  <div>   



     @include('partials.review-actions',$r) 
 </div>
</div>