<script type="text/javascript">
	$(document).ready(function(){


		$('.r-vote').on('click', function(){

			

			var vote=$(this).val()
			var review_id=$(this).attr('data-review')
			var status="";

			
		
			$.ajax({
                type: "POST", 	
                url: "{{url('vote')}}", 

                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{'review':review_id,'vote':vote}, 

                //contentType: 'application/json',		// content type sent to server
                dataType: "json", 	//Expected data format from server
                processdata: true, 	//True or False
                success: function (result) { //On Successful service call
                    _label='#ajaxNotify-'+review_id
                   //console.log(result);
                    if(result.status==="1"){
                    	var current=$('.like-count-'+vote+"-"+review_id);
						currentCount=parseInt(current.text());
                    	currentCount++;
                    	current.text(currentCount);
                    }
                   $(_label).html(result.message);
                    
                    
                },
                error: function (xhr, status, error) {
                    //alert(xhr.responseText);
                   $(_label).html(error);
                }	// When Service call fails


            });

			_notice="like-notice-"+review_id
			$(_notice).show().delay(5000).fadeOut();

	    })
	})
</script>

<style type="text/css">

	.like-notice{
		display: none;
	}
	.like-span:hover .like-notice{
		display: inline;
		/*clear: both;*/
	}
	.ul-no-pad{
		padding: 0;
	}

	.li-no-style li {
		list-style-type: none;
	}
	.action-div{
		margin-top: 5px;
	}

	.r-vote{
		color: #6B0707;
	}
</style>