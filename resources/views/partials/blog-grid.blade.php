<?php 
  use app\Helpers\SiteHelper ;
?>
@if(isset($posts))
	@foreach($posts as $n)
		<div class="col-sm-4">
			

			
			<div class="card">
				<div class="card-body">
					<div class="">
						<?php
							$img=asset('img/home023.jpg');
							if(isset($n->Image))
							{
								$img=asset($n->Image->ReturnUrl());
							}
						?>
						<img class="media-object img-responsive" src="{{$img}}">

					</div>
					<h4>
						<span class="news-title">
							{{$n->name}} <small> <span class="pull-right text-muted text-italics ">{{
								SiteHelper::ReturnDayFromDateObject($n->created_at)}}</span></small>
						</span>
					</h4>
					
				    	
					<div class="news-excerpt">
						<?php 
					        $body_array=null;
					        $body_array=SiteHelper::ReturnWordsWithFlag($n->body,20);
					    ?>
						{!!$body_array[0]!!} 
				    	@if($body_array[1])
				    	<a href="{{url('blog/'.$n->slug)}}">... read more</a>
				    	@endif
					</div>
				</div>
			</div>
		</div>
	@endforeach
@endif