<?php 
	//$xyz=urlencode(url()->current());
	$xyz = urlencode(
			'http' .
			($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://' .
			$_SERVER['HTTP_HOST'] .
			$_SERVER['PHP_SELF']
		);
	$s_title=urlencode($s_title);
 ?>
<p class="fbh-social">
	<a href="https://www.facebook.com/sharer/sharer.php?u={{$xyz}}&display=popup" class="share facebook">
		
	</a>

	<a  style="color:#00abf0;" href="https://twitter.com/intent/tweet?url={{$xyz}}&text={{urlencode($s_title)}}" class="share twitter">
	
	</a>	

	<a style="color:red" href="https://plus.google.com/share?url={{$xyz}}" class="share google">
		
	</a>

	<a href="http://www.linkedin.com/shareArticle?mini=true&url={{$xyz}}&source={{urlencode($s_title)}}" class="share linkedin">
	
	</a>
</p>


<script>
// create social networking pop-ups
(function() {
	// link selector and pop-up window size
	var Config = {
		Link: "a.share",
		Width: 500,
		Height: 500
	};

	// add handler links
	var slink = document.querySelectorAll(Config.Link);
	for (var a = 0; a < slink.length; a++) {
		slink[a].onclick = PopupHandler;
	}

	// create popup
	function PopupHandler(e) {

		e = (e ? e : window.event);
		var t = (e.target ? e.target : e.srcElement);

		// popup position
		var
			px = Math.floor(((screen.availWidth || 1024) - Config.Width) / 2),
			py = Math.floor(((screen.availHeight || 700) - Config.Height) / 2);

		// open popup
		var popup = window.open(t.href, "social", 
			"width="+Config.Width+",height="+Config.Height+
			",left="+px+",top="+py+
			",location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1");
		if (popup) {
			popup.focus();
			if (e.preventDefault) e.preventDefault();
			e.returnValue = false;
		}

		return !!popup;
	}

}());
</script>
<style type="text/css">
  
  .fbh-social a.facebook::before{
    font-family: FontAwesome;
     content: '\f230'; color: #3c599f;
  }
  .fbh-social a.twitter::before{
    font-family: FontAwesome;
     content: '\f081'; color: #337ab7;
  }
  .fbh-social a.google::before{
    font-family: FontAwesome;
     content: '\f0d5'; color: red;
  }
  .fbh-social a.linkedin::before{
    font-family: FontAwesome;
     content: '\f0e1'; color: #337ab7;
  }
</style>