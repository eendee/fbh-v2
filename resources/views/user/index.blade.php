@extends('front.layouts.main')
<?php
	$picture="assets/img/user_icon.png";
	if(isset($profile->picture_id)){
		$picture='uploads/'.$profile->file->url;
	}

?>
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
			<h1 class="">
				My Dashboard

				<div class="pull-right">
					<a href="{{url('user/notifications')}}" class="btn btn-dark btn-danger">
						Notifications
						@if($notifications_count>0)
							<span class="badge badge-danger">{{$notifications_count}}</span>
						@endif
					</a>
				</div>
			</h1>
			<div>
				<p>You can now follow your favorite items and get notified when it has a new post/review. Click <a href="{{url('user/follow-categories')}}">here</a> to follow items</p>
			</div>
			<hr class="colorgraph">
			<h4 class="text-muted">Welcome {{$user->profile->firstname}}</h4>
			@if(Session::has('message'))
				<div class="alert alert-success">
					{{Session::get('message')}}
				</div>
			@endif
		<div id="fbh-main" class="col-md-3">
			<div class="row">
				<div style="padding:5px; border:solid 1px #f5f5f5;">
					<div>
						<img class="img-responsive"  src="{{asset($picture)}}">
					</div>
					<table class="table table-striped">
						<tr>
							<td>Products Reviewed <span class="badge">{{$stats->reviews}}</span></td>
						</tr>
						<tr>
							<td>No. of Comments <span class="badge">{{$stats->comments}}</span></td>
						</tr>
						<tr>
							<td>Items Voted  <span class="badge">{{$stats->votes}}</span></td>
						</tr>
						<tr>
							<td>Community Score <span class="badge">{{$stats->TotalVotes}}</span></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="col-sm-8">

				<h3>

					@if(isset($profile->firstname)&&isset($profile->surname))
                        <span>{{$profile->firstname.' '.$profile->surname}}</span>
					@else
						<span>
							Your name goes here!
						</span>
					@endif

				</h3>
				<h5 style="display: flex; flex-direction: row;">
                    {{-- Rank: @include('front.partials.badge', array('rank'=>$profile->Rank)) --}}
                    @php $size = $stats->badges; @endphp
                    @include('front.partials.point-badge')

					{{-- Rank: @include('front.partials.badge', array('rank'=>$profile->Rank)) {{$profile->Rank->rank}} --}}
					Points: {{ $stats->points}}

				</h5>
				<p>
					@if(isset($profile->about))
						<span>
							{{$profile->about}}
						</span>
					@else
						<span>
							About you goes here
						</span>
					@endif
				</p>
			</div>
			<div class="col-sm-4">
				<div class="well">
					<ul class="nav">
						<li>
							<a href="{{url('user/profile')}}">Update Profile</a>
						</li>
						<li>
							<a href="{{url('user/follow-categories')}}">Follow Categories/Items</a>
						</li>
						<li>
							<a href="{{url('user/password')}}">Change Password</a>
						</li>
						<li>
							<a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                    </form>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12 hidden">
				<h3>Recent Activities</h3>
				<table class="table table-striped">
					<tr>
						<th>SN</th>
						<th>Category</th>
						<th>Date</th>
					</tr>
					@if(isset($activities))
					<?php $i=1;?>
						@foreach($activities as $a)
							<tr>
								<td>{{{$i}}}</td>
								<td>{{{$a->activity}}}</td>
								<td>{{{date('j F, Y', strtotime($a->created_at))}}}</td>
							</tr>
						<?php $i++;?>
						@endforeach
					@endif

				</table>
			</div>
			<div class="col-sm-12">

				<h3>Your Reviews</h3>
				@include('user/reviews')
			</div>
		</div>
	</div>
</div>
@endsection
