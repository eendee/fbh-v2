@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<h1 class="">My notifications </h1>
		<p class="well well-success">
			You get notified whenever a product/service you are following gets a new review. To make changes to the list of items you follow please click <a href="{{url('user/follow-categories')}}">here</a> 
		</p>
			@if(Session::has('message'))
				<div class="alert alert-success">
					{{Session::get('message')}}
				</div>
			@endif
		<hr class="colorgraph">
		<div class="col-md-6">
				<div>
					<table class="table table-striped">
						<?php //dd($catsCount) ?>
						@if(isset($notifications) && count($notifications))
							@foreach($notifications as $n)
								<tr>
									<td>
										{{$n->Activity->details}}
									</td>
									<td>
										<a class="btn btn-dark btn-danger" href="{{$n->Activity->url}}">View</a>
									</td>
								</tr>							
							@endforeach
						@else
							<tr>
								<td><p>You have no unread notifications</p></td>
							</tr>
						@endif
					</table>
					<p><a href="{{url('user')}}"> Back to dashboard</a></p>
				</div>
		</div>
	</div>
</div>
@endsection
