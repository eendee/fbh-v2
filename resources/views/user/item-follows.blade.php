@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<h1 class="">Follow Categories and Items </h1>
		<hr class="colorgraph">
		<div class="col-md-6">
				@if(Session::has('message'))
					<div class="alert alert-success">
						{{Session::get('message')}}
					</div>
				@endif
				@if(Session::has('error'))
					<div class="alert alert-danger">
						{{Session::get('error')}}
					</div>
				@endif
			{!! Form::model($user,array('url' => 'user/follows/'.$id,'method' => 'post', 'class'=>'form-horizontal')) !!}
		    	<div>
		    		<table class="table table-stripped table-bordered">
						<tr>
							<th>Item</th>
							<th></th>
						</tr>

						@if(isset($items))
							@foreach($items->sortBy('name') as $i)
								<?php 
									$do_check="";
									if(in_array($i->id, array_keys($mappings))){
										$do_check="checked";
									}
								?>
								<tr>
									<td>{{{$i->name}}}</td>
									
									<td>
										<input type="checkbox" name="value[]" {{$do_check}} value="{{$i->id}}">
									</td>
								</tr>
							@endforeach
							<tr>
								<td></td>
								<td>
									{{Form::hidden('object_type_id','2')}}
									{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
									
								</td>
							</tr>
						@endif

					</table>
		    	</div>	
			{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
