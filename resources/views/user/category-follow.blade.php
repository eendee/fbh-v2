@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<h1 class="">Follow Categories and Items </h1>
			@if(Session::has('message'))
				<div class="alert alert-success">
					{{Session::get('message')}}
				</div>
			@endif
		<hr class="colorgraph">
		<div class="col-md-6">
				<div>
					<table class="table table-striped">
						<tr>
							<th>Categories</th>
							<th>Following</th>
							<th></th>
						</tr>
						<?php //dd($catsCount) ?>
						@if(isset($categories))
							@foreach($categories as $c)
								<tr>
									<td>
										{{$c->name}}
									</td>
									<td>
										<span class="badge">
											@if(isset($catsCount[$c->id]))
												{{count($catsCount[$c->id])}}
											@else
												0
											@endif
										</span>
									</td>
									<td>
										<a class="btn btn-default" href="{{url('user/follow-items/'.$c->id)}}">Select Items</a>
									</td>
								</tr>							
							@endforeach
						@endif
					</table>
					<p><a href="{{url('user')}}"> Back to dashboard</a></p>
				</div>
		</div>
	</div>
</div>
@endsection
