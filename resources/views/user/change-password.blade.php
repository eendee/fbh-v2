@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<h1 class="">Change Your Password </h1>
		<hr class="colorgraph">
		<div class="col-md-6">
			@if(Session::has('message'))
				<div class="alert alert-danger">
					{{Session::get('message')}}
				</div>
			@endif
			{!! Form::model($user,array('url' => 'user/password','method' => 'post', 'class'=>'form-horizontal')) !!}
		 
				<div class="form-group">
					{{Form::label('Old Password')}}
					{{Form::password('oldpassword',array('class'=>'form-control', 'required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('New Password')}}
					{{Form::password('newpassword',array('class'=>'form-control', 'required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('Retype New Password')}}
					{{Form::password('newpasswordconfirm',array('class'=>'form-control', 'required'=>'required'))}}
				</div>
	
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-danger btn-dark pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
