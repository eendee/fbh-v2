@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<h1 class="">Edit My Profile: <span class="text-muted">{{$user->Profile->Fullname()}}</span> </h1>
		<hr class="colorgraph">
		<div class="col-md-6">
				@if(Session::has('message'))
					<div class="alert alert-danger">
						{{Session::get('message')}}
					</div>
				@endif
			{!! Form::model($user,array('url' => 'user/profile','method' => 'post','enctype'=>'multipart/form-data', 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Surname')}}
					{{Form::text('surname',$profile->surname,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Firstname')}}
					{{Form::text('firstname',$profile->firstname,array('class'=>'form-control'))}}
				</div>

				<div class="form-group">
					<?php 
						$male="";
						$female="";
						//dd($user->Profile);
						if(isset($user->Profile->sex)){
							if(strtolower(substr($user->Profile->sex, 0,1))==='m'){
								$male='checked="checked"';
							}
							if(strtolower(substr($user->Profile->sex, 0,1))==='f'){
								$female='checked="checked"';
							}
						}
						//dd($user->sex);
					 ?>
					{{Form::label('Sex')}}<br>
					<input <?php echo $male  ?> name="sex" type="radio" value="M"> Male
					<input <?php echo $female  ?> name="sex" type="radio" value="F"> Female
				</div>
				<div class="form-group">
					{{Form::label('Age')}}
					{{Form::text('age',$profile->age,array('class'=>'form-control datepicker'))}}
				</div>
				<div class="form-group">
					{{Form::label('State of residence')}}
					{{Form::select('state_id', array(''=>'Select State')+$states,$profile->state_id,array('class'=>'form-control', 'requried'=>'required'))}}
                                
				</div>
				<div class="form-group">
					{{Form::label('Profile Picture')}}
					{{Form::file('file')}}
				</div>
				<div class="form-group">
					{{Form::label('About me')}}
					{{Form::textarea('about',$profile->about,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
</div>

 <script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker({
            inline: true,
            sideBySide: true
        });
    });
</script>
@endsection
