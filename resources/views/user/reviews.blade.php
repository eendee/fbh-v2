<div>
	@if(isset($reviews))

		<table class="table table-striped">
            @foreach($reviews as $r)
                <tr>

                    <td>
                        @if ($r->Item->name && $r->Item->name != 'null')
                            <h5 style="font-weight: bold">{{$r->Item->name}}</h5>
                        @endif
                        <span class="text-muted pull-right">  {{ date('j F, Y', strtotime($r->updated_at))}} </span>
                        @if ($r->title && $r->title != 'null')
                            {{$r->title}}
                            <hr>
                        @endif


                        {{$r->body}}
                        <p style="margin-top: 5px">
                            <a class="btn btn-danger btn-sm btn-dark pull-right" href="{{url('reviews/show/'.$r->id)}}">View</a>
                        </p>
                    </td>

                </tr>
            @endforeach

        </table>
        <p>
            {{ $reviews->links() }}
        </p>
	@endif
</div>
