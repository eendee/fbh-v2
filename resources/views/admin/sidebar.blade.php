<?php

	//dd($menu);
?>
<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a href="javascript:;"><img src="{{asset('assets/img/user-13.jp')}}g" alt="" /></a>
						</div>
						<div class="info">
							Welcome
							<small>Admin Dashboard</small>
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="has-sub {{isset($menu['home'])?$menu['dashboard']:""}}">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-laptop"></i>
						    <span>Dashboard</span>
					    </a>
						<ul class="sub-menu">
						     <li><a href="{{url('admin/dashboard')}}">Admin Dashboard</a></li>
						</ul>
					</li>
					<li class="has-sub {{isset($menu['categories'])?$menu['categories']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Categories</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/categories/')}}">View All</a></li>
						    <li><a href="{{url('admin/categories/create')}}">Add New</a></li>
						</ul>
					</li>


					<li class="has-sub {{isset($menu['items'])?$menu['items']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Items</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/items/')}}">View All</a></li>
						    <li><a href="{{url('admin/items/create')}}">Add New</a></li>
						</ul>
					</li>


					<li class="has-sub {{isset($menu['pages'])?$menu['pages']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Pages</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/pages/')}}">View Pages</a></li>
						    <li><a href="{{url('admin/pages/create')}}">Add New</a></li>
						    <li><a href="{{url('admin/settings/')}}">Settings</a></li>
						</ul>
					</li>

					<li class="has-sub {{isset($menu['review_templates'])?$menu['review_templates']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Review Templates</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/templates/')}}">View All</a></li>
						    <li><a href="{{url('admin/templates/create')}}">Add New</a></li>
						</ul>
					</li>
					<li class="has-sub {{isset($menu['files'])?$menu['files']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Files</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/files/upload')}}">View All</a></li>
						</ul>
					</li>
					<li class="has-sub {{isset($menu['users'])?$menu['users']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Users</span>
						</a>
						<ul class="sub-menu">
						     <li><a href="{{url('admin/users')}}">View All</a></li>
						</ul>
					</li>

					<li class="has-sub {{isset($menu['reviews'])?$menu['reviews']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Reviews</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/reviews/featured')}}">Featured Reviews</a></li>
						    <li><a href="{{url('admin/reviews')}}">All Reviews</a></li>
						</ul>
					</li>

					<li class="has-sub {{isset($menu['messages'])?$menu['messages']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Messages</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/messages')}}">All Messages</a></li>
						    <li><a href="{{url('admin/suggestions')}}">Item Suggestions</a></li>
						    <li><a href="{{url('admin/flags')}}">Flags</a></li>
						</ul>
					</li>
					<li class="has-sub {{isset($menu['businesses'])?$menu['businesses']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Businesses</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/businesses')}}">All Businesses</a></li>

						</ul>
					</li>
					<li class="has-sub {{isset($menu['polls'])?$menu['polls']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Polls</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{url('admin/poll')}}">All Polls</a></li>

						</ul>
					</li>

                    <li class="has-sub {{isset($menu['categories'])?$menu['categories']:""}}">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<i class="fa fa-inbox"></i>
							<span>Adverts</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="{{route('advert.index')}}">View All</a></li>
						    <li><a href="{{route('advert.create')}}">Add New</a></li>
						</ul>
					</li>

			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
