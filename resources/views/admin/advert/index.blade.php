@extends('admin/_layout')

@section('content')

	<div>
		<div class="pull-right hidden">
			<form id="searchUser" action="{{url('admin/users/search/') }}" method="get">


	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>

		<table class="table table-stripped table-bordered">

			<tr>
				<th>SN</th>
				<th>Title</th>
				<th>Image</th>
				<th>Advert Url</th>
                <th>Description</th>
                <th>Position</th>
				<th>Date</th>
				<th></th>
			</tr>

			@if(isset($adverts))
				@foreach($adverts as $key => $advert)
					<tr>
						<td width="2%">{{$key + 1}}</td>
						<td width="12%">{{$advert->title}}</td>
						<td width="10%">
                            <div>
                                {{-- {{$advert->image}} --}}
                                <img style="max-height:50px; width: 50px; object-fit: contain; border-radius: 6px;" class="img-responsive" src="{{asset($advert->image)}}">
                            </div>
                        </td>
                        <td width="12%">{{$advert->url}}</td>
                        <td width="15%">{{$advert->description}}</td>
						<td width="10%">{{$advert->position}}</td>
						<td width="10%">
							{{ date('j F, Y g:i a', strtotime($advert->created_at))}}
						</td>
						<td width="16%">
                            {!! Form::open(['route' => ['advert.destroy', $advert->id], 'method'=>'delete']) !!}
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                                <a href="{{ route('advert.edit', $advert->id )}}" class="btn btn-info btn-xs">Edit</a>
                                @if ($advert->is_active)
                                    <a href="{{ route('advert.makeactive', ['id' => $advert->id, 'makeactive' => true] )}}" class="btn btn-warning btn-xs">Make inactive</a>
                                @else
                                    <a href="{{ route('advert.makeactive', ['id' => $advert->id, 'makeactive' => false] )}}" class="btn btn-success btn-xs">Make active</a>
                                @endif

                            {!! Form::close() !!}

                        </td>

					</tr>
				@endforeach
		    @endif



		</table>
        @if ($adverts->count() == 0)
        <div class="panel">
            <div class="panel-body">
                <h4>You have no adverts yet</h4>
                <a href="{{ route('advert.create')}}" class="btn btn-info">Add Advert</a>
            </div>
        </div>
        @endif
		{{ $adverts->links() }}

	</div>

@stop
