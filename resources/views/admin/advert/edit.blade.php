@extends('admin/_layout')

@section('content')
	<div class="panel panel-inverse col-lg-12">


		<div class="panel-body">
            <div class="row" style="margin-bottom: 20px; padding-left: 15px;">
                <div class="col-md-6">
                    <h4>Edit Advert</h4>
                </div>
                <div class="col-md-6">
                    <a href="{{route('advert.index')}}" class="btn btn-info">View all Adverts</a>
                </div>
            </div>
			{!! Form::model($advert,array('route' => ['advert.update', $advert->id], 'method' => 'put', 'class'=>'col-md-8', 'enctype'=>'multipart/form-data')) !!}
		    	<div class="form-group">
					{{Form::label('Title')}}
					{{Form::text('title',$advert->title,array('class'=>'form-control'))}}

				</div>
                <div class="form-group">
                    {{ Form::label('Advert Position') }}
                    {{ Form::select('position', ['left' => 'Left Side of Page', 'right' => 'Right Side of Page', 'bottom' => 'Bottom of Page', 'top' => 'Top of Page', 'app' => 'Show in App'], $advert->position,
                    ['placeholder' => 'Pick advert position...', 'class'=>'form-horizontal form-control', 'required'=>'required']) }}
                </div>
                <div class="form-group">
                    {{Form::label('Description')}}
                    {{Form::textarea('description',$advert->description, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
                </div>
                <div class="form-group">
                    {{Form::label('Advert Image')}}
                    {{Form::file('file')}}
                </div>
                <div class="form-group">
                    {{Form::label('Advert Url(optional)')}}
                    {{Form::text('url',$advert->url,array('class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    {{ Form::submit('Update',array('class'=>'btn btn-success col-md-4'))}}
                </div>
                {{ Form::token() }}

			{!! Form::close() !!}
		</div>
	</div>
@stop
