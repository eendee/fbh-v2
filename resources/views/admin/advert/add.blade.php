@extends('admin/_layout')

@section('content')
	<div class="panel panel-inverse col-lg-12">
        @if ($errors->any())
            <div class="alert alert-danger" style="margin-top: 10px;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


		<div class="panel-body" style="padding-top: 20px; padding-bottom: 50px;">
            <div class="row" style="margin-bottom: 20px; padding-left: 15px;">
                <div class="col-md-6">
                    <h4>Add Advert</h4>
                </div>
                <div class="col-md-6">
                    <a href="{{route('advert.index')}}" class="btn btn-info">View all Adverts</a>
                </div>
            </div>
            <div >
                {!! Form::open(['route' => 'advert.store', 'enctype'=>'multipart/form-data', 'class' => 'col-md-8']) !!}
                {{-- {!! Form::model($page,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!} --}}
                    <div class="form-group">
                        {{Form::label('Title')}}
                        {{Form::text('title',null,array('class'=>'form-control', 'required'=>'required'))}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Advert Position') }}
                        {{ Form::select('position', ['left' => 'Left Side of Page', 'right' => 'Right Side of Page', 'bottom' => 'Bottom of Page', 'top' => 'Top of Page', 'app' => 'Show in App'], null,
                        ['placeholder' => 'Pick advert position...', 'class'=>'form-horizontal form-control', 'required'=>'required']) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('Description')}}
                        {{Form::textarea('description',null, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
                    </div>
                    <div class="form-group">
                        {{Form::label('Advert Image')}}
                        {{Form::file('file')}}
                    </div>
                    <div>
                        <h6>N/B: Please upload landscape image for positions 'Top of Page', 'Bottom of page', 'Show in App'</h6>
                    </div>
                    <div class="form-group">
                        {{Form::label('Advert Url(optional)')}}
                        {{Form::text('url',null,array('class'=>'form-control'))}}
                    </div>
                    <div style="margin-bottom: 30px;">
                        {{ Form::submit('Save',array('class'=>'btn btn-success col-md-4'))}}
                    </div>
                    {{ Form::token() }}
                {!! Form::close() !!}
            </div>
		</div>
	</div>
@stop
