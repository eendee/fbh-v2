@extends('admin/_layout')



@section('content')

	<div>
		<?php $count=11; ?>
		<div class="col-sm-6">
			<h2>Add/Edit question for {{$poll->name}}</h2>
			{!! Form::model($question,array('url' => 'admin/poll/questions/edit/'.$question->id,'method' => 'post', 'class'=>'form-horizontal')) !!}
			<div class="form-group">
				{{Form::hidden('poll_id',$poll->id)}}
				{{Form::label('Question Body')}}
				{{Form::textarea('body',$question->body,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
			</div>
				<?php 
					for ($i=1; $i < $count; $i++) {   ?>
						<div class="form-group">
							{{Form::label('Optoin '.$i)}}
							{{Form::textarea('option[]',isset($options[$i-1])?$options[$i-1]->body:'',array('class'=>'textarea form-control', 'id'=>'a' ))}}
						</div>
				<?php
					}
				?>
				
				{{ Form::token() }}
				<div class="form-group">
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
			{!! Form::close() !!}
		</div>
	<style type="text/css">
		#a{
			height: 50px;
		}

	</style>
	</div>


@stop