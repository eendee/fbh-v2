@extends('admin/_layout')



@section('content')

	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="col-sm-12">
				<h3>Poll Details</h3>
				<div class="well">
					<h3>
					{{$poll->name}}
					</h3>
					<section>
						{!!
							$poll->description
						!!}
					</section>
				</div>
			</div>
			<div class="">
				<div class="col-sm-10">
					<table class="table table-stripped table-bordered">
						<tr cl>
							<th>User</th>
							@if(isset($questions))
								@foreach($questions as $q)
									<th class="poll-question-div">
																
									</th>
								@endforeach
							@endif
						</tr>
						
							@if(isset($responses))
								@foreach($responses as $email=>$responses)
									<tr>
										<td>
											{{$email}}							
										</td>
										@foreach($responses as $r)
											<th class="">
												<span class="text-muted text-success" style="font-size: .8em;">
													{!!$r->question!!}
												</span>
												{!!$r->answer!!}								
											</th>
										@endforeach
									</tr>
								@endforeach
							@endif							
						
					</table>

					
				</div>
				
			</div>
			
		</div>
		
	</div>

@stop