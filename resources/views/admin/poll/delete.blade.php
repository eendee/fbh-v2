@extends('admin/_layout')

@section('content')
	{!! Form::model($poll,array('url' => "admin/poll/dodelete/$poll->id/",'method' => 'post')) !!}
    	<div class="form-group">
			{{Form::label('Confirm Delete: ')}}
			{{"$poll->name ?"}}
			<p class="text-danger">This will delete all the questions and options associated with this poll.</p>
		</div>
		<div class="form-group">
			{{Form::hidden('id')}}
		</div>
		<div>
			{{ Form::submit('Delete')}}
		</div>
		{{ Form::token() }}
	{!! Form::close() !!}
@stop