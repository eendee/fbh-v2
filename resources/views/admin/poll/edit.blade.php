@extends('admin/_layout')
<?php 
	$url = 'admin/poll';
	$method='post';
	if(isset($poll)&&$poll->id>=1){
		$url="admin/poll/$poll->id";
		$method="put";
	}
?>

@section('content')
	<div class="panel panel-inverse col-lg-6">


		<div class="panel-body">
			{!! Form::model($poll,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Name')}}
					{{Form::text('name',$poll->name,array('class'=>'form-control'))}}
				</div>
						
			
				<div class="form-group">
					{{Form::label('description')}}
					{{Form::textarea('description',$poll->description,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div class="form-group">
					{{Form::label('Start')}}
					{{Form::text('start',$poll->start,array('class'=>'form-control datepicker'))}}
				</div>
				<div class="form-group">
					{{Form::label('End')}}
					{{Form::text('end',$poll->end,array('class'=>'form-control datepicker'))}}
				</div>
				<div class="form-group">
					{{Form::label('Activated')}}
					{{Form::hidden('activated','0')}}
					{{Form::checkbox('activated')}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
	 <script type="text/javascript">
	    $(function () {
	        $('.datepicker').datepicker({
	            inline: true,
	            autoclose: true,
	            format:'yyyy-mm-dd'
	        });
	    });
</script>
@stop