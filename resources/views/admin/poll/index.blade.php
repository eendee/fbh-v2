@extends('admin/_layout')



@section('content')
	<div>
		<a href="{{url('admin/poll/create')}}" class='btn btn-primary pull-right'>
			Add New
		</a>

	</div>
	<div>
	<h2>All polls</h2>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>Start</th>
				<th>End</th>
				<th>Activated</th>
				<th>No. of questions</th>
				<th>Responses</th>
				<th></th>
			</tr>
			@if(isset($polls))
				@foreach($polls as $poll)
					<tr>
						<td>{{$poll->name}}</td>
						<td>{{$poll->start}}</td>
						<td>{{$poll->end}}</td>
						<td>{{$poll->activated==1?'true':'false'}}</td>
						<td>
							<span class="badge">{{isset($poll->Questions)? $poll->Questions->count():0}}</span>
							<a href="{{url('admin/poll/questions/'.$poll->id)}}">Edit</a>
						</td>
						<td>
							<span class="badge">{{isset($poll->Questions)? $poll->Responses->count():0}}</span>
							<a href="{{url('admin/poll/responses/'.$poll->id)}}">View</a>
						</td>
						<td>
							<a href="{{url('admin/poll/'.$poll->id.'/edit')}}">Edit</a> |
							<a class="" href="{{url('admin/poll/delete/'.$poll->id)}}">Delete</a>
						</td>

					</div>
					</tr>
				@endforeach
		@endif
		</table>

	</div>

@stop
