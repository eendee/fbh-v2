@extends('admin/_layout')

@section('content')
	{!! Form::model($question,array('url' => "admin/poll/questions/dodelete/$question->id/",'method' => 'post')) !!}
    	<div class="form-group">
			{{Form::label('Confirm Delete of Question: ')}}
			{{"$question->body ?"}}
		</div>
		<div class="form-group">
			{{Form::hidden('id')}}
		</div>
		<div>
			{{ Form::submit('Delete')}}
		</div>
		{{ Form::token() }}
	{!! Form::close() !!}
@stop