@extends('admin/_layout')



@section('content')

	<div>
	<h2>Questions in  {{$poll->name}}</h2>
		<a href="{{url('admin/poll/questions/edit/'.$poll->id.'/')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Questions</th>
				<th></th>
			</tr>
			<?php   ?>
			@if(isset($questions))
				@foreach($questions as $q)
					<tr>
						<td>{!!$q->body!!}
							@if(isset($q->Options))
								<ul>
									@foreach($q->Options as $o)
									<li>
										{{$o->body}}
									</li>
									@endforeach
								</ul>
								
							@endif
						</td>
						<td>
							<a href="{{url('admin/poll/questions/edit/'.$poll->id.'/'.$q->id)}}">Edit</a> |
							<a class="" href="{{url('admin/poll/questions/delete/'.$q->id)}}">Delete</a>
						</td>	
								
					</div>
					</tr>
				@endforeach
			@endif
			
		</table>
		<div>
			
			<?php  if(count($questions)==0){

				echo("No questions added yet");
			}

			?>
		</div>
	</div>


@stop