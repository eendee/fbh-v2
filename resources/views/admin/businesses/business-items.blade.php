@extends('admin/_layout')


@section('content')
	<div>
            <div class="col-sm-8">
                <h2>Items belonging to {{$b->name}} </h2>
            </div>

            {{-- {!! Form::model(null,array('url' => "admin/businesses/items/$b->id",'method' => 'get', 'class'=>'form-horizontal')) !!}
                {{ Form::token() }}
                <div class="form-group">
                    <input type="text" class="form-control" name="item_search" placeholder="Enter keyword" />
                    <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                </div>
            {!! Form::close() !!} --}}

			<div class="col-sm-8">
				{!! Form::model(null,array('url' => "admin/businesses/items/$b->id",'method' => 'post', 'class'=>'form-horizontal')) !!}

                    {{ Form::token() }}
                    <div class="col-sm-12" style="margin-bottom: 20px; padding-right: 0px;">
                        {{ Form::submit('Save',array('class'=>'btn btn-success pull-right col-sm-4'))}}
                    </div>
                    <table class="table table-stripped table-bordered">
                        <tr>
                            <th>Category</th>
                            <th></th>
                        </tr>

                        @if(isset($items))
                            @foreach($items->sortBy('name') as $i)
                                <?php
                                    $do_check="";
                                    if(in_array($i->id, array_keys($mappings))){
                                        $do_check="checked";
                                    }
                                ?>
                                <tr>
                                    <td>{{{$i->name}}}</td>

                                    <td>
                                        <input type="checkbox" name="value[]" {{$do_check}} value="{{$i->id}}">
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                    </table>

                    <div class="col-sm-12" style="margin-bottom: 20px; padding-right: 0px;">
                        {{ Form::submit('Save',array('class'=>'btn btn-success pull-right col-sm-4'))}}
                    </div>
				{!! Form::close() !!}
			</div>

	</div>
	<div>

	</div>

@stop
