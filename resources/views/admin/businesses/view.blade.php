@extends('admin/_layout')

@section('content')


	<div class="panel panel-inverse col-lg-10">


		<div class="panel-body">
			<div>
				<table class="table table-striped">
					<tr>
						<td>Name</td>
						<td>{{$b->name}}</td>
					</tr>
					<tr>
						<td>Address</td>
						<td>{{$b->address}}</td>
					</tr>
					<tr>
						<td>Website</td>
						<td>{{$b->website}}</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{$b->email}}</td>
					</tr>
					<tr>
						<td>Phone</td>
						<td>{{$b->phone}}</td>
					</tr>
					<tr>
						<td>Products</td>
						<td>{{$b->products}}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							@if($b->activated=='1')
								Activated
                                {!! Form::model(null,array('url' => 'admin/businesses/deactivate/'.$b->id,'method' => 'Post', 'class'=>'form-horizontal')) !!}
								{{ Form::token() }}
                                {{ Form::submit('Deactivate Account',array('class'=>'btn btn-danger pull-right'))}}
                                {!! Form::close() !!}
							@else
								Pending
								{!! Form::model(null,array('url' => 'admin/businesses/view/'.$b->id,'method' => 'Post', 'class'=>'form-horizontal')) !!}
								{{ Form::token() }}

								{{ Form::submit('Activate Account',array('class'=>'btn btn-success pull-right'))}}
								{!! Form::close() !!}
							@endif
						</td>
					</tr>
				</table>

			</div>

			<hr>
			<div>
                <div class="row">
                    <div class="col-xs-6 col-sm-10">
                        <h2 style="padding: 0px; margin-top: 0px;">Produts/Services of {{$b->name}}</h2>
                    </div>
                    <div class="col-xs-6 col-sm-2">
                        <a href="{{url('admin/businesses/items/'.$b->id)}}" class="btn btn-primary col-sm-12">Edit</a>
                    </div>
                </div>

				{{-- <p class="pull-right"><a href="{{url('admin/businesses/items/'.$b->id)}}" class="btn btn-primary">Edit</a></p> --}}
				<hr>

				@if(count($items))
					@foreach($items as $i)
                        <p>
                            <span>{{$i->Item->name}}</span><br>
                        </p>
					@endforeach
				@else
					<p>No item found</p>
				@endif

			</div>
		</div>
	</div>




@stop
