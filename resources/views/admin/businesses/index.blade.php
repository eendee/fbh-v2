@extends('admin/_layout')

@section('content')
	
	<div>
		<div class="pull-right hidden">
			<form id="searchUser" action="{{url('admin/users/search/') }}" method="get">
	                   
	        
	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>SN</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Name</th>
				<th>Products</th>
				<th>Date</th>
				<th>Status</th>
				<th></th>
			</tr>
			@if(isset($businesses))
				@foreach($businesses as $b)
					<tr>
						<td>{{$b->id}}</td>
						<td>{{$b->email}}</td>
						<td>{{$b->phone}}</td>
						<td>{{$b->name}}</td>
						<td>{{$b->products}}</td>
						<td>
							{{ date('j F, Y g:i a', strtotime($b->created_at))}}
						</td>
						<td>
							{{$b->activated=='1'?'Activated':'Pending'}}
						</td>
						<td><a href="{{url('admin/businesses/view/'.$b->id)}}" class="btn btn-primary">View</a></td>

					</tr>
				@endforeach
		@endif
		</table>
		{{ $businesses->links() }}
	</div>

@stop