@extends('admin/_layout')


@section('content')
	<div>
		<h2>Edit settings for {{$setting->name}} </h2>

			{!! Form::model(null,array('url' => "admin/settings/edit/$setting->edit_template/$setting->id",'method' => 'post', 'class'=>'form-horizontal')) !!}
		    	
			<table class="table table-stripped table-bordered">
				<tr>
					<th>Category</th>
					<th></th>
				</tr>
				@if(isset($pages))
					@foreach($pages as $p)
						<tr>
							<td>{{{$p->name}}}</td>
							<?php  
								$do_check="";
								if(in_array($p->id, $checked)){
									$do_check="checked";
								}
							?>
							<td>
								<input type="checkbox" name="value[]" {{$do_check}} value="{{$p->id}}">
							</td>
						</tr>
					@endforeach
				@endif

			</table>
				{{ Form::token() }}
			<div class="col-sm-10">
				{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
			</div>
			{!! Form::close() !!}

	</div>
	<div>
		
	</div>

@stop