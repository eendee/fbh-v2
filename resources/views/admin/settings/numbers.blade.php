@extends('admin/_layout')


@section('content')
	<div>
		<h2>Numbers/Values related settings </h2>

			{!! Form::model(null,array('url' => "admin/settings/edit/nmbr/0",'method' => 'post', 'class'=>'form-horizontal')) !!}
		    	
			<table class="table table-stripped table-bordered">
				<tr>
					<th>Setting</th>
					<th>Value</th>
				</tr>

				
				@if(isset($settings))
					@foreach($settings as $s)
						<tr>
							<td>{{{$s->name}}}</td>
							<td>
								{{Form::text("vals_".$s->id,
								$s->value,array('class'=>'form-control','required'=>'required'))}}
							</td>
						</tr>
					@endforeach
				@endif

			</table>
				{{ Form::token() }}
			<div class="col-sm-10">
				{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
			</div>
			{!! Form::close() !!}

	</div>
	<div>
		
	</div>

@stop