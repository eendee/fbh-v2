@extends('admin/_layout')


@section('content')
	<div>
		<a href="{{url('admin/setting/create')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		
	</div>
	<div>
	<h2>All setting</h2>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>Template</th>
				<th></th>
			</tr>
			@if(isset($settings))
				@foreach($settings as $s)
					<tr>
						<td>{{$s->name}}</td>
						<td>{{$s->edit_template}}</td>
						<td><a  class="btn btn-default" href="{{url('admin/settings/edit/'.$s->edit_template.'/'.$s->id)}}">Edit</a></td>	
								
					</div>
					</tr>
				@endforeach
		@endif
		</table>
		
	</div>

@stop