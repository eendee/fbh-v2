@extends('admin/_layout')
<?php 
	$url = 'admin/categories';
	$method='post';
	if(isset($cat)&&$cat->id>=1){
		$url="admin/categories/$cat->id";
		$method="put";
	}
?>

@section('content')
	<div class="panel panel-inverse col-lg-6">


		<div class="panel-body">
			{!! Form::model($cat,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Name')}}
					{{Form::text('name',$cat->name,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Slug')}}
					{{Form::text('slug',$cat->slug,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Parent')}}
					{{Form::select('parent_id', $categories+array('0'=>'None'),$cat->parent_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Image')}}
					{{Form::select('image_id',$images,$cat->image_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Icon')}}
					{{Form::select('icon_id',$images,$cat->icon_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('description')}}
					{{Form::textarea('description',$cat->description,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
@stop