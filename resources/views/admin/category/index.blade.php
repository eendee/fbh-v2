@extends('admin/_layout')



@section('content')
	<div>
		<a href="{{url('admin/categories/create')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		
	</div>
	<div>
	<h2>All Categories</h2>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Parent</th>
				<th>Item Count</th>
				<th></th>
				<th></th>
			</tr>
			@if(isset($cats))
				@foreach($cats as $cat)
					<tr>
						<td>{{$cat->id}}</td>
						<td>{{$cat->name}}</td>
						<td>{{$cat->Parent!=null?$cat->Parent->name:"None"}}</td>
						<td>{{$cat->Items->count()}}</td>
						<td><a href="{{url('admin/categories/'.$cat->id.'/edit')}}">Edit</a></td>
						<td><a href="{{url('admin/categories/delete/'.$cat->id)}}">Delete</a></td>	
								
					</div>
					</tr>
				@endforeach
		@endif
		</table>
		
	</div>

@stop