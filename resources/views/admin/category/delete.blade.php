@extends('admin/_layout')

@section('content')
	{!! Form::model($category,array('url' => "admin/categories/$category->id",'method' => 'delete')) !!}
    	<div class="form-group">
			{{Form::label('Name')}}
			{{"Delete $category->name ?"}}
		</div>
		<div class="form-group">
			{{Form::hidden('id')}}
		</div>
		<div>
			{{ Form::submit('Delete')}}
		</div>
		{{ Form::token() }}
	{!! Form::close() !!}
@stop