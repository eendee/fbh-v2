@extends('admin/_layout')


@section('content')
	<h1 class="page-header">Admin <small>Dashboard</small></h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-green">
						<div class="stats-icon"><i class="fa fa-users"></i></div>
						<div class="stats-info">
							<h4>TOTAL USERS</h4>
							<p>{{$stats->users}}</p>	
						</div>
						<div class="stats-link">
							<a href="{{url('admin/users')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-edit"></i></div>
						<div class="stats-info">
							<h4>TOTAL REVIEWS</h4>
							<p>{{$stats->reviews}}</p>		
						</div>
						<div class="stats-link">
							<a href="{{url('admin/reviews')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-red">
						<div class="stats-icon"><i class="fa fa-tags"></i></div>
						<div class="stats-info">
							<h4>TOTAL CATEGORIES</h4>
							<p>{{$stats->category}}</p>	
						</div>
						<div class="stats-link">
							<a href="{{url('admin/categories')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-purple">
						<div class="stats-icon"><i class="fa fa-list"></i></div>
						<div class="stats-info">
							<h4>TOTAL ITEMS</h4>
							<p>{{$stats->items}}</p>	
						</div>
						<div class="stats-link">
							<a href="{{url('admin/items')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				
			</div>
			<!-- end row -->
			

@stop