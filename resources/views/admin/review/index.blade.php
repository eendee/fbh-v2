@extends('admin/_layout')
<?php
	$boolean_values=array('False','True');
	$activation_values=array('Activate', 'Deactivate');
	$feature_values=array('Feature', 'Dont Feature');
?>

@section('content')

	<div>
		<div class="pull-right">
			<form id="searchReview" action="{{url('admin/reviews/search/') }}" method="get">


	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>SN</th>
				<th>User</th>
				<th>Review Type</th>
				<th>Item</th>
				<th>Review </th>
				<th>Score</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
			@if(isset($reviews))
				@foreach($reviews as $r)
					<tr>
						<td>{{$r->id}}</td>
						<td>
							@if(isset($r->User))
								{{$r->User->email}}
								age: {{$r->Profile->age}}
								sex: {{$r->Profile->sex}}

							@endif

							@if(isset($r->Profile->State))
								{{$r->Profile->State->name}}

							@endif
						</td>
						<td>
							@if(isset($r->ReviewType))
								{{$r->ReviewType->name}}
							@endif
						</td>
						<td>{{$r->Item->name}}</td>
						<td>
							{{$r->title}}
							<hr>
							{{$r->body}}
						</td>
						<td>
							<?php
								$sum=0;

							if(isset($r->ReviewDetails)){
								$rds=$r->ReviewDetails;
								if(isset($rds)&& count($rds)){
									foreach ($rds as $rd) {
										$sum+=$rd->rating;
									}
									echo $sum/$rds->count();
								}
								else{
									echo "N/A";
								}
							}

							?>
						</td>
						<td>
							Activated: {{$boolean_values[$r->activated]}}<br>
							Featured: {{$boolean_values[$r->featured]}}
						</td>
						<td>
							<a href="{{url('admin/review/edit/'.$r->id)}}">Edit</a>|
							<a href="{{ url('admin/review/activate/'.$r->id) }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('activate-form-{{$r->id}}').submit();">
                                    {{$activation_values[$r->activated]}}
                            </a>|
                            <form id="activate-form-{{$r->id}}" action="{{url('admin/review/activate/'.$r->id) }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                            </form>

							<a href="{{ url('admin/review/feature/'.$r->id) }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('feature-form-{{$r->id}}').submit();">
                                    {{$feature_values[$r->featured]}}
                            </a>|
                            <form id="feature-form-{{$r->id}}" action="{{url('admin/review/feature/'.$r->id) }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                            </form>

                            <a href="{{ url('admin/review/delete/'.$r->id) }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('delete-form-{{$r->id}}').submit();">
                                    Delete
                            </a>
                            <form id="delete-form-{{$r->id}}" action="{{url('admin/review/delete/'.$r->id) }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                            </form>


						</td>

					</div>
					</tr>
				@endforeach
			@endif
		</table>
		@if(isset($term))
			{{ $reviews->appends(['s'=>$term])->links() }}
		@else
			{{ $reviews->links() }}
		@endif

	</div>

@stop
