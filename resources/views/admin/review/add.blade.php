@extends('admin/_layout')

@section('content')
	<div class="panel panel-inverse col-lg-6">


		<div class="panel-body">
			{!! Form::model($review,array('url' => "admin/item/review/update/$item->id",'method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
		    	<div class="form-group">
					{{Form::label('Title')}}
					{{Form::text('title',$review->title,array('class'=>'form-control'))}}

				</div>
				<div class="form-group">
					{{Form::label('Description')}}
					{{Form::textarea('body',$review->body, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div class="form-group">
					{{Form::label('Picture(Optional)')}}
					{{Form::file('file')}}
				</div>
				<div class="form-group">
					@if(isset($ti))
						<ul>
							@foreach($ti as $t)
								
								<li>{{{$t->attribute}}}</li>
								<div class="stars">
									<input class="star star-5" value="5" id="star-{{$t->id}}-5" type="radio" name="rating[{{$t->id}}]"/>
								    <label class="star star-5" for="star-{{$t->id}}-5"></label>
								    <input class="star star-4" value="4" id="star-{{$t->id}}-4" type="radio" name="rating[{{$t->id}}]"/>
								    <label class="star star-4" for="star-{{$t->id}}-4"></label>
								    <input class="star star-3" value="3" id="star-{{$t->id}}-3" type="radio" name="rating[{{$t->id}}]"/>
								    <label class="star star-3" for="star-{{$t->id}}-3"></label>
								    <input class="star star-2" value="2" id="star-{{$t->id}}-2" type="radio" name="rating[{{$t->id}}]"/>
								    <label class="star star-2" for="star-{{$t->id}}-2"></label>
								    <input class="star star-1" value="1" id="star-{{$t->id}}-1" type="radio" name="rating[{{$t->id}}]"/>
								    <label class="star star-1" for="star-{{$t->id}}-1"></label>
								</div>
								
							@endforeach
							<li><b>Overall Score:</b> <span class="text-muted"><small></small> </span></li>
							<div class="stars">
								<input class="star star-5" value="5" id="star-z-5" type="radio" name="overall_score"/>
								<label class="star star-5" for="star-z-5"></label>
								<input class="star star-4" value="4" id="star-z-4" type="radio" name="overall_score"/>
								<label class="star star-4" for="star-z-4"></label>
								<input class="star star-3" value="3" id="star-z-3" type="radio" name="overall_score"/>
								<label class="star star-3" for="star-z-3"></label>
								<input class="star star-2" value="2" id="star-z-2" type="radio" name="overall_score"/>
								<label class="star star-2" for="star-z-2"></label>
								<input class="star star-1" value="1" id="star-z-1" type="radio" name="overall_score]"/>
								<label class="star star-1" for="star-z-1"></label>
								<br>
								<hr>
							</div>
						<ul>
					@endif
					{{ Form::token() }}
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
			
			{!! Form::close() !!}
		</div>
	</div>
@stop
