@extends('admin/_layout')
<?php 
	$url = 'admin/templates';
	$method='post';
	if(isset($template)&&$template->id>=1){
		$url="admin/templates/$template->id";
		$method="put";
	}
?>

@section('content')
	<div class="panel panel-inverse col-lg-6">


		<div class="panel-body">
			{!! Form::model($template,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Name')}}
					{{Form::text('name',$template->name,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Description')}}
					{{Form::textarea('description',$template->description,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
@stop
