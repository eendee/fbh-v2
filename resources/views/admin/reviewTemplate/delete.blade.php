@extends('admin/_layout')

@section('content')
	{!! Form::model($template,array('url' => "admin/templates/$template->id",'method' => 'delete')) !!}
    	<div class="form-group">
			{{Form::label('Name')}}
			{{"Delete $template->name ?"}}
		</div>
		<div class="form-group">
			{{Form::hidden('id')}}
		</div>
		<div>
			{{ Form::submit('Delete')}}
		</div>
	{!! Form::close() !!}
@stop