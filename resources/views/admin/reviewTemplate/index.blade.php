@extends('admin/_layout')


@section('content')
	<div>
		<a href="{{url('admin/templates/create')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		
	</div>
	<div>
	<h2>All Review Templates</h2>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>No. of Features</th>
				<th>Edit</th>
				<th>Delete</th>
				<th>View Features</th>
			</tr>
			@if(isset($templates))
				@foreach($templates as $template)
					<tr>
						<td>{{$template->name}}</td>
						<td>{{$template->ReviewTemplateItems->count()}}</td>
						<td><a href="{{url('admin/templates/'.$template->id.'/edit')}}">Edit</a>	</td>
						<td><a href="{{url('admin/templates/delete/'.$template->id)}}">-</a>	</td>
						<td><a href="{{url('admin/templates/items/'.$template->id)}}">features</a>	</td>
							
					</tr>
				@endforeach
		@endif
		</table>
		
	</div>

@stop

