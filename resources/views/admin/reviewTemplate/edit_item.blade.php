@extends('admin/_layout')
<?php 
	$url = "admin/templates/$item->review_template_id/items/add";
	$method='post';
	if(isset($item)&&$item->id>=1){
		$url="admin/templates/item/edit/$item->id";
		$method="put";
	}
?>



@section('content')
	<div class="panel panel-inverse col-lg-6">


		<div class="panel-body">
			{!! Form::model($item,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Attribute')}}
					{{Form::text('attribute',$item->attribute,array('class'=>'form-control','required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('Activated')}}
					{{Form::hidden('activated','0')}}
					{{Form::hidden('review_template_id',$template->id)}}
					{{Form::checkbox('activated')}}
				</div>
				<div class="form-group">
					{{Form::label('Description')}}
					{{Form::textarea('description',$item->description,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
@stop