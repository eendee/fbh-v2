@extends('admin/_layout')

@section('content')
	<div>
		<a href="{{url('admin/templates/'.$template->id.'/items/add')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		
	</div>
	<div>
	<h2>All Review Templates Details of {{$template->name}}</h2>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Edit</th>
			</tr>
			@if(isset($items))
				@foreach($items as $item)
					<tr>
						<td>{{$item->attribute}}</td>
						<td>{!!$item->description!!}</td>
						<td><a href="{{url('admin/templates/item/edit/'.$item->id)}}">Edit</a>	</td>
							
					</tr>
				@endforeach
			@else
				<p>Not items added yet.</p> click <a href="{{url('admin/templates/'.$template->id.'/items/add')}}">here</a> to add.
			@endif
		</table>
	</div>

@stop

