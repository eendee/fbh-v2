@extends('admin/_layout')

@section('content')
	{!! Form::model($page,array('url' => "admin/pages/$page->id/delete",'method' => 'delete')) !!}
    	<div class="form-group">
			{{Form::label('Name')}}
			{{"Delete $page->name ?"}}
		</div>
		<div class="form-group">
			{{Form::hidden('id')}}
		</div>
		<div>
			{{ Form::submit('Delete')}}
		</div>
	{!! Form::close() !!}
@stop