@extends('admin/_layout')


@section('content')
	<div>
		<a href="{{url('admin/pages/create')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		
	</div>
	<div>
	<h2>All Pages</h2>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>Slug</th>
				<th>Group</th>
				<th></th>
				<th></th>
			</tr>
			@if(isset($pages))
				@foreach($pages as $page)
					<tr>
						<td>{{$page->name}}</td>
						<td>{{$page->slug}}</td>
						<td><?php try { ?>
							{{$page->Section->name}}
						<?php } catch (Exception $e) {
							
						} ?></td>
						<td><a href="{{url('admin/pages/'.$page->id.'/edit')}}">Edit</a></td>
						<td><a href="{{url('admin/pages/delete/'.$page->id)}}">Delete</a></td>	
								
					</div>
					</tr>
				@endforeach
		@endif
		</table>
		
	</div>

@stop