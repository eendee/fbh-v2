@extends('admin/_layout')
<?php
	$url = 'admin/pages/create';
	$method='POST';
	if(isset($page)&&$page->id>=1){
		$url="admin/pages/$page->id/update";
		$method="POST";
	}
?>

@section('content')
	<div class="panel panel-inverse col-lg-10">
        @if ($errors->any())
            <div class="alert alert-danger" style="margin-top: 10px;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

		<div class="panel-body">
			{!! Form::model($page,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Name')}}
					{{Form::text('name',$page->name,array('class'=>'form-control', 'required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('Slug')}}
					{{Form::text('slug',$page->slug,array('class'=>'form-control','placeholder'=>'user friendly slug', 'required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('excerpt')}}
					{{Form::text('excerpt',$page->excerpt,array('class'=>'form-control', 'placeholder'=>'excerpt of page, optional'))}}
				</div>
				<div class="form-group">
					{{Form::label('Template')}}
					{{Form::select('template_id', $pageTemplates,$page->template_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Page Group')}}
					{{Form::select('page_section_id', $pageSections,$page->page_section_id,array('class'=>'form-control', 'required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('Feature Image')}}
					{{Form::select('featured_image_id', $images,$page->feature_image_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('body')}}
					{{Form::textarea('body',$page->body,array('class'=>'form-controll', 'class'=>'mceEditor'))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
@stop
