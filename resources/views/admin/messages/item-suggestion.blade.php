@extends('admin/_layout')

@section('content')
	
	<div>
		<div class="pull-right hidden">
			<form id="searchUser" action="{{url('admin/users/search/') }}" method="get">
	                   
	        
	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>SN</th>
				<th>Email</th>
				<th>Name</th>
				<th>Product</th>
				<th>Producer</th>
				<th>Location</th>
				<th>Reason</th>
				<th>Date</th>
			</tr>
			@if(isset($messages))
				@foreach($messages as $m)
					<tr>
						<td>{{$m->id}}</td>
						<td>{{$m->email}}</td>
						<td>{{$m->name}}</td>
						<td>{{$m->product}}</td>
						<td>{{$m->producer}}</td>
						<td>
							{{$m->locations}}
						</td>
						<td>
							{{$m->reason}}
						</td>
						<td>
							{{ date('j F, Y g:i a', strtotime($m->created_at))}}
						</td>
					</tr>
				@endforeach
		@endif
		</table>
		{{ $messages->links() }}
	</div>

@stop