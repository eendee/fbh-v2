@extends('admin/_layout')

@section('content')
	
	<div>
		<div class="pull-right">
			<form id="searchUser" action="{{url('admin/users/search/') }}" method="get">
	                   
	        
	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>SN</th>
				<th>User</th>
				<th>Name</th>
				<th>Actions</th>
			</tr>
			@if(isset($users))
				@foreach($users as $u)
					<tr>
						<td>{{$u->id}}</td>
						<td>{{$u->email}}</td>
						<td>
							{{$u->Profile->Fullname()}}
							@include('partials.badge', array('rank'=>$u->Profile->Rank)) 
						</td>
						<td>
							<a href="{{url('admin/users/profile/'.$u->id)}}">View Profile</a>|
							<a href="{{url('admin/reviews/user/'.$u->id)}}">View Reviews</a>
						</td>
					</tr>
				@endforeach
		@endif
		</table>
		{{ $users->links() }}
	</div>

@stop