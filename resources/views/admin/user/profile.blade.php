<?php  
	$picture="assets/img/user_icon.png";
	if(isset($profile->picture_id)){
		$picture='uploads/'.$profile->file->url;
	}

?>
@extends('admin/_layout')

@section('content')
	
<div class="container">
	<div class="row">
			<h1 class="">Profile of  {{$user->profile->firstname}}</h1>
			<hr class="colorgraph">
			@if(Session::has('message'))
				<div class="alert alert-success">
					{{Session::get('message')}}
				</div>
			@endif
		<div id="fbh-main" class="col-md-3">
			<div class="row">
				<div style="padding:5px; border:solid 1px #f5f5f5;">
					<div>
						<img class="img-responsive"  src="{{asset($picture)}}">
					</div>
					<table class="table table-striped">
						<tr>
							<td>Products Reviewed <span class="badge">{{$stats->reviews}}</span></td>
						</tr>
						<tr>
							<td>No. of Comments <span class="badge">{{$stats->comments}}</span></td>
						</tr>
						<tr>
							<td>Items Voted  <span class="badge">{{$stats->votes}}</span></td>
						</tr>
						<tr>
							<td>Community Score <span class="badge">{{$stats->TotalVotes}}</span></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="col-sm-8">
				
				<h3>
					@if(isset($profile->firstname)&&isset($profile->surname))
						<span>{{$profile->firstname.' '.$profile->surname}}</span>
						@include('partials.badge', array('rank'=>$profile->Rank) )
					@endif
				</h3>
				<p>
					@if(isset($profile->about))
						<span>
							{{$profile->about}}
						</span>
					@endif
				</p>
			</div
		</div>
	</div>
</div>

@stop