@extends('admin/_layout')

@section('content')
	<div>
		<a href="{{url('admin/items/create')}}" class='btn btn-primary pull-right'>
			Add New
		</a>
		
	</div>
	<div>
		<div class="pull-right">
			<form id="searchReview" action="{{url('admin/items/search/') }}" method="get">
	                   
	        
	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>
		
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>Category</th>
				<th>Review Template</th>
				<th>Featured</th>
				<th>C-Score</th>
				<th>Activated</th>
				<th>View</th>
				<th>Edit</th>
			</tr>
			@if(isset($items))
				@foreach($items as $item)
					<tr>
						<td>{{{$item->name}}}</td>
						<td>
							@if(isset($item->Category->name))
								{{{$item->Category->name}}}
							@endif
						</td>
						<td>
							@if(isset($item->ReviewTemplate->name))
								{{{$item->ReviewTemplate->name}}}
							@endif
							
						</td>
						<td>
							@if($item->featured ==1)
								<span>True</span>
							@else
								<span>False</span>
							@endif
						</td>
						<td>
							<span class="badge">{{$item->AdminScore()}}</span>
							<a href="{{url('admin/item/review/score/'.$item->id)}}">update</a>
							
						</td>
						<td>
							@if($item->activated ==1)
								<span>Tue</span>
							@else
								<span>False</span>
							@endif
						</td>
						<td><a href="{{url('items/'.$item->id)}}">View</a></td>
						<td><a href="{{url('admin/items/'.$item->id.'/edit')}}">edit</a></td>
					</tr>
				@endforeach
			@endif

		</table>
		{{ $items->links() }}
	</div>

@stop