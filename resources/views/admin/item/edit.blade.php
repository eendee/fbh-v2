@extends('admin/_layout')
<?php 
	$url = 'admin/items';
	$method='post';
	if(isset($item)&&$item->id>=1){
		$url="admin/items/$item->id";
		$method="put";
	}
?>



@section('content')
	<div class="panel panel-inverse col-lg-10">


		<div class="panel-body">
			{!! Form::model($item,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Name')}}
					{{Form::text('name',$item->name,array('class'=>'form-control','required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('Image')}}
					{{Form::select('image_id',$images,$item->image_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Category')}}
					{{Form::select('category_id', $categories,$item->category_id,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Review Template')}}
					{{Form::select('review_template_id', $reviewTemplates,$item->review_template_id,array('class'=>'form-control'))}}
				</div>

				<div class="form-group">
					{{Form::label('Description')}}
					{{Form::textarea('description',$item->description, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
					{{Form::label('Activated')}}
					{{Form::hidden('activated','0')}}
					{{Form::checkbox('activated')}}

					{{Form::label('Featured')}}
					{{Form::hidden('featured','0')}}
					{{Form::checkbox('featured')}}
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
@stop