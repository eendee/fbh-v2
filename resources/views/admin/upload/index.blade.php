@extends('admin/_layout')
<?php 
	$url = 'admin/files/upload';
	$method='post';
	
?>

@section('content')
	<div>
		<div class="pull-right">
			<form id="searchReview" action="{{url('admin/files/search/') }}" method="get">
	                   
	        
	          <div class="col-lg-12">
			    <div class="input-group">
			      <input type="text" name="s" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <input type="submit"  class='btn btn-primary' value="Search">
			      </span>
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</form>
		</div>
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Name</th>
				<th>File Type</th>
				<th>Uploader</th>
				<th>View</th>
			</tr>
			@if(isset($files))
				@foreach($files as $file)
					<tr>
						<td>{{$file->name}}</td>
						<td><span>{{$file->extension}}</span>	</td>
						<td>{{isset($file->User->email)?$file->User->email:'N/A'}}</td>
						<td><a href="{{url('').'/uploads/'.$file->url}}">View	</td>
							
					</tr>
				@endforeach
			@endif
		</table>

		{{ $files->links() }}
	</div>
	<h2>Add New</h2>
	{!! Form::model($file,array('url' => $url,'method' => $method,'enctype'=>'multipart/form-data'))!!}

		<div class="form-group">
			{{Form::label('File')}}
			{{Form::file('file')}}
		</div>
		<div>
			{{ Form::token() }}
			{{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
		</div>
		
	{!! Form::close() !!}
@stop