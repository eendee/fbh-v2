@extends('front.layouts.manage')
@section('title', 'Register')
<?php  

    $sex = array(''=>'select','M' => 'Male','F'=>'Female' );
    $ages=array();
    $ages["0"]="Select";
    for ($i=17; $i <=120 ; $i++) { 
        $ages["$i"]=$i;
    }
?>
@section('content')
<div class="container">
    <div class="row">
        <div style="max-width:700px; min-width:400px; margin:auto; margin-top:50px; padding:20px; border:3px solid rgba(132, 130, 130, 0.1); background:#fcfcfc;">
            @include('front.partials.logo')

           
            
            <h3 class="form-signin-heading">Create A New Account</h3>
            <hr class="colorgraph"><br>
            <div class="form-group pull-right">
                <label for="firstname" class="control-label">Have a facebook account?</label>
                <div class="input-group">
                    <a href="{{url('/redirect')}}" class="btn btn-primary btn-block">
                    <i class="fa fa-facebook-square fa-lg"></i>
                    Register with Facebook
                </a>
                </div>
            </div>
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <table class="table">
                    
                    <tr>
                        
                        <td>Firstname</td>
                        <td>
                            <div class="input-group">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>
                            </div>
                            @if ($errors->has('firstname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </td>
                        <td>Surname</td>
                        <td>
                            <div class="input-group">
                                <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" required>
                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Sex</td>
                        <td>
                            <div class="input-group">
                                {{Form::select('sex', $sex,'',array('class'=>'form-control', 'requried'=>'required'))}}
                                @if ($errors->has('sex'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td>Age</td>
                        <td>
                           <div class="input-group">
                                {{Form::select('age', $ages,'',array('class'=>'form-control', 'requried'=>'required'))}}
                                       
                                @if ($errors->has('age'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('age') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>
                            <div class="input-group">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td>
                            State of residence
                            <div class="text-muted" style="font-size:.8em;">
                                this will help us make sense of your experiences
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                {{Form::select('state_id',array(''=>'select')+$states,'',array('class'=>'form-control', 'requried'=>'required'))}}
                                @if ($errors->has('state_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>
                            <div class="input-group">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td>Confirm Password</td>
                        <td>
                            <div class="input-group">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <button type="submit" class="btn btn-danger  btn-dark">
                                Register
                            </button>
                        </td>
                    </tr>
                </table>        
            </form>
        </div>
    </div>
</div>
 <script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker({
            inline: true,
            sideBySide: true
        });
    });
</script>
@endsection
