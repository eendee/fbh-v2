@extends('front.layouts.manage')
@section('title', 'LogIn')
@section('content')


<div class = "container">
    <div class="wrapper">
        <form action="" method="post" action="{{ route('login') }}" name="Login_Form" class="form-signin">
            @include('front.partials.logo')
            {{ csrf_field() }}
            <h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
            <div class="">
                <div class="">
                    <a href="{{url('/redirect')}}" class="btn btn-primary btn-block">
                        <i class="fa fa-facebook-square fa-lg"></i>
                        Login with Facebook
                    </a>
                </div>
            </div>
            <p>
                @if(Session::has('message'))
                    <div class="alert alert-danger">
                        {{Session::get('message')}}
                    </div>
                @endif

            </p>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
              <hr class="colorgraph"><br>

                <input placeholder="Email" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>


              <button class="btn btn-lg btn-danger btn-block"  name="Submit" value="Login" type="Submit">Login</button>
            <a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>

        </form>
    </div>
</div>
@endsection
