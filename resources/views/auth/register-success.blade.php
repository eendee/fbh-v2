@extends('front.layouts.manage')
@section('title', 'One more step')
@section('content')


<div class = "container">
    <div class="wrapper">
        <form action="" method="post" action="{{ route('login') }}" name="Login_Form" class="form-signin">   
            @include('front.partials.logo')
            {{ csrf_field() }}    
            <h3 class="form-signin-heading">Congratulations! You have successfully registered</h3>

            <p class="">

                <div class="alert alert-success text-center">
                     <span class="glyphicon glyphicon-check" style="font-size: 5em"> </span>
                     <br>
                   We have sent you a verification email, check your mail and click on the "Verify" Button!
                </div>

            </p>

              <hr class="colorgraph"><br>
                
               
             
                 
            <a class="btn btn-lg btn-danger btn-block" href="{{ route('login') }}">
                Continue to login
            </a>
                        
        </form>         
    </div>
</div>
@endsection
