@component('mail::message')

Dear {{$name}}

<p>Your Business account has been deactivated by admin. If you do not want this, please contact admin for more assistance</p>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
