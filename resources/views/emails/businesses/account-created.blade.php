@component('mail::message')
# Congratulations

We are happy to inform you that after succeessful review of the information you provided, your Feedbackhall Business Owner Account has been created. The details are as follows:<br><br>
Username: {{$business->email}}<br>
Password: {{$pass}}
<p>You will be required to change your password immediately you log in.</p>

@component('mail::button', ['url' => url('login')])
Click here to login.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
