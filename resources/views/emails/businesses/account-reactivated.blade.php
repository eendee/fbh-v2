@component('mail::message')
Dear {{$name}}

<p>We are happy to inform you that your business account has been reactivated.</p>

@component('mail::button', ['url' => url('login')])
Click here to login.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
