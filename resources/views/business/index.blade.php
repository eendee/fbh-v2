@extends('front.layouts.main')
<?php  
	$picture="assets/img/user_icon.png";
	try {
		if(isset($business->image_id) && $business->image_id!="0"){
			$picture='uploads/'.$business->image->url;
		}
	} catch (Exception $e) {
		
	}

?>
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
			<h1 class="">Business Dashboard {{$business->name}}</h1>
			<hr class="colorgraph">
			<h4 class="text-muted">Your Business Profile</h4>
			@if(Session::has('message'))
				<div class="alert alert-success">
					{{Session::get('message')}}
				</div>
			@endif
		<div id="fbh-main" class="col-md-3">
			<div class="row">

			
				<div style="padding:5px; border:solid 1px #f5f5f5;">
					<div>
						<img class="img-responsive"  src="{{asset($picture)}}">
					</div>
	
				</div>
				<table class="table table-striped">
			
					<tr>
						<td>Address</td>
						<td>{{$business->address}}</td>
					</tr>
					<tr>
						<td>Website</td>
						<td>{{$business->website}}</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{$business->email}}</td>
					</tr>
					<tr>
						<td>Phone</td>
						<td>{{$business->phone}}</td>
					</tr>
					<tr>
						<td>Products</td>
						<td>{{$business->products}}</td>
					</tr>
				</table>

				<ul class="nav">
						<li>
							<a href="{{url('business/profile')}}">Update Business Information</a>
						</li>
						<li>
							<a href="{{url('user/password')}}">Change Password</a>
						</li>
						<li>
							<a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                    </form>
						</li>
					</ul>
			</div>
		</div>
		<div class="col-md-8">
			<div class="col-sm-12 card">
				
				<h3>

					@if(isset($business))
						<span>{{$business->name}}</span>
					@endif

				</h3>
				<p>
					@if(isset($business->description))
						<span>
							{{$business->description}}
						</span>
					@else
						<span>
							About you goes here
						</span>
					@endif
				</p>
				<h4>Verified Products/Services</h4>
				<table class="table table-striped">
					<tr>
						<th>SN</th>
						<th>Item</th>
						<th>Category</th>
					</tr>
					@if(isset($items))
					<?php $i=1;?>
						@foreach($items as $item)
							<tr>
								<td>{{{$i}}}</td>
								<td><a href="{{url('items/'.$item->item_id)}}">{{$item->item->name}}</a></td>
								<td>{{$item->item->category->name}}</td>
							</tr>
						<?php $i++;?>
						@endforeach
					@endif

				</table>
			</div>


		</div>
	</div>
</div>
@endsection
