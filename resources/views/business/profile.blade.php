@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<h1 class="">Edit My Profile: <span class="text-muted">{{$user->email}}</span> </h1>
		<hr class="colorgraph">
		<div class="col-md-6">
				@if(Session::has('message'))
					<div class="alert alert-danger">
						{{Session::get('message')}}
					</div>
				@endif
			{!! Form::model($user,array('url' => 'business/profile','method' => 'post','enctype'=>'multipart/form-data', 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Company Registration Number')}}
					{{Form::text('rc_no',$business->rc_no,array('class'=>'form-control','required'=>'required'))}}
				</div>
				<div class="form-group">
					{{Form::label('Business Logo/Image')}}
					{{Form::file('file')}}
				</div>
				<div class="form-group">
					{{Form::label('General Description of business')}}
					{{Form::textarea('description',$business->description,array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-danger btn-dark pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
</div>

 <script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker({
            inline: true,
            sideBySide: true
        });
    });
</script>
@endsection
