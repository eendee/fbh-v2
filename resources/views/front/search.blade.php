@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-12">
			<h2 class="page-header">Search results for: <span class="text-muted">{{$term}}</span></h2>
			<ol class="breadcrumb">
			  <li><a href="{{url('/')}}">Home</a></li>
			  <li class="active">Search Results</li>
			</ol>
			<div class="row">
				<app-search term="{{$term}}"></app-search>
			</div>
			<p class="col-sm-12">
				@include('front.partials.add-product-prompt')
			</p>
		</div>

	</div>
</div>

@endsection


