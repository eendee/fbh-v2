

	<form class="navbar-form hidden-xs" role="search" action="{{url('search')}}">
		<span class="screen-reader-text"></span>
		<input type="search" id="s" name="s" class="form-control" placeholder="find products and services" title="Search for:">
		<button class="btn-search-form" type="submit" style="" title="Search">
			<i class="fa fa-search"></i>
		</button>
	</form>