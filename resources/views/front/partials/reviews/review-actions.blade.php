<div class="action-div" style="margin-top: 10px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="action-div__container">
                <div class="action-div__container__comment">
                    <span class="fa fa-comment-o fa-lg"></span>
                    <span style="margin-left: 10px;">
                        {{$r->Comments->count()}}
                    </span>
                </div>
                <div class="action-div__container__view">
                    <a href="{{url('reviews/show/'.$r->id)}}" class="action-div__view">View</a>
                </div>
                <div class="action-div__container__like">
                    @include('front.partials.reviews.review-like-button')
                </div>
                <div class="action-div__container__flag">
                    <a href="{{url('review/flag/'.$r->id)}}"><span class="fa fa-flag fa-lg"></span> </a>
                </div>
            </div>
            {{-- <span class="fa fa-comment-o fa-lg"></span> {{$r->Comments->count()}} &nbsp;
            <a href="{{url('reviews/show/'.$r->id)}}" style="margin-left: 20px;">View</a> --}}
            {{-- @include('front.partials.reviews.review-like-button') --}}
        </div>
        {{-- <div class="col-xs-2 col-sm-4">
            <a class="action-flag" href="{{url('review/flag/'.$r->id)}}"><i class="fa fa-flag fa-lg"></i> </a>
        </div> --}}
    </div>
</div>
