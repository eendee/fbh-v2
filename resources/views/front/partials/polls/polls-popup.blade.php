@if(isset($poll))
<div id="PollModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="row">
				<div id="fbh-main" class="col-md-12">
					<div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">{{$poll->name}}</h4>
		            </div>
		            <div class="modal-body">
		               <section>
							{!!
								$poll->description
							!!}
							<br>
						</section>

						<div>
							@if(isset($questions))
								{!! Form::model($poll,array('url' => 'poll/submit/'.$poll->id,'method' => 'post', 'class'=>'form-horizontal')) !!}
									@foreach($questions as $q)
										<div class="poll-question-div">
											<div>
												<p>
													{!!$q->body!!}
												</p>
												@if(isset($q->Options))
													<div>
														@foreach($q->Options as $o)
														<div>
															<div class="iradio">
																<input type="radio" name="questions[{{$q->id}}]" value="{{$o->id}}">
																<span class="poll-question-option">
																	{{$o->body}}
																</span>
															</div>
															
														</div>
														@endforeach
													</div>
													
												@endif
											</div>
											
										</div>
									@endforeach
									{{ Form::token() }}
									<div class="modal-footer">
										 <button style="margin-left: 5px;" type="button" class="btn btn-dark" data-dismiss="modal">Close</button> 
										@if(Auth::check())
										   {{ Form::submit('Submit',array('class'=>'btn btn-danger pull-right'))}}
										
										@else
											<a href="{{url('poll/'.$poll->id)}}" class="btn btn-danger"> Login to participate</a>
										@endif
									</div>
								{!! Form::close() !!}
							@endif
						</div>
		            </div>
					
				</div>
			</div>
            
        </div>
    </div>
</div>


@endif