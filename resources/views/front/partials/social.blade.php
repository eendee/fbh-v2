<?php 
	//$xyz=urlencode(url()->current());
	$xyz = urlencode(
			'http' .
			($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://' .
			$_SERVER['HTTP_HOST'] .
			$_SERVER['PHP_SELF']
		);
	$s_title=urlencode($s_title);
 ?>
<p class="fbh-social">
	<a href="https://www.facebook.com/sharer/sharer.php?u={{$xyz}}&display=popup" class="share facebook">
		
	</a>

	<a  style="color:#00abf0;" href="https://twitter.com/intent/tweet?url={{$xyz}}&text={{urlencode($s_title)}}" class="share twitter">
	
	</a>	

	<a style="color:red" href="https://plus.google.com/share?url={{$xyz}}" class="share google">
		
	</a>

	<a href="http://www.linkedin.com/shareArticle?mini=true&url={{$xyz}}&source={{urlencode($s_title)}}" class="share linkedin">
	
	</a>
</p>

