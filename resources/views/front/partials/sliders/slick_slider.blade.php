<section class="sufia-slider" >
      <div class="slider-main" >
        @if(isset($sliders))
          @foreach($sliders as $s)
            <!-- Slider Item -->
            <div class="single-slider" style="background-image:url({{asset($s->Image->ReturnUrl())}});">
              <div class="container header-container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="welcome-text font-weight-thin">
                      <h1>{{ strip_tags($s->name) }}</h1>
                      <h5 style="color:white">{{ strip_tags($s->body) }}</h5>
                      @if($s->template_id=="2")
                        <div class="button">
                          <a href="{{url('register')}}" class="btn primary btn-lg">Register</a>
                        </div>
                      @endif
                      @if($s->template_id=="3")
                        <div class="button">
                          <a href="http://{{$s->excerpt}}" class="btn primary btn-lg">View</a>
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--/ End Slider Item -->

          @endforeach
        @endif

      </div>
</section>
