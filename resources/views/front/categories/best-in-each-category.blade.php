@extends('front.layouts.main')

@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-12">
			<div>
				<v-app>
					<app-bests></app-bests>
				</v-app>
			</div>
			<p>
				@include('front.partials.add-product-prompt')
			</p>
		</div>
		
	</div>
</div>
@endsection
