@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<h2 class="page-header">Select an item <span class="text-muted hidden">{{$category->name}}</span></h2>
			<div class="row clear-threes" >
				@if(count($category_items))
					@foreach($category_items->sortBy('name') as $i)
						@include('partials/item-partial-template-1',$i)
					@endforeach
				@else
					<div class="col-sm-12">
						<span>No items found.</span>
					</div>
				@endif

				<p class="col-sm-12">
					@include('partials.add-product-prompt')
				</p>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>

@endsection


