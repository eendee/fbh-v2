@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			
			<div>
				@if(isset($polls) && count($polls)>0)
					@foreach($polls as $p)
						<div class="row">
							<h3>{{$p->name}}</h3>
							<span>
								{{date('j F, Y', strtotime($p->start))}}-
								{{date('j F, Y', strtotime($p->end))}} 
								<span class="badge">{{$p->status()}}</span>
							</span>
							<div>
								{!!$p->description!!}
							</div>
							<div>
								<a class="btn btn-danger btn-dark" href="{{url('poll/'.$p->id)}}">Start</a>
							</div>
							<hr>
						</div>
					@endforeach
				@else
					<div class="alert alert-warning">
						<p>No active polls right now.</p>
					</div>
				@endif
			</div>
		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>
@endsection