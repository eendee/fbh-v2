@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			
			<h5></h5>
			<div class="alert alert-{{$type}}">
				{{$message}}
			</div>
		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/sidebar')
		</div>
	</div>
</div>
@endsection