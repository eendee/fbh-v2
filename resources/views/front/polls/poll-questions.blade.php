@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="col-sm-12">
				<h3>
					{{$poll->name}}
				</h3>
				<section>
					{!!
						$poll->description
					!!}
				</section>
			</div>
			<div class="">
				<div class="col-sm-10">
					<div class="row">
						@if(isset($questions))
							{!! Form::model($poll,array('url' => 'poll/submit/'.$poll->id,'method' => 'post', 'class'=>'form-horizontal')) !!}
								@foreach($questions as $q)
									<div class="poll-question-div">
										<div>
											<span>
												{!!$q->body!!}
											</span>
											@if(isset($q->Options))
												<div>
													@foreach($q->Options as $o)
													<div>
														<div class="iradio">
															<input type="radio" name="questions[{{$q->id}}]" value="{{$o->id}}">
															<span class="poll-question-option">
																{{$o->body}}
															</span>
														</div>
														
													</div>
													@endforeach
												</div>
												
											@endif
										</div>
										
									</div>
								@endforeach
								{{ Form::token() }}
								<div>
									{{ Form::submit('Submit',array('class'=>'btn btn-dark btn-danger pull-right'))}}
								</div>
							{!! Form::close() !!}
						@endif
					</div>
					<div style="margin-bottom:50px"></div>
				</div>
				
			</div>
			
		</div>
		<div id="fbh-sidebar" class="col-md-4">
			<div class="row">
				@include('front/sidebar')
			</div>
		</div>
	</div>
</div>
@endsection