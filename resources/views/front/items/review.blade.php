@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">

				<div class="col-sm-12">
                    @php $size = $badges; @endphp
                    <div class="review-heading">
                        <div class="review-heading__left">Customer Posts & Reviews</div>

                        <div class="review-heading__middle">
                            <span class="review-heading__middle__dot fa fa-circle"></span>
                            <div class="review-heading__middle__name">{{$review->Profile->Fullname()}}'s Post
                                <div style="display: flex; flex-direction: row; margin-left: 8px;"> @include('front.partials.point-badge')</div></div>
                            <span class="review-heading__middle__dot fa fa-circle"></span>
                        </div>
                        @if(isset($item))
                            <div class="review-heading__left">
                                <a href="{{ url('items/'.$item->id)}}">{{$item->name}}</a>
                            </div>
                        @endif
                    </div>
					{{-- <h4 style="display: flex; flex-direction: row;">
                        </span>@include('front.partials.badge', array('rank'=>$review->Profile->Rank)) | <a href="{{ url('items/'.$item->id)}}">{{$item->name}}</a>
					</h4> --}}
					<div class="item-image">
						<div class="row">
							<div class="col-sm-4">
								<div class="review-container">
                                    <div class="review-container__head">
                                        <div class="review-container__head__img">
                                            <img  class="img-responsive" src="{{asset($item->file->ReturnUrl())}}">
                                        </div>
                                        <div>Category: {{$item->Category->name}}</div>
                                        @if(isset($item_details)&& $item_details!=null)
                                            Rating:
                                            @include('front.partials.star-review', array('score'=>$item_details->score))
                                            <span class="text-muted" style="font-size: 0.85em">{{$item_details->ratings}} Rating(s)</span>
                                            <br>
                                        @endif
                                        @if(isset($review_rating) && $review_rating!=null)
                                            {{$review->Profile->Fullname()}}'s Rating
                                            @include('front.partials.star-review', array('score'=>$review_rating->score))
                                        @endif
                                    </div>

                                    <div class="review-summary">

                                        <?php //load templates details  ?>
                                        @if(count($reviewDetails) && count($reviewDetails)>0)
                                            <hr>
                                            <h5>Details</h5>
                                            <ul class="attributes no-list-style review-summary__ul" >
                                                @foreach($reviewDetails as $r)
                                                    @if(0)
                                                        <li>
                                                            Overall Score
                                                            @include('front.partials.star-review', array('score'=>$r->rating))
                                                        </li>
                                                    @endif
                                                    <li class="attribute review-summary__ul__li">
                                                        {{$r->ReviewTemplateDetail->attribute }}<br>
                                                        @include('front.partials.star-review', array('score'=>$r->rating))

                                                    </li>
                                                @endforeach
                                            </ul>

                                        @endif
                                    </div>
                                    <div class="review-summary c-score">
                                        <?php //load templates details  ?>
                                        @if(isset($ComplementaryScore)&&count($ComplementaryScore) && count($ComplementaryScore)>0)
                                            <hr>
                                            <h5>Details</h5>
                                            <ul class="attributes no-list-style">
                                                @foreach($ComplementaryScore as $r)
                                                    <li class="attribute">
                                                        {{$r->ReviewTemplateDetail->attribute }}<br>
                                                        @include('front.partials.star-review', array('score'=>$r->rating))

                                                    </li>
                                                @endforeach
                                            </ul>

                                        @endif
                                    </div>


								</div>

							</div>
							<div class="col-sm-8 card">
								@if($review->activated=="0")
                                    <span class="">
                                        <span class=" text-danger glyphicon glyphicon-info-sign"></span>
                                        <span class="text-muted">This review is awaiting approval </span>
                                    <span>
                                @endif
								<div class="item-description">
									<h3>Description</h3>
                                    {!!$item->description!!}
									@if(isset($review->location)&& $review->location!=null) <br>
										<span class="glyphicon glyphicon-map-marker"> {{$review->location}}
									@endif
								</div>
								<div class="user-review">
									<hr class="colorgraph">
									@if(isset($review->title) && strlen($review->title)>2 && $review->title != 'null')
								      <h4>{{$review->title}}</h4>
								    @endif
									@if($review->review_type_id==2)
										<div class="user-review__img">
											<img  class="img-responsive" src="{{asset($review->File->ReturnUrl())}}">
											<br>
										</div>
									@else

										@if(isset($review->File) && $review->File->id!=null)
											<div class="user-review__img">
												<img  class="img-responsive" src="{{asset($review->File->ReturnUrl())}}">
												<br>
											</div>

										@endif

                                        <div class="user-review__body">
                                            {{$review->body}}
                                            @include('front.partials.time-stamp', array('created_at'=>$review->created_at))
                                        </div>

									@endif
								</div>
								<div class="actions">
									<div class="actions__one">
                                        <div class="actions__one__comment">
                                            <span class="fa fa-comments fa-lg"></span> {{$review->Comments->count()}}
                                        </div>
                                        <div class="actions__one__like">
                                            @include('front.partials.reviews.review-like-button',array('r'=>$review))
                                        </div>
										<div class="actions__one__flag">
											@include('front.partials.reviews.review-flag-button',array('review_id'=>$review->id))
										</div>

									</div>
									<div class="actions__two">
										<h4>Share this</h4>
										@include('front.partials.social',array('s_title'=>"Feedbackhall customer review for ".$review->Item->name))
									</div>
								</div>
								<div class="show-comments">

									@if(isset($comments))
										<div class="row">
											<div class="col-sm-12">
												<h4 style="color: #6B0707; font-weight: bold; margin-bottom: 15px;">Comments ({{$review->Comments->count()}})</h4>
											</div>

											@foreach($comments as $c)

                                                @if ($c->user->role_id == 3)

                                                    @php
                                                        $url = $c->User->business->Image->ReturnThumbnail();
                                                        if (auth()->user() && auth()->user()->id == $c->User->id) {
                                                            $commentorName = 'You';
                                                        } else {
                                                            $commentorName = $c->User->business->name;
                                                        }
                                                    @endphp
                                                @else

                                                    @php
                                                        $url = $c->GetProfile()->ReturnThumbnail();
                                                        if (auth()->user() && auth()->user()->id == $c->User->id) {
                                                            $commentorName = 'You';
                                                        } else {
                                                            $commentorName = $c->GetCommenterName();
                                                        }

                                                    @endphp

                                                @endif

												<div class="row" style="margin-bottom:20px; padding-left: 15px; padding-right: 15px">

													<div class="col-sm-2">
														{{-- <img class="img-circle img-responsive"
                                                        style="max-width:50px;"
														src="{{asset($c->GetProfile()->ReturnThumbnail())}}"> --}}
                                                        <img class="img-circle img-responsive"
                                                        style="max-width:50px;"
														src="{{asset($url)}}">
													</div>
													<div class="col-sm-10" style="padding-left: 0px">
														<div>
															<a href="#">{{$commentorName}}</a> said
															<span class="text-muted pull-right text-small-1"> {{ date('j F, Y', strtotime($c->created_at))}} </span>
														</div>
														<div>
															{{{$c->body}}}
														</div>
													</div>
												</div>
											@endforeach
										</div>
									@endif
								</div>
								<hr>
								<h4>Post comment</h4>
								<div class="add-comment col-sm-12"  style="margin-bottom:20px; padding: 0px;">
									@if(Auth::Check())
                                        <div style="padding: 20px;">
                                            @if($showCommentBox)
                                                {!! Form::model($review,array('url' => "review/comment/$review->id",'method' => 'post', 'class'=>'form-horizontal')) !!}

                                                    <div class="form-group">
                                                        {{Form::label('Comment')}}
                                                        {{Form::textarea('body','', array('class'=>'textarea form-control', 'id'=>'wysihtml5',  'rows'=>'3', 'required'=>true ))}}
                                                    </div>
                                                    <div class="form-group">

                                                        {{ Form::submit('Save',array('class'=>'btn btn-success btn-dark pull-right'))}}
                                                    </div>
                                                {{ Form::token() }}
                                                {!! Form::close() !!}
                                            @else
                                                <p class="text-info">You can only comment on reviews of your own products/services</p>
                                            @endif
                                        </div>
									@else
										@include('front.layouts.login',array('url'=>'abc'))
									@endif
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/sidebar')
		</div>
	</div>
</div>

@include('front.partials.like-javascript')
@endsection
