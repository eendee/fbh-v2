@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				<div>
					<h2>Flag a Review</h2>
					<h4>Review By: {{$review->profile->fullname()}} for {{$review->item->name}}</h4>
					<p class="well">If you found this review to be offensive, misleading or if  you feel
					it violates any of our terms and conditions, please kindly inform us and we will look into it.</p>

					<div class="col-sm-10">
						{!! Form::model($flag,array('url' => "review/flag/save/$review->id",'method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}

							<div class="row">
                                @if ($user->Profile)
                                    <div class="col-sm-2">
                                        <img class="img-responsive" style="max-width:80px; margin-right:40px;" src="{{asset($user->Profile->File->ReturnThumbnail())}}">
                                    </div>
                                @endif
								<div class="col-sm-10">
									<div class="form-group">
										{{Form::label('Reason for flagging this review')}}
										{{Form::textarea('body',$flag->body, array('class'=>'textarea form-control', 'id'=>'wysihtml5', 'required' ))}}
									</div>
                                    <div class="form-group">
                                        {{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
                                    </div>
								</div>
							</div>


						{!! Form::close() !!}
					</div>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>

@endsection

