<?php
  use app\Helpers\SiteHelper ;
?>
@extends('front.layouts.main')
@section('title', 'Posts & Reviews: '. $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				{{-- <div class="col-sm-2">

				</div> --}}
				<div class="col-sm-12 card">
						@if(Session::has('message'))
							<div class="alert alert-info">
								{{Session::get('message')}}
							</div>
						@endif
					<h4 style="padding-top: 8px; padding-bottom: 8px; font-weight: bold;"> View all reviews and posts for {{$item->name}}</h4>
					<div class="item-image">
						<div class="frame2 border-style-1">
							<img  class="img-responsive" src="{{asset($item->File->ReturnUrl())}}">
						</div>
					</div>
					<div style="margin-bottom: 10px; margin-top: 20px;">
                        @if(isset($item->Category))
                            <p><b>Category:</b> {{{$item->Category->name}}} </p>
                        @endif
                        <p>
                            Rating:
                            @if($itemDetails!=null && $itemDetails->ratings>0)
                                @include('front.partials.star-review', array('score'=> $itemDetails->score))
                                <span>From {{$itemDetails->ratings}} Rating(s)</span>
                            @else
                                <span>Not yet rated, be the first to rate it!</span>
                            @endif
                        </p>
					</div>
					<ul class="nav nav-tabs" style="font-size:.9em;">
					  <?php $tab="";  ?>
					  <li id="user-reviews-anchor" class="active"><a data-toggle="tab" href="#user-reviews">Summary from users</a></li>
					  <li id="fbh-reviews-anchor" class=""><a data-toggle="tab" href="#fbh-reviews">Summary from Admins</a></li>
					</ul>
					@if(isset($reviewDetails)&&count($reviewDetails)>1)
						<div class="tab-content">
							<div id="user-reviews" class="tab-pane fade in active">
								<div class="review-summary" style="margin-top: 15px;">
										@if(isset($reviewDetails)&&count($reviewDetails)>1)
											<ul class="no-list-style attributes">
												@foreach($reviewDetails as $rd)
													<li style="padding: 10px;">
														<span class="attribute">{{$rd->ReviewTemplateDetail->attribute}}</span>
														@include('front.partials.star-review', array('score'=>$rd->rating))
													</li>
												@endforeach
											</ul>
										@endif
								</div>
							</div>
							<div  id="fbh-reviews" class="tab-pane fade">
								<div class="review-summary c-score" style="margin-top: 15px;">
									<?php //load templates details  ?>
									@if(isset($ComplementaryScore)&&count($ComplementaryScore) && count($ComplementaryScore)>0)

										<ul class="no-list-style attributes">
											@foreach($ComplementaryScore as $r)
												<li>
													<span class="attribute"> {{$r->ReviewTemplateDetail->attribute }}</span>
													@include('front.partials.star-review', array('score'=>$r->rating))

												</li>
											@endforeach
										</ul>

									@endif
								</div>
							</div>
						</div>
					@endif


					<div class="item-description">
						<h4 style="font-weight: bold; color: #6B0707; margin-top: 15px">Brief Description</h4>
						{!!$item->description!!}

					</div>
					<div>
						<h4>Share this</h4>
						@include('front.partials.social',array('s_title'=>"Feedbackhall customer reviews for $item->name"))

					</div>


					@if($isBusiness)
						<h4 style="margin-bottom: 20px;">Only user accounts can make reviews</h4>
					@else
                        <hr class="colorgraph">
						<div style="border: 1px solid #f2f2f2; padding: 5px;">
							@include('front.partials.reviews.review-form', array('sender'=>''))
						</div>
					@endif

					<div class="">
						@include('front.partials.add-product-prompt')
					</div>
					<hr class="colorgraph">
					<div class="item-reviews">
						<hr>

						<h4 style="color: #6B0707; font-weight: bold">Customer Reviews & Posts</h4>
						@if(isset($posts) && count($posts))
							@foreach($posts as $r)


                                <div class="row" style="margin-top:20px;">

                                    <div class="col-sm-2">
                                        <img class="img-responsive img-circle" style="max-width:70px; margin-right:30px; margin-bottom: 10px;"
                                                        src="{{asset($r['review']->Profile->File->ReturnThumbnail())}}">
                                    </div>
                                    <div class="col-sm-8">
                                        @if(Auth::Check())
                                            @if(Auth::user()->id==$r['review']->user_id)
                                                @if($r['review']->IsInEditWindow())
                                                    <div class="">
                                                        <p>
                                                            <a class="" href="{{url('reviews/edit/'.$r['review']->id)}}"><span class="glyphicon glyphicon-edit"></span>
                                                                Edit
                                                            </a>
                                                        </p>
                                                    </div>
                                                @endif
                                            @endif
                                        @endif
                                        <?php $body_array=null; $body_array=SiteHelper::ReturnWordsWithFlag($r['review']->body,50); ?>
                                        @if(isset($r['type'])&&$r['type']=='1')
                                            @if(isset($r['rating']))
                                                <div>
                                                    <?php $_score=0;
                                                        $_score=isset($r['review']->overall_score) && $r['review']->overall_score!=null?$r['review']->overall_score:$r['rating']->score;
                                                    ?>

                                                    {{-- {{$r['review']->Profile->Fullname()}} @include('front.partials.badge', array('rank'=>$r['review']->Profile->Rank))  rated it --}}
                                                    <b>{{$r['review']->Profile->Fullname()}}</b> &nbsp;rated it &nbsp;&nbsp;
                                                    @include('front.partials.star-review', array('score'=>$_score))
                                                    @include('front.partials.time-stamp', array('created_at'=>$r['review']->created_at))

                                                </div>
                                            @else
                                                <div>

                                                    {{-- {{$r['review']->Profile->Fullname()}} @include('front.partials.badge', array('rank'=>$r['review']->Profile->Rank)) review --}}
                                                    <b>{{$r['review']->Profile->Fullname()}}</b>  review
                                                    @include('front.partials.time-stamp', array('created_at'=>$r['review']->created_at))
                                                </div>
                                            @endif
                                            @if ($r['review']->title && $r['review']->title != 'null')
                                                <h4>{{$r['review']->title}}</h4>
                                            @endif

                                            <div>
                                                {{$body_array[0]}}
                                                @if($body_array[1])
                                                    <a href="{{url('reviews/show/'.$r['review']->id)}}">... read more</a>
                                                @endif
                                            </div>
                                        @elseif(isset($r['type'])&&$r['type']=='3')

                                            {{$r['review']->Profile->Fullname()}} @include('partials.badge', array('rank'=>$r['review']->Profile->Rank))  asked a question
                                            @include('partials.time-stamp', array('created_at'=>$r['review']->created_at))

                                            <div class="question">

                                                <blockquote>
                                                    {{$body_array[0]}}
                                                    @if($body_array[1])
                                                        <a href="{{url('reviews/show/'.$r['review']->id)}}">... read more</a>
                                                    @endif
                                                </blockquote>
                                            </div>

                                        @else
                                            <div>

                                                {{$r['review']->Profile->Fullname()}} @include('partials.badge', array('rank'=>$r['review']->Profile->Rank))  Posted an image
                                                @include('partials.time-stamp', array('created_at'=>$r['review']->created_at))
                                                <img style="width:80%"  class="img-responsive border-style-1" src="{{asset($r['review']->File->ReturnUrl())}}">
                                            <br>
                                            </div>
                                        @endif
                                        <div class="actions">
                                            @include('front.partials.reviews.review-actions', array('r'=>$r['review']))
                                        </div>
                                    </div>
                                </div>


							@endforeach

							<p>
								{{ $reviews->links() }}
							</p>
						@else
							<p>No customer reviews yet @if(!$isBusiness), be the first to add a review!@endif</p>
						@endif
					</div>


				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>
@endsection

