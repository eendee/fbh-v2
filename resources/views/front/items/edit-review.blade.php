@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				<div class="card" style="padding:30px;">
					<h2>Edit review <small>Only avaliable 15 minutes after posting review</small></h2>
					@include('front.partials.reviews.review-form',array('sender'=>"/$review->id"))
					
				</div>

			</div>

		</div>
	</div>
</div>
@endsection

