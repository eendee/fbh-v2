@extends('front.layouts.main')
@section('title', "Pending Approval")
@section('description', "")
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div>
				<h2>{{$page->name}}</h2>
				
				
				<div id="overview">
					{!! $page->body !!}
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/sidebar')
		</div>
	</div>
</div>
@endsection
