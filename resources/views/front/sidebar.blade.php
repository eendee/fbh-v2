<div id="fb-sb" class="sidebar">
	<h4>Categories</h4>
	@if(isset($cats))

		<ul class="nav">
			@foreach($cats as $root=>$c)
				<li>
					{{$c['name']}}
					@if(isset($c['children']))
						<ul class="collapsibleList">
							@foreach($c['children'] as $c1=>$v1)
								<li >

									@if(isset($v1['children']))
										{{$v1['name']}}
										<ul>
											@foreach($v1['children'] as $c2=>$v2)
												<li>
													<a href="{{url('category/'.$v2['id'])}}">{{$v2['name']}}</a>
												</li>
											@endforeach
										</ul>
									@else
										<li>
											<a href="{{url('category/'.$v1['id'])}}">{{$v1['name']}}</a>
										</li>
									@endif

								</li>
							@endforeach
						</ul>
					@endif
				</li>
			@endforeach
		</ul>
	@endif
	<h4 class="sidebar__second">Newest Items</h4>
	@if(isset($items))

		<ul class="nav2">
			@foreach($items as $i)
				<li>
					<a href="{{url('items/'.$i->id)}}">{{$i->name}}</a>
				</li>
			@endforeach
		</ul>
	@endif

</div>
