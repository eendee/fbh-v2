@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
  <div class="row">
    <div id="fbh-main" class="col-md-8">
      <div>
          @if(isset($faqs))
            @foreach($faqs as $f)
              <div class="accordion" id="accordionFAQ">

            <div class="card z-depth-0 bordered">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                  data-target="#collaps{{$f->id}}" aria-expanded="false" aria-controls="collaps{{$f->id}}">
                  {{$f->name}}
                </button>
              </h5>
            </div>
            <div id="collaps{{$f->id}}" class="collapse" aria-labelledby="heading{{$f->id}}" data-parent="#accordionFAQ">
              <div class="card-body">
                {!!$f->body!!}
              </div>
            </div>
          </div>
        </div>
            @endforeach
          @endif
      </div>

    </div>
    <div id="fbh-sidebar" class="col-md-4">
      @include('front.sidebar')
    </div>
  </div>
</div>
@endsection
