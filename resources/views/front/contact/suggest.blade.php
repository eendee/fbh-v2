@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				<div>
					<h2 class="page-header">Product/Service listing request</h2>
					<p class="well well-success">
							Got any product/service you want us to add? Then send us a message with the form below. 

						</p>
					<div class="">
						
						<hr class="colorgraph">
						<div class="card"  style="padding:40px;">
							{!! Form::model($m,array('url' => "contact/list-item/save",'method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
					    	
								<div>
									
									<div class="">
										<div class="form-group">
											{{Form::label('Name')}}
											{{Form::text('name',$m->name,array('class'=>'form-control','required'=>'required'))}}

										</div>
										<div class="form-group">
											{{Form::label('Email')}}
											{{Form::text('email',$m->email,array('class'=>'form-control','required'=>'required'))}}

										</div>
										<div class="form-group">
											{{Form::label('Name of Product/Service')}}
											{{Form::text('product',$m->product,array('class'=>'form-control','required'=>'required'))}}

										</div>
										<div class="form-group">
											{{Form::label('Name of Producer/Provider')}}
											{{Form::text('producer',$m->producer,array('class'=>'form-control','required'=>'required'))}}

										</div>

										<div class="form-group">
											{{Form::label('Locations where the product/service is made available ')}}
											{{Form::text('locations',$m->location,array('class'=>'form-control','required'=>'required'))}}

										</div>

										<div class="form-group">
											{{Form::label('Primary reason for listing')}}
											{{Form::textarea('reason',$m->reason, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
										</div>
									</div>
								</div>
								<div class="form-group">
									{{ Form::token() }}
									{{ Form::submit('Save',array('class'=>'btn btn-success btn-dark pull-right'))}}
								</div>
							
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.about.sidebar')
		</div>
	</div>
</div>

@endsection

