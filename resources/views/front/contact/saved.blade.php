@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				<div>
					<h2 class="page-header">Thank You {{$m->name}}</h2>
					<div class="col-sm-12 card alert alert-success">
						<p>
							@if(isset($m->message))
								{{$m->message}}
							@else
								Your Message Has been saved. We will respond to you shortly.
							@endif

						</p>
					</div>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>

@endsection

