@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				<div>
					<h2 class="page-header">Thank You {{$business->name}}</h2>
					<div class="col-sm-12 card">
						<p>
							Your Message Has been received, we will evaluate your submissions and respond to you shortly via the email you provided.
						</p>
					</div>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>

@endsection

