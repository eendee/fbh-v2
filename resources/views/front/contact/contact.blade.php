@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div class="row">
				<div>
					<h2 class="page-header">We would love to hear from you!</h2>
					<p class="col-sm-12">
							Thank you for visiting Feedbackhall.com.<br>
							 You are important to us! To get information about any of our services or give us feedback,
							 please contact us:<br>


							 No 3, Nwanza Close, Off Idimba Street, Zone 3, Wuse, Abuja<br>
                            Phone: +2349062010222, +2349055566666<br>
                            e-mail: contact@feedbackhall.com

						</p>
					<div class="col-sm-12">


						<div>
							<div style="background:#f3f3f3; padding:20px;">
								{!! Form::model($m,array('url' => "contact-us/success",'method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}

								<div>
									<h2>Send us a message</h2>
									<div class="">
										<div class="form-group">
											{{Form::label('Name')}}
											{{Form::text('name',$m->name,array('class'=>'form-control','required'=>'required'))}}

										</div>
										<div class="form-group">
											{{Form::label('Email')}}
											{{Form::text('email',$m->email,array('class'=>'form-control','required'=>'required'))}}

										</div>

										<div class="form-group">
											{{Form::label('Message Title')}}
											{{Form::text('title',$m->title,array('class'=>'form-control','required'=>'required'))}}

										</div>

										<div class="form-group">
											{{Form::label('Message Body')}}
											{{Form::textarea('body',$m->body, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
										</div>
									</div>
								</div>
								<div class="form-group">
									{{ Form::token() }}
									{{ Form::submit('Save',array('class'=>'btn btn-success btn-dark pull-right'))}}
								</div>

								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/about/sidebar')
		</div>
	</div>
</div>

@endsection

