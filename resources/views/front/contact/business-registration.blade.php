@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			
			<div  >
				<div class="well well-success card">
					Signing up as a business allows you to manage some information about your
				product or service, see key insights about your business and publicize 
				special offers that align with our terms and conditions.
					</div>
				<div>
					<div class="card" style="padding:40px;">
					@if(Session::has('message'))
						<div class="alert alert-warning">
							{{Session::get('message')}}
						</div>
					@endif
					{!! Form::model($business,array('url' => 'business-signup-save','method' => 'post', 'class'=>'form-horizontal')) !!}
				    	<div class="form-group">
							{{Form::label('Name of business')}}
							{{Form::text('name',$business->name,array('class'=>'form-control','required'=>'required'))}}
						</div>

						<div class="form-group">
							{{Form::label('Company Registration Number')}}
							{{Form::text('rc_no',$business->rc_no,array('class'=>'form-control','required'=>'required'))}}
						</div>
						<div class="form-group">
							{{Form::label('Address')}}
							{{Form::text('address',$business->address,array('class'=>'form-control','required'=>'required'))}}
						</div>

						<div class="form-group">
							{{Form::label('Products')}}
							{{Form::text('products',$business->products,array('class'=>'form-control','required'=>'required'))}}
						</div>

						<div class="form-group">
							{{Form::label('Company Email')}}
							{{Form::text('email',$business->email,array('class'=>'form-control','required'=>'required'))}}
						</div>
						<div class="form-group">
							{{Form::label('Website')}}
							{{Form::text('website',$business->website,array('class'=>'form-control','required'=>'required'))}}
						</div>
						<div class="form-group">
							{{Form::label('Phone number')}}
							{{Form::text('phone',$business->phone, array('class'=>'form-control', 'id'=>'wysihtml5' ))}}
						</div>
						<div>
							{{ Form::submit('Save',array('class'=>'btn btn-danger btn-dark pull-right'))}}
						</div>
						{{ Form::token() }}
					{!! Form::close() !!}
				</div>
				</div>
					
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front.sidebar')
		</div>
	</div>
</div>

@endsection

