

	<form class="navbar-form hidden-xs" role="search" action="{{url('search')}}" style="display: flex; margin-top: 25px; padding: 0px">
		<span class="screen-reader-text"></span>
		<input type="search" id="s" name="s" class="form-control" style="width: 75%; border: 1px 0px 0px 1px;margin-right: 0px; border-radius: 8px 0px 0px 8px"
        placeholder="find products and services" title="Search for:">
		<button class="btn-search-form" type="submit" style="width: 25%; margin-left: 0px; border-radius: 0px 8px 8px 0px" title="Search">
			<i class="fa fa-search"></i>
		</button>
	</form>
