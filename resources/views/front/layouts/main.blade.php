<!DOCTYPE html>
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117881241-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-117881241-1');
    </script>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta property="og:type" content= "website" />
    <meta property="og:site_name" content="Feedbackhall" />
    <meta property="og:image" content="https://feedbackhall.com/img/fbh-sharable-image.png">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:domain" content="feedbackhall.com"/>
    <meta name="twitter:title" property="og:title" itemprop="name" content="Feedbackhall: The Reality" />
    <meta name="twitter:description" property="og:description" itemprop="description" content="FeedbackHall is Nigeria's largest feedback platform, launched in 2018. It is a platform on which Nigerians share real life experiences and opinions gathered by interacting with products and services offered in Nigerian markets - made in Nigeria or not."/>


    <title>@yield('title') | Feedbackhall.com</title>
    <link rel="shortcut icon" href="{{url('img/icon.png')}}" type="image/x-icon" />
    <meta name="description" content="@yield('description')">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">


    {{--  <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">  --}}
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato%3A400%2C500%2C600%2C700%7CHind%3A400%2C600%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link href="{{ asset('assets/plugins/flexslider/flexslider.css') }}" rel="stylesheet">
    {{--  <link href="{{ asset('css/owlcarousel2.css') }}" rel="stylesheet">  --}}
    <link href="{{ asset('assets/plugins/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/slick/slick-theme.css') }}" rel="stylesheet">


    <link href="{{ asset('css/fbh.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
 <!--    <link href="{{asset('assets/plugins/iCheck/square/red.css')}}" rel="stylesheet" /> -->


    {{--  <script src="{{asset('js/popper.js')}}"></script>  --}}
    @if(isset($meta->load_app_js) && $meta->load_app_js===1)
        <script src="{{asset('js/app.js')}}"></script>
    @else
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    @endif

    {{-- @elseif (isset($meta->load_app_js) && $meta->load_app_js===0)
        <script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
    @else
         <script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
    @endif --}}


    {{-- <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/flexslider/flexslider.js')}}"></script>
    <script src="{{asset('assets/plugins/slick/slick.js')}}"></script>
    <script src="{{asset('js/CollapsibleLists.js')}}"></script>

    {{--  <script src="{{asset('js/owlcarousel2.js')}}"></script>  --}}
  {{--  <!--   <script src="{{asset('assets/plugins/iCheck/icheck.min.js')}}"></script> -->  --}}


    <script>
window['_fs_debug'] = false;
window['_fs_host'] = 'fullstory.com';
window['_fs_org'] = 'B9H51';
window['_fs_namespace'] = 'FS';
(function(m,n,e,t,l,o,g,y){
    if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
    g=m[e]=function(a,b){g.q?g.q.push([a,b]):g._api(a,b);};g.q=[];
    o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
    y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
    g.identify=function(i,v){g(l,{uid:i});if(v)g(l,v)};g.setUserVars=function(v){g(l,v)};
    y="rec";g.shutdown=function(i,v){g(y,!1)};g.restart=function(i,v){g(y,!0)};
    g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
    g.clearUserCookie=function(){};
})(window,document,window['_fs_namespace'],'script','user');
</script>
</head>
<body>
    <?php //exit();  ?>
    <div style="background:#B63939; height:95px; ">
        <div class="container header-container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 navbar-brand" style="padding: 0;">
                    <img src="{{asset('assets/img/logo.png')}}" style="height:80px;">
                </div>

                {{-- @if ($header_adverts->count() > 0)
                    <div class="col-md-6 hidden-xs hidden-sm">
                        <div id="myCarousel" class="carousel slide ad" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner ad__c" role="listbox">
                                @foreach ($header_adverts as $key => $advert)
                                    <div class="item {{ $key == 0 ? 'active' : ''}} ad__c__i">
                                        <img src="{{asset($advert->image)}}" alt="Flower" width="360" >
                                        <div class="carousel-caption">
                                            <h4>{{$advert->title}}</h4>
                                            <p>Beautiful flowers in Kolymbari, Crete.</p>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>


                @endif --}}


                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                    data-toggle="collapse" data-target="#main-nav-1" style="border: 1px solid white; margin-top: -20px;">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <div class="pull-right col-sm-3">
                    <div class="hidden-xs hidden-sm">
                        @include('front.layouts.search-form')
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div>
        @include('front.layouts.menu')
    </div>
    <div>
        @if(!isset($skip_header))
             @include('front.layouts.below-menu')
        @endif
    </div>
    <div id="app" >
        @yield('content')
    </div>


    @include('front/layouts.footer')
    <div style="background:black; height:20px; width:100%;">

    </div>
</body>
    <script src="{{asset('js/fbh.js')}}"></script>
</html>
