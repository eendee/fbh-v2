<div style="min-height:100px; background:#F9F9FA; margin-bottom:10px; width:100%;">
	<div class="container">
		@if(!isset($hideTitle))
			@if(isset($page)&& isset($page->page_section_id) && $page->page_section_id==5)
				<h3>News</h3>
			@else
				<h3>@yield('title')</h3>
			@endif
		@endif
	</div>
</div>