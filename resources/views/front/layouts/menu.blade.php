    <?php //dd($menu);  ?>
    <nav class=" navbar-default" role="navigation" style="border-bottom: 0px;" >
        <div class="container header-container" style="">
            <div class="row" style="margin-bottom: 0px">
                <div class="col-sm-12" style="">
                    {{-- <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div> --}}
                    <div class="collapse navbar-collapse" id="main-nav-1" style="border: 0px solid green">
                        <ul class="nav navbar-nav">
                            <li class="{{isset($menu['home'])?$menu['home']:""}}"><a href="{{url('/')}}">Home</a></li>
                            <li class="{{isset($menu['about'])?$menu['about']:""}}"><a href="{{url('/about')}}">About</a></li>
                            <li class="{{isset($menu['contact-us'])?$menu['contact-us']:""}}"><a href="{{url('/contact-us')}}">Contact</a></li>
                            <li class="{{isset($menu['categories'])?$menu['categories']:""}}"><a href="{{url('/categories')}}">Categories</a></li>
                            <li class="{{isset($menu['write-a-review'])?$menu['write-a-review']:""}}"><a href="{{url('/write-review')}}">Write review</a></li>
                            <li class="{{isset($menu['post-an-image'])?$menu['post-an-image']:""}} hidden"><a href="{{url('/write-review/post')}}">Add photo</a></li>
                            <li class="{{isset($menu['blog'])?$menu['blog']:""}} hidden-md"><a href="{{url('/blog')}}">Blog</a></li>
                            <li class="{{isset($menu['polls'])?$menu['polls']:""}} hidden-md"><a href="{{url('/polls')}}">Polls</a></li>

                            {{-- <a href="{{url('/write-review')}}" style="background:#595252" class="">Write a Review</a> --}}
                            <li class="{{isset($menu['write-a-review'])?$menu['write-a-review']:""}}">
                                <a href="{{url('/write-review/post')}}" class="">Add a Photo</a>
                            </li>
                            <li class="hidden-sm hidden-lg {{isset($menu['write-a-review'])?$menu['write-a-review']:""}}">
                                <a href="{{url('/write-review/question')}}" class="">Ask a Question</a>
                            </li>


                        </ul>
                        <div class="col-sm-5 col-md-4 col-lg-4 pull-right">
                            <ul class="nav navbar-nav navbar-right">
                                @if(!Auth::guest())
                                    <li class="{{isset($menu['user'])?$menu['user']:""}}"><a href="{{url('user/')}}">My Dashboard</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                            </form>
                                    </li>

                                @else
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                    <li>
                                        <a href="{{url('/business-signup')}}">Businesses</a>
                                    </li>

                                @endif
                            </ul>
                        </div>

                        <div class="hidden-md hidden-lg">

                            <form class="navbar-form " role="search" action="{{url('search')}}" style="border: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; ">
                                <span class="screen-reader-text"></span>
                                <input type="search" id="s" style="width: 80%; display: inline; border: 1px 0px 0px 1px;margin-right: 0px; border-radius: 8px 0px 0px 8px" name="s" class="form-control" placeholder="find products and services" title="Search for:">
                                <button class="btn-search-form" type="submit" style="width: 18%; margin-left: 0px; border-radius: 0px 8px 8px 0px" title="Search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                    {{-- <div class="hidden-md hidden-lg">

                        <form class="navbar-form " role="search" action="{{url('search')}}" style="border: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; ">
                            <span class="screen-reader-text"></span>
                            <input type="search" id="s" style="width: 80%; display: inline; border: 1px 0px 0px 1px;margin-right: 0px; border-radius: 8px 0px 0px 8px" name="s" class="form-control" placeholder="find products and services" title="Search for:">
                            <button class="btn-search-form" type="submit" style="width: 18%; margin-left: 0px; border-radius: 0px 8px 8px 0px" title="Search">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div> --}}
                </div>
            </div>
        </div>
    </nav>
