    <div class="footer" style="background:#6B0707; width:100%; margin-top:50px; padding-top:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="footer-heading">Connect With Us</h3>
                    <span style="color:white">
                        <a href="https://www.facebook.com/feedbackhall"><i class="fa fa-facebook-square fa-lg"></i></a>
                        <a href="https://twitter.com/feedback_hall"><i class="fa fa-twitter-square fa-lg"></i></a>
                        <a href="https://www.instagram.com/feedbackhall"><i class="fa fa-instagram fa-lg"></i></a>
                     </span>
                </div>
                <div class="col-md-3">
                    <h3 class="footer-heading">Quick Links</h3>
                    <div>
                        <ul class="menu">
                            <li>
                                <a href="{{url('/about')}}">About</a>
                            </li>
                            <li>
                                <a href="{{url('/faq')}}">Frequently Asked Questions</a>
                            </li>
                            <li>
                                <a href="{{url('/write-review')}}">Write A Review</a>
                            </li>
                            <li>
                                <a href="{{url('/contact/list-item')}}">List New Product/Service</a>
                            </li>
                            <li>
                                <a href="{{url('/advertise')}}">Advertise With  Feedbackhall</a>
                            </li>
                            <li>
                                <a href="{{url('/careers')}}">Careers</a>
                            </li>
                            <li>
                                <a href="{{url('/advertising-policy')}}">Advertising Policy</a>
                            </li>
                            <li>
                                <a href="{{url('/privacy-policy')}}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{url('/Content-guidelines')}}">Content Guidelines</a>
                            </li>
                            <li>
                                <a href="{{url('/terms-of-service')}}">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="{{url('/business-signup')}}">Business  Account</a>
                            </li>
                            <li>
                                <a href="{{url('/business-owner-support')}}">For Business Owners</a>
                            </li>
                            <li>
                                <a href="https://feedbackhall.com:2096/">Mail</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <h3 class="footer-heading">Search</h3>
                    <div>
                        @include('front/layouts.search-form')
                    </div>
                </div>
                <div class="col-md-3">
                    <h3 class="footer-heading">Contact Us</h3>
                    <div>
                    <p>
                        No 3, Nwanza Close, Off Idimba Street, Zone 3, Wuse, Abuja.
                    </p>
                        Phone: +2349062010222, +2349055566666;
                       <br> e-mail: contact@feedbackhall.com
                    </div>
                </div>
            </div>
            <div class="disclaimer"><br>© {{date("Y")}} Feedback Hall Limited. <img class="footer-img" src="{{asset('img/branding/fbh-logo-only.png')}}">  and <img  class="footer-img" src="{{asset('assets/img/logo.png')}}">  are registered trademarks of Feedback Hall Limited. All Rights Reserved. <br>
               @if(isset($disclaimer))
                 {!!$disclaimer->body!!}
               @endif
            </div>
        </div>
    </div>

