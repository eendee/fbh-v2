<?php 
  use app\Helpers\SiteHelper ;
?>
@extends('front.layouts.main')
@section('title', $postDetails['type'])
@section('description', $meta->description)
@section('content')
<div class="container">
	
	<h2 class="text-center">{{$page->name}}</h2>
	

	<hr class="colorgraph">
	<br>
	<div class="row">
		<div class="col-sm-6 card" style="background:#f2f2f2">			
			<div class="col-sm-12">
				<div class="div-centered-800">
					<h3>{{$postDetails['type']}}</h3>
					<script type="text/javascript">
					var _type="{{$postDetails['_type']}}"
					</script>
				</div>
				<div>
				
					<span us-spinner="{radius:150, width:50, length: 56}" spinner-key="mySpinner" spinner-on="showSpinner"></span>
					<form method="POST" action="{{url('review')}}" accept-charset="UTF-8" style="max-width: 100%; padding-left: 20px;" class="form-horizontal" enctype="multipart/form-data">
						<div>
							<p class="hidden1">
								 {!!$page->body!!}
							</p>			
							<div class="form-group">
								<span class="glyphicon glyphicon-list-alt"></span> 
								{{Form::label('Please select product or service category')}}
								{{Form::select('category_id',  $CatsWithItems,"0",array(
									'id'=>'category_id',
									'class'=>'form-control', 
									'required'=>"required",  
									'onchange'=>"location ='../category/'+this.options[this.selectedIndex].value+'?type='+_type;"
								))}}
							</div>

							
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="hidden1">
					
				<img  class="img-responsive" src="{{asset('img/Write-a-review.jpg')}}">
				
			</div>
			
		</div>

		
</div>
<style type="text/css">
	
	p br{
		display: none;
		padding-left: 3px;

	}
</style>



@endsection

