@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div>
				<h2>Wrong Busstop!</h2>
				<div>
				
				</div>
				<div>
					<p>The item you are looking for does not exist</p>
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/sidebar')
		</div>
	</div>
</div>
@endsection
