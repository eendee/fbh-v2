@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<h2 class="text-center">Showcase your products and services on Feedbackhall</h2>
	<hr class="colorgraph">
	<br>
	<div class="row">
		<div id="fbh-main" class="col-md-12">
			<div class="row">
				<div>
					<div>
						<div class="col-sm-3 text-center">
							<span style="font-size:3em;" class="glyphicon glyphicon-send"></span>
							<h4>
								Many consumers obtain information online before making purchase decisions
							</h4>
							<p class="text-muted">
								The Feedbackhall website is Nigeria’s first comprehensive consumer opinion site, designed exclusively for providing such information.
							</p>
						</div>
						<div class="col-sm-3 text-center">
							<span style="font-size:3em;" class="glyphicon glyphicon-flash"></span>
							<h4>
								Feedbackhall’s targeted ads appear in a variety of right places 
							</h4>							    
							<p class="text-muted">
								Your adverts, along with  rating and top reviews will appear  on relevant pages and also on search results of consumers who are searching for similar products
							</p>
						</div>
						
						<div class="col-sm-3 text-center">
							<span style="font-size:3em;" class="glyphicon glyphicon-check"></span>						
							 <h4>
							 	Feedbackhall adverts allows you to advance your profile 
							 </h4>
							<p>
								When you place an advert, you can also enhance your product profile or business page with  videos, Photo slide shows, and a message with call to action features.
							</p>
						</div>
						<div class="col-sm-3 text-center">
							<span style="font-size:3em;" class="glyphicon glyphicon-heart"></span>
							<h4>
								Convert your enhanced Feedback Hall product profile viewers into new customers or clients
							</h4>
							<p class="text-muted">
								Potential  consumers can navigated from your Feedback Hall  product page  directly to a page on your website.
							</p>
						</div>
					</div>							
				</div>	

			</div>
			<div class="row">
				<h2 class="text-center">
					
					Send Us A Message
				</h2>
				<hr class="colorgraph">

				<div class="col-sm-6 col-sm-offset-3" style="background:#f2f2f2;">
				<br>
					<div class="col-sm-12">
						{!! Form::model($m,array('url' => "advertise",'method' => 'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
			    	
					
						
						
							<div class="form-group">
								{{Form::label('Name')}}
								{{Form::text('name',$m->name,array('class'=>'form-control','required'=>'required'))}}

							</div>
							<div class="form-group">
								{{Form::label('Email')}}
								{{Form::text('email',$m->email,array('class'=>'form-control','required'=>'required'))}}

							</div>
							<div class="form-group">
								{{Form::label('Phone')}}
								{{Form::text('phone',$m->phone,array('class'=>'form-control','required'=>'required'))}}

							</div>

							<div class="form-group">
								{{Form::label('Name of organization')}}
								{{Form::text('organization',$m->phone,array('class'=>'form-control','required'=>'required'))}}

							</div>

							<div class="form-group">
								{{Form::label('What is your role in your organization')}}
								{{Form::text('role',$m->email,array('class'=>'form-control','required'=>'required'))}}

							</div>

							


							<div class="form-group">
								{{Form::label('Message')}}
								{{Form::textarea('message',$m->message, array('class'=>'textarea form-control', 'id'=>'wysihtml5' ))}}
							</div>
						
							<div class="form-group">
								{{ Form::token() }}
								{{ Form::submit('Send',array('class'=>'btn btn-success btn-dark pull-right'))}}
							</div>
					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>

	</div>
		
</div>


@endsection

