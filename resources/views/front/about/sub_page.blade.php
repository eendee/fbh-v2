@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div>
				<h2>Empowering Consumers Everywhere</h2>
				
				<div>
					 <img class="img-responsive" src="{{asset('img/about-'.$page->slug.'.jpg')}}" alt="{{$page->name}}">
				</div>
				<div id="overview">
					<h3>{{$page->name}}</h3>
					{!! $page->body !!}
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/about/sidebar')
		</div>
	</div>
</div>
@endsection
