<?php 
  use App\Helpers\SiteHelper ;
?>
@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-12">
			<div>
				@if($page->template_id==1)
					<h2>{{$page->name}}</h2>
				@endif
				<div>
					
				</div>
				<div>
					{!! $page->body !!}
				</div>
			</div>

			<div class="row">
			<div class="col-sm-12">
			<hr class="colorgraph">
				<h4>Advisory  Board</h4>
			</div>
				@if(isset($adivisoryPages))
					@foreach($adivisoryPages as $p)
						<div class="col-sm-3">
							<div class="thumbnail">
								<div class="caption">
									@if(isset($p->Image))
										<img  class="img-responsive" style="max-height:250px;" src="{{asset($p->Image->ReturnUrl())}}">
									@endif
									{{$p->name}}
									<p>{{$p->excerpt}}</p>
								</div>
								@if(strlen($p->body)>5)

									<p>
					        			<a href="{{url('management/'.$p->slug)}}" class="btn btn-danger btn-dark pull" role="button">More</a> 
					        		</p>
								@endif
							</div>
						</div>
						
					@endforeach
				@endif
			</div>

		</div>
			<div class="row">

				<div class="col-sm-12">
					<hr class="colorgraph">
					<h4>Management  Team</h4>
				</div> 
				@if(isset($managementPages))
					@foreach($managementPages as $p)
						<div class="col-sm-3">
							<div class="thumbnail">
								<div class="caption">
									@if(isset($p->Image))
										<img  class="img-responsive" style="max-height:250px;" src="{{asset($p->Image->ReturnUrl())}}">
									@endif
									{{$p->name}}
									<p>{{$p->excerpt}}</p>
								</div>
								@if(strlen($p->body)>5)
									
									<p>
					        			<a href="{{url('management/'.$p->slug)}}" class="btn btn-danger btn-dark pull" role="button">More</a> 
					        		</p>
								@endif
							</div>
						</div>
						
					@endforeach
				@endif
			</div>
		<div id="fbh-sidebar" class="col-md-4">
			
		</div>
	</div>
</div>
@endsection