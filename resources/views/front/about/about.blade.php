<?php 
  use App\Helpers\SiteHelper;
?>
@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
<div class="container">
	<h2 class="text-center">{{$teaser->name}}</h2>
	<div>
		{!! $teaser->body!!}

	</div>
	<hr class="colorgraph">
	<br>
	<div class="row">
		<div id="fbh-main" class="col-md-12">
			<div class="row">
	
			  @if(isset($pages['mission-and-vision']))
			  	<div class="col-sm-6 col-md-4">
				    <div class="thumbnail">
				    <h3>Mission & Vision</h3>
				      <img src="{{asset('img/about-mission-and-vision.jpg')}}" alt="...">
				      <div class="caption">
				        
				        <p>
				        	{!! $pages['mission-and-vision']->body!!}
						</p>
				        <p class="hidden">
				        	<a href="{{url('about/mission-and-vision')}}" class="btn btn-danger btn-dark" role="button">More</a> 
				        </p>
				      </div>
				    </div>
				  </div>
			  @endif
			  	
			  @if(isset($pages['core-values']))
			  	<div class="col-sm-6 col-md-4">
				    <div class="thumbnail">
				    	<h3>Values</h3>
				      	<img src="{{asset('img/about-core-values.jpg')}}" alt="...">
				      	<div class="caption">
				        
				        <p>
				        	{!! SiteHelper::ReturnWords($pages['core-values']->body,40) !!}
						</p>
				        <p>
				        	<a href="{{url('about/core-values')}}" class="btn btn-danger btn-dark" role="button">More</a> 
				        </p>
				      </div>
				    </div>
				</div>
			  @endif

			  @if(isset($pages['careers']))
			  	<div class="col-sm-6 col-md-4">
				    <div class="thumbnail">
				    	<h3>Careers</h3>
				      	@if(isset($pages['careers']->Image))
							<img class="" src="{{asset($pages['careers']->Image->ReturnUrl())}}" alt="...">
						@endif
				      	<div class="caption">
				        
				        <p>
				        	{!! SiteHelper::ReturnWords($pages['careers']->body,40) !!}
						</p>
				        <p>
				        	<a href="{{url('careers')}}" class="btn btn-danger btn-dark" role="button">More</a> 
				        </p>
				      </div>
				    </div>
				</div>
			  @endif
			  		 
			</div>
			<div class="row">
	
			  	@if(isset($pages['governance']))
			  		<div class="col-sm-6 col-md-4">
					    <div class="thumbnail">
					    	<h3>Governance</h3>
					      	@if(isset($pages['governance']->Image))
								<img class="" src="{{asset($pages['governance']->Image->ReturnUrl())}}" alt="...">
							@endif
						      <div class="caption">
						        
						        <p>{!! SiteHelper::ReturnWords($pages['governance']->body,40) !!}</p>
						        <p>
						        	<a href="{{url('about/governance')}}" class="btn btn-danger btn-dark" role="button">More</a> 
						        </p>
						     </div>
					    </div>
				  </div>
			  	@endif
			 @if(isset($pages['content-guidelines']))
				  <div class="col-sm-6 col-md-4">
					    <div class="thumbnail">
					    <h3>Content Guidelines</h3>
					    	@if(isset($pages['content-guidelines']->Image))
								<img class="" src="{{asset($pages['content-guidelines']->Image->ReturnUrl())}}" alt="...">
							@endif
					     
					      <div class="caption">
					        
					        <p>{!! SiteHelper::ReturnWords($pages['content-guidelines']->body,40) !!}</p>
					        <p>
					        	<a href="{{url('content-guidelines')}}" class="btn btn-danger btn-dark" role="button">More</a> 
					        </p>
					      </div>
					    </div>
				  </div>
			@endif
			@if(isset($management))
			  	<div class="col-sm-6 col-md-4">
				    <div class="thumbnail">
				    <h3>{{$management->name}}</h3>
				    	@if(isset($management->Image))
							<img class="" src="{{$management->Image->ReturnUrl()}}" alt="...">
						@endif
				      <div class="caption">
				        
				        <p>{!! SiteHelper::ReturnWords($management->body,40) !!}</p>
				        <p>
				        	<a href="{{url('management')}}" class="btn btn-danger btn-dark" role="button">More</a> 
				        </p>
				      </div>
				    </div>
			  </div>
			  @endif
			</div>
		</div>
		
	</div>
</div>
<style type="text/css">
	
	p br{
		display: none;
		padding-left: 3px;

	}
</style>

@endsection

