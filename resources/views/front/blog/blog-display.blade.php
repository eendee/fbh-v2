@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row" style="margin-top:20px;">
		<div id="fbh-main" class="col-md-8">
			<div class="card" style="width:99%; padding:20px;">
				
				<div style="margin-bottom: 20px;">
					@if(isset($page->Image))
						<img   style="max-height:400px;" class="img-responsive" src="{{asset($page->Image->ReturnUrl())}}">
					@endif
				</div>
				<div style="text-align:justify;">
					{!! $page->body !!}
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			<div  id="fb-sb" style="border-top: 5px solid;">
				<h3>Latest Posts</h3>
				@if(isset($posts))
					<ul class="nav2">
						@foreach($posts as $n)
							<li>
								<a href="{{url('blog/'.$n->slug)}}">
									{{$n->name}}
								</a>
							</li>
						@endforeach

					</ul>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
