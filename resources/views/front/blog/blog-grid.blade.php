<?php
  use App\Helpers\SiteHelper ;
?>
<div class="row clear-threes">
	@if(isset($posts))
		@foreach($posts as $n)
			<div class="col-sm-4">
				<div class="card" style="padding: 0px; overflow: hidden">
					<div class="card-body" style="min-height:350px; padding: 0px; overflow: hidden">
						<div class="">
							<?php
								$img=asset('img/home023.jpg');
								if(isset($n->Image))
								{
									$img=asset($n->Image->ReturnUrl());
								}
							?>
							<img class="media-object img-responsive" src="{{$img}}">

						</div>

                        <div style="padding: 10px 20px;">
                            <h4 style="font-weight: bold; color: #6B0707">
                                <span class="news-title">
                                    {{$n->name}}
                                    </br>
                                    <small>
                                        <span class="pull-right text-muted" style="font-size:.8em; padding-bottom: 20px; padding-top: 10px">
                                            {{SiteHelper::ReturnDayFromDateObject($n->created_at)}}
                                        </span>
                                    </small>
                                </span>
                            </h4>


                            <div class="news-excerpt" style="padding-top:30px;">
                                <?php $body_array=null;
                                    $body_array=SiteHelper::ReturnWordsWithFlag($n->body,20);
                                ?>
                                {!!$body_array[0]!!}
                                @if($body_array[1])
                                <a href="{{url('blog/'.$n->slug)}}" style="margin-top: 10px;">... read more</a>
                                @endif
                            </div>
                        </div>

					</div>
				</div>
			</div>
		@endforeach
	@endif
</div>
