@extends('front.layouts.main')

@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-12">
			<div class="row" style="margin-top:20px;">
				@include('front.blog.blog-grid')
			</div>
		</div>
		
	</div>
</div>
@endsection
