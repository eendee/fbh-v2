@extends('front/layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')
@include('front/partials/sliders/slick_slider')

<div class="container-fluid header-container" style="background:#fff; padding: 0px;">
	<p  class="row" style="margin: 0px; padding-top: 0px;">
		<div id="fbh-main" class="col-md-12">
                {{-- <div class="row">
                    <a href="{{url('/write-review')}}" style="background:#595252" class="visible-xs visible-sm btn btn-danger  btn-block">Write a Review</a>
                    <a href="{{url('/write-review/post')}}" style="background:#595252" class="visible-xs visible-sm btn btn-danger  btn-block">Add a Photo</a>
                    <a href="{{url('/write-review/question')}}" style="background:#595252" class="visible-xs visible-sm btn btn-danger  btn-block">Ask a Question</a>
                </div>

	    		<hr class="colorgraph"> --}}
		</div>
	</p>
    <div style="margin-top: 0px;">
        <v-app>
            <app-home l_status="{{Auth::guest()?0:1}}"></app-home>
        </v-app>
    </div>
</div>
<div>
	@include('front/partials/polls/polls-popup')
</div>
@endsection
