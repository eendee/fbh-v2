@extends('front.layouts.main')
@section('title', $meta->title)
@section('description', $meta->description)
@section('content')

<div class="container">
	<div class="row">
		<div id="fbh-main" class="col-md-8">
			<div>
				@if($page->template_id==1)
					<h2>{{$page->name}}</h2>
				@endif
				<div>
					@if(isset($page->Image))
						<img   style="max-height:400px;" class="img-responsive" src="{{asset($page->Image->ReturnUrl())}}">
					@endif
				</div>
				<div>
					{!! $page->body !!}
				</div>
			</div>

		</div>
		<div id="fbh-sidebar" class="col-md-4">
			@include('front/about/sidebar')
		</div>
	</div>
</div>
@endsection
